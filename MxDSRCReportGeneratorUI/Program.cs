﻿//******************************************************************************************
//*   Proprietary program material                                                         *
//*                                                                                        *
//*   This material is proprietary to Danlaw, Inc., and is not to be reproduced,  used     *
//*   or disclosed except in accordance with a written license agreement with Danlaw, Inc. *
//*                                                                                        *
//*   (C) Copyright 2017 Danlaw, Inc.  All rights reserved.                                *
//*                                                                                        *
//*   Danlaw, Inc., believes that the material furnished herewith is accurate and          *
//*   reliable.  However, no responsibility, financial or otherwise, can be accepted for   *
//*   any consequences arising out of the use of this material.                            *
//*                                                                                        *
//******************************************************************************************

using System;
using System.Reflection;
using System.Windows.Forms;

namespace MxDSRCReportGenerator
{
   static class Program
   {
      /// <summary>
      /// The main entry point for the application.
      /// </summary>
      [STAThread]
      static void Main()
      {
         AppDomain.CurrentDomain.AssemblyResolve += (sender, args) => {
            string resourceName = "MxDSRCReportGenerator.Resources." + new AssemblyName(args.Name).Name + ".dll";
            using (var stream = Assembly.GetExecutingAssembly().GetManifestResourceStream(resourceName))
            {
               byte[] assemblyData = new byte[stream.Length];
               stream.Read(assemblyData, 0, assemblyData.Length);
               return Assembly.Load(assemblyData);
            }
         };
         Application.EnableVisualStyles();
         Application.SetCompatibleTextRenderingDefault(false);
         Application.Run(new frmMxDsrcReportGenerator());
      }
   }
}
