﻿//******************************************************************************************
//*   Proprietary program material                                                         *
//*                                                                                        *
//*   This material is proprietary to Danlaw, Inc., and is not to be reproduced,  used     *
//*   or disclosed except in accordance with a written license agreement with Danlaw, Inc. *
//*                                                                                        *
//*   (C) Copyright 2017 Danlaw, Inc.  All rights reserved.                                *
//*                                                                                        *
//*   Danlaw, Inc., believes that the material furnished herewith is accurate and          *
//*   reliable.  However, no responsibility, financial or otherwise, can be accepted for   *
//*   any consequences arising out of the use of this material.                            *
//*                                                                                        *
//******************************************************************************************

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Windows.Forms;
using System.IO;
using GemBox.Document;
using GemBox.Document.Tables;
using System.Runtime.Remoting.Messaging;
using System.Windows.Forms.DataVisualization.Charting;


// This software use points for dimensions
// an inch is 72 points

namespace MxDSRCReportGenerator
{
   public partial class frmMxDsrcReportGenerator : Form
   {
      private const Keys CopyKeys = Keys.Control | Keys.C;
      private const string SummaryTableTitle16092 = "IEEE1609.2 Test Summary Result";
      private const string SummaryTableTitle16093 = "IEEE1609.3 Test Summary Result";
      private const string SummaryTableTitle16094 = "IEEE1609.4 Test Summary Result";
      private const string SummaryTableTitle80211 = "IEEE802.11 Test Summary Result";
      private const string SummaryTableTitleJ2945 = "J2945 Test Summary Result";
      private const string SummaryTableTitleV2I = "V2I Test Summary Result";
      private const int WidthPointsPerInch = 72;

      public delegate ReportStatus GenerateTestReport();
      public delegate ReportStatus AppendTestReport();

      private string myAppendToFilePath;
      private string myDeviceImageFile = string.Empty;
      private string myLabLogoFile = string.Empty;
      private string myRegressionReportFolder = string.Empty;
      private string myReportsDirectory = string.Empty;
      private List<string> myRegressionFilesList = new List<string>();
      private GemBox.Document.DocumentModel myDocumentModel;
      private GemBox.Document.Section myDocumentMainSection;
      private GemBox.Document.ParagraphStyle myHeading1Style;
      private GemBox.Document.ParagraphStyle myTableHeadingStyle;
      private GemBox.Document.ParagraphStyle myTableCellStyle;
      private GemBox.Document.ParagraphStyle myRedCellStyle;
      private GemBox.Document.ParagraphStyle myGreenCellStyle;
      private GemBox.Document.ParagraphStyle myBlueCellStyle;
      private GemBox.Document.ParagraphStyle myCentreBoldStyle;
      private GenerateTestReport myGenerateTestReportHandler;
      private AppendTestReport myAppendTestReportHandler;

      public frmMxDsrcReportGenerator()
      {
         GemBox.Document.ComponentInfo.SetLicense("DXK1-O45U-N5K3-ZJ3C");
         InitializeComponent();
         this.cmbSpecification.SelectedIndex = (int)Specifications.WAVE80211;
         this.tbMnemonic.Text = "WAVE802.11-TSS&TP";
         Control.CheckForIllegalCrossThreadCalls = false;
         this.InitializeDocumentModel();
         this.Text = $"MxDSRC Report Generator {System.Reflection.Assembly.GetExecutingAssembly().GetName().Version}";
      }

      public int ProgressLabel
      {
         set
         {
            Invoke((MethodInvoker)delegate
            {
               this.ctlProgressBar.Value = value;
               this.ctlProgressBar.Minimum = 0;
               this.ctlProgressBar.Maximum = 100;
            });
         }
      }
      public string AppendErrorMessage
      {
         set { Invoke((MethodInvoker)(() => rtbErrorWindow.AppendText(value))); }
      }

      public void AddPrologue(ref GemBox.Document.DocumentModel doc, ref Section mainSection)
      {
         string para1 = "This report details the outcome of performing the tests with the parameters detailed in the appropriate sections of this document. This document does not in itself guarantee that a device meets all the "
                        + "requirements instead it may be used as evidence of compliance / non-compliance with the test procedures.  Further independent review may be required to obtain certification.";
         string para1a = "The test system is partitioned such that the tests for each of the Test Specifications are run separately and the reported data is presented as such.";
         string para2 = "This test report contains data obtained from both running the Danlaw MxDSRC Automated test suite supporting the stated Test Specifications and data entered manually from those tests performed outside of the Mx-Suite environment.";
         string para3 = "Test equipment may vary between the Test Specifications. The detailed list of equipment is included in the individual Test Result sections of this document.";
         string para4 = "This report does not contain data produced under subcontract.";
         string para5 = "The calibration of all analog measuring and test equipment and the measured data using this equipment are traceable to the National Institute for Standards and Technology (NIST).";
         string para6 = "The test results contained in this report relate only to the Item(s) tested. Any electrical, Firmware, Software, or mechanical modification made to the test item after the test "
                        + "shall invalidate the data presented in this report. Any modification made to the test item after this test date shall require an evaluation to verify continued compliance.";
         string para7 = "This report shall not be reproduced, except in full, without the written approval of Danlaw Inc.";
         string para8 = "This report shall not be used to claim product endorsement by Danlaw or other organization.";

         mainSection.Blocks.Add(new GemBox.Document.Paragraph(doc, "Introduction") { ParagraphFormat = { Style = this.myHeading1Style } });
         mainSection.Blocks.Add(new GemBox.Document.Paragraph(doc, new GemBox.Document.Run(doc, para1)));
         mainSection.Blocks.Add(new GemBox.Document.Paragraph(doc, new GemBox.Document.Run(doc, para1a)));

         mainSection.Blocks.Add(new GemBox.Document.Paragraph(doc, "Test Data") { ParagraphFormat = { Style = this.myHeading1Style } });
         mainSection.Blocks.Add(new GemBox.Document.Paragraph(doc, new GemBox.Document.Run(doc, para2)));

         mainSection.Blocks.Add(new GemBox.Document.Paragraph(doc, "Test Equipment") { ParagraphFormat = { Style = this.myHeading1Style } });
         mainSection.Blocks.Add(new GemBox.Document.Paragraph(doc, new GemBox.Document.Run(doc, para3)));

         mainSection.Blocks.Add(new GemBox.Document.Paragraph(doc, "Subcontracted Testing") { ParagraphFormat = { Style = this.myHeading1Style } });
         mainSection.Blocks.Add(new GemBox.Document.Paragraph(doc, new GemBox.Document.Run(doc, para4)));

         mainSection.Blocks.Add(new GemBox.Document.Paragraph(doc, "Test Traceability") { ParagraphFormat = { Style = this.myHeading1Style } });
         mainSection.Blocks.Add(new GemBox.Document.Paragraph(doc, new GemBox.Document.Run(doc, para5)));

         mainSection.Blocks.Add(new GemBox.Document.Paragraph(doc, "Limitations on Results") { ParagraphFormat = { Style = this.myHeading1Style } });
         mainSection.Blocks.Add(new GemBox.Document.Paragraph(doc, new GemBox.Document.Run(doc, para6)));

         mainSection.Blocks.Add(new GemBox.Document.Paragraph(doc, "Limitations on Copying") { ParagraphFormat = { Style = this.myHeading1Style } });
         mainSection.Blocks.Add(new GemBox.Document.Paragraph(doc, new GemBox.Document.Run(doc, para7)));

         mainSection.Blocks.Add(new GemBox.Document.Paragraph(doc, "Limitations of the Report") { ParagraphFormat = { Style = this.myHeading1Style } });
         mainSection.Blocks.Add(new GemBox.Document.Paragraph(doc, new GemBox.Document.Run(doc, para8)));

         mainSection.Blocks.Add(new GemBox.Document.Paragraph(doc, "Device Under Test Description") { ParagraphFormat = { Style = this.myHeading1Style } });
         mainSection.Blocks.Add(new GemBox.Document.Paragraph(doc, new GemBox.Document.Run(doc, txtDeviceDescription.Text.Replace("\r\n", " "))));
      }
      private void AddRowWithTwoColumns(ref GemBox.Document.DocumentModel doc, ref Table table, string[] parsedTestResults)
      {
         try
         {
            TableRow row = new TableRow(doc);
            row.RowFormat.Height = new TableRowHeight(12, TableRowHeightRule.AtLeast);
            table.Rows.Add(row);

            Paragraph para = new Paragraph(doc, parsedTestResults[0]);
            para.ParagraphFormat.Alignment = GemBox.Document.HorizontalAlignment.Left;
            para.ParagraphFormat.Style = this.myTableCellStyle;

            TableCell cell = new TableCell(doc, para);
            cell.CellFormat.TextDirection = TableCellTextDirection.LeftToRight;
            row.Cells.Add(cell);

            para = new Paragraph(doc, parsedTestResults[1]);
            para.ParagraphFormat.Alignment = GemBox.Document.HorizontalAlignment.Left;
            if (parsedTestResults[1].ToUpper().Equals("PASS"))
            {
               para.ParagraphFormat.Style = this.myGreenCellStyle;
            }
            else
            {
               if (parsedTestResults[1].ToUpper().Equals("FAIL"))
               {
                  para.ParagraphFormat.Style = this.myRedCellStyle;
               }
               else
               {
                  para.ParagraphFormat.Style = this.myTableCellStyle;
               }
            }
            cell = new TableCell(doc, para);
            cell.CellFormat.TextDirection = TableCellTextDirection.LeftToRight;
            row.Cells.Add(cell);
         }
         catch
         {
            this.AppendErrorMessage = "Unable to add Test case results in report. Possible reasons:\n" +
                                 "1. 802.11 Tests: Regression was not completed, has errors or execution was aborted.\n" +
                                 "2. 1609.X Tests: Report summary files (csv) are not in proper format.";
         }
      }
      private void AddRowWithTwoColumnsAndMerge(ref GemBox.Document.DocumentModel doc, ref Table table, string[] parsedTestResults)
      {
         try
         {
            TableRow row = new TableRow(doc);
            row.RowFormat.Height = new TableRowHeight(12, TableRowHeightRule.AtLeast);
            table.Rows.Add(row);

            Paragraph para = new Paragraph(doc, parsedTestResults[0]) { ParagraphFormat = new ParagraphFormat() { Alignment = GemBox.Document.HorizontalAlignment.Center, Style = this.myCentreBoldStyle } };
            TableCell cell = new TableCell(doc, para) { ColumnSpan = 2, CellFormat = new TableCellFormat() { TextDirection = TableCellTextDirection.LeftToRight } };
            row.Cells.Add(cell);
         }
         catch
         {
            this.AppendErrorMessage = "Unable to add Test case Inputs in report. Possible reasons:\n" +
                                 "1. 802.11 Tests: Regression was not completed, has errors or execution was aborted.\n" +
                                 "2. 1609.X Tests: Report summary files (csv) are not in proper format.";
         }
      }
      private void AddRowWithFiveColumnsAndMergeInto2Columns(ref GemBox.Document.DocumentModel doc, ref Table table, string[] parsedTestResults)
      {
         try
         {
            TableRow row = new TableRow(doc) { RowFormat = new TableRowFormat() { Height = new TableRowHeight(12, TableRowHeightRule.AtLeast) } };
            table.Rows.Add(row);

            Paragraph para = new Paragraph(doc, parsedTestResults[0]);
            para.ParagraphFormat.Alignment = GemBox.Document.HorizontalAlignment.Left;
            para.ParagraphFormat.Style = this.myTableCellStyle;

            TableCell cell = new TableCell(doc, para) { ColumnSpan = 2 };
            cell.CellFormat.TextDirection = TableCellTextDirection.LeftToRight;
            row.Cells.Add(cell);

            para = new Paragraph(doc, parsedTestResults[1]);
            para.ParagraphFormat.Alignment = GemBox.Document.HorizontalAlignment.Left;
            para.ParagraphFormat.Style = this.myTableCellStyle;

            cell = new TableCell(doc, para) { ColumnSpan = 3 };
            cell.CellFormat.TextDirection = TableCellTextDirection.LeftToRight;
            row.Cells.Add(cell);
         }
         catch
         {
            this.AppendErrorMessage = "Unable to add Iteration Inputs or Results in report. Possible reasons:\n" +
                                 "1. 802.11 Tests: Regression was not completed, has errors or execution was aborted.\n" +
                                 "2. 1609.X Tests: Report summary files (csv) are not in proper format.";
         }
      }
      private void AddIterationData(ref GemBox.Document.DocumentModel doc, ref Table table, string[] inputs, string verdict, string comment)
      {
         try
         {
            TableRow row = new TableRow(doc) { RowFormat = new TableRowFormat() { Height = new TableRowHeight(12, TableRowHeightRule.AtLeast) } };
            table.Rows.Add(row);

            Paragraph[] para1 = new Paragraph[inputs.Length];
            for (int i = 0; i < inputs.Length; i++)
            {
               para1[i] = new Paragraph(doc, new Run(doc, inputs[i]), new Run(doc, " ")) { ParagraphFormat = new ParagraphFormat() { Alignment = GemBox.Document.HorizontalAlignment.Left, Style = this.myTableCellStyle } };
            }
            TableCell cell = new TableCell(doc, para1) { CellFormat = new TableCellFormat() { TextDirection = TableCellTextDirection.LeftToRight, WrapText = true } };
            row.Cells.Add(cell);

            Paragraph para = new Paragraph(doc, verdict);
            para.ParagraphFormat.Alignment = GemBox.Document.HorizontalAlignment.Left;
            if (verdict.ToUpper().Contains("FAIL"))
            {
               para.ParagraphFormat.Style = this.myRedCellStyle;
            }
            else
            {
               para.ParagraphFormat.Style = this.myGreenCellStyle;
            }
            cell = new TableCell(doc, para) { CellFormat = new TableCellFormat() { TextDirection = TableCellTextDirection.LeftToRight, WrapText = true } };
            row.Cells.Add(cell);

            string[] comments = comment.Split(new char[] { '\n' }, StringSplitOptions.RemoveEmptyEntries);
            para1 = new Paragraph[comments.Length];
            for (int i = 0; i < comments.Length; i++)
            {
               para1[i] = new Paragraph(doc, new Run(doc, comments[i]), new SpecialCharacter(doc, SpecialCharacterType.Tab)) { ParagraphFormat = new ParagraphFormat() { Alignment = GemBox.Document.HorizontalAlignment.Left, Style = this.myTableCellStyle } };
            }
            cell = new TableCell(doc, para1) { CellFormat = new TableCellFormat() { TextDirection = TableCellTextDirection.LeftToRight, WrapText = true } };
            row.Cells.Add(cell);
         }
         catch (Exception e)
         {
            this.AppendErrorMessage = "Unable to add Iteration Inputs or Results in report. Possible reasons:\n" +
                                 "1. 802.11 Tests: Regression was not completed, has errors or execution was aborted.";
         }
      }
      private void AddTestStepOrDescription(ref GemBox.Document.DocumentModel doc, ref Table table, string[] data)
      {
         try
         {
            TableRow row = new TableRow(doc) { RowFormat = new TableRowFormat() { Height = new TableRowHeight(12, TableRowHeightRule.AtLeast) } };
            table.Rows.Add(row);

            Paragraph para = new Paragraph(doc, data[0]) { ParagraphFormat = new ParagraphFormat() { Alignment = GemBox.Document.HorizontalAlignment.Left, Style = this.myTableCellStyle } };
            TableCell cell = new TableCell(doc, para) { CellFormat = new TableCellFormat() { TextDirection = TableCellTextDirection.LeftToRight } };
            row.Cells.Add(cell);

            para = new Paragraph(doc, data[1]) { ParagraphFormat = new ParagraphFormat() { Alignment = GemBox.Document.HorizontalAlignment.Left, Style = this.myTableCellStyle } };

            cell = new TableCell(doc, para) { CellFormat = new TableCellFormat() { TextDirection = TableCellTextDirection.LeftToRight } };
            row.Cells.Add(cell);

            string[] descriptions = data[2].Split(new char[] { '\n' }, StringSplitOptions.RemoveEmptyEntries);
            Paragraph[] para1 = new Paragraph[descriptions.Length];
            for (int i = 0; i < descriptions.Length; i++)
            {
               para1[i] = new Paragraph(doc, new Run(doc, descriptions[i]), new SpecialCharacter(doc, SpecialCharacterType.Tab)) { ParagraphFormat = new ParagraphFormat() { Alignment = GemBox.Document.HorizontalAlignment.Left, Style = this.myTableCellStyle } };
            }
            cell = new TableCell(doc, para1) { CellFormat = new TableCellFormat() { TextDirection = TableCellTextDirection.LeftToRight } };
            row.Cells.Add(cell);
         }
         catch
         {
            this.AppendErrorMessage = "Unable to add Test case results in report. Possible reasons:\n" +
                                 "1. 802.11 Tests: Regression was not completed, has errors or execution was aborted.";
         }
      }
      private void AddRowWithFiveColumns(ref GemBox.Document.DocumentModel doc, ref Table table, string[] parsedTestResults)
      {
         try
         {
            table.TableFormat.AutomaticallyResizeToFitContents = false;

            TableRow row = new TableRow(doc);
            row.RowFormat.Height = new TableRowHeight(12, TableRowHeightRule.AtLeast);
            table.Rows.Add(row);

            for (int i = 0; i < parsedTestResults.Length; i++)
            {
               if (parsedTestResults[i].Contains('\n'))
               {
                  string[] texts = parsedTestResults[i].Split('\n');
                  List<Paragraph> items = new List<Paragraph>();
                  foreach (string text in texts)
                  {
                     Paragraph para = new Paragraph(doc, text);
                     para.ParagraphFormat.Alignment = GemBox.Document.HorizontalAlignment.Left;
                     para.ParagraphFormat.Style = this.myTableCellStyle;
                     items.Add(para);
                  }

                  TableCell cell = new TableCell(doc, items);
                  cell.CellFormat.TextDirection = TableCellTextDirection.LeftToRight;
                  row.Cells.Add(cell);
               }
               else
               {
                  Paragraph para = new Paragraph(doc, parsedTestResults[i]);
                  para.ParagraphFormat.Alignment = GemBox.Document.HorizontalAlignment.Left;
                  if (parsedTestResults[i].ToUpper().Contains("FAIL"))
                  {
                     para.ParagraphFormat.Style = this.myRedCellStyle;
                  }
                  else
                  {
                     if (parsedTestResults[i].ToUpper().Contains("PASS"))
                     {
                        para.ParagraphFormat.Style = this.myGreenCellStyle;
                     }
                     else
                     {
                        para.ParagraphFormat.Style = this.myTableCellStyle;
                     }
                  }

                  TableCell cell = new TableCell(doc, para);
                  cell.CellFormat.TextDirection = TableCellTextDirection.LeftToRight;
                  row.Cells.Add(cell);
               }
            }
         }
         catch
         {
            this.AppendErrorMessage = "Unable to add Test Summary in report. Possible reasons:\n" +
                                 "1. 802.11 Tests: Regression was not completed, has errors or execution was aborted.\n" +
                                 "2. 1609.X Tests: Report summary files (csv) are not in proper format.";
         }
      }
      private void AddRowWithFiveColumnsAndMergeInto1Column(ref GemBox.Document.DocumentModel doc, ref Table table, string[] parsedTestResults)
      {
         try
         {
            TableRow row = new TableRow(doc) { RowFormat = new TableRowFormat() { Height = new TableRowHeight(12, TableRowHeightRule.AtLeast) } };
            table.Rows.Add(row);

            Paragraph para1 = new Paragraph(doc, parsedTestResults[0]) { ParagraphFormat = new ParagraphFormat() { Alignment = GemBox.Document.HorizontalAlignment.Center, Style = this.myCentreBoldStyle } };
            TableCell cell1 = new TableCell(doc, para1) { ColumnSpan = 5, CellFormat = new TableCellFormat() { TextDirection = TableCellTextDirection.LeftToRight } };
            row.Cells.Add(cell1);
         }
         catch
         {
            this.AppendErrorMessage = "Unable to add Iteration information in report. Possible reasons:\n" +
                                 "1. 802.11 Tests: Regression was not completed, has errors or execution was aborted.\n" +
                                 "2. 1609.X Tests: Report summary files (csv) are not in proper format.";
         }
      }
      private ReportStatus AppendToReport()
      {
         try
         {
            this.myDocumentModel = DocumentModel.Load(this.myAppendToFilePath);
            this.myDocumentMainSection = this.myDocumentModel.Sections[1];
            this.myHeading1Style = (ParagraphStyle)this.myDocumentModel.Styles.First(item => item.Name == "heading 1");
            this.myTableHeadingStyle = (ParagraphStyle)this.myDocumentModel.Styles.First(item => item.Name == "tableHeadingStyle");
            this.myTableCellStyle = (ParagraphStyle)this.myDocumentModel.Styles.First(item => item.Name == "tableCellStyle");
            this.myGreenCellStyle = (ParagraphStyle)this.myDocumentModel.Styles.First(item => item.Name == "tableGreenCellStyle");
            this.myRedCellStyle = (ParagraphStyle)this.myDocumentModel.Styles.First(item => item.Name == "tableRedCellStyle");
            this.myBlueCellStyle = (ParagraphStyle)this.myDocumentModel.Styles.First(item => item.Name == "tableBlueCellStyle");
            this.myCentreBoldStyle = (ParagraphStyle)this.myDocumentModel.Styles.First(item => item.Name == "tableCentreBoldCellStyle");

            if (this.cmbSpecification.SelectedIndex != (int)Specifications.WAVE80211)
            {
               RegressionResults regressionResults = new RegressionResults(this.myRegressionReportFolder, (Specifications)this.cmbSpecification.SelectedIndex);
               switch (this.cmbSpecification.SelectedIndex)
               {
                  case (int)Specifications.SAEJ29451OnboardSystemRequirementsforV2VSafetyCommunications:
                     {

                     }
                     break;

                  case (int)Specifications.WAVEMultiChannelOperation:
                     {
                        this.AppendToSummaryTable(ref this.myDocumentModel, frmMxDsrcReportGenerator.SummaryTableTitle16094, regressionResults.SummaryResults);
                     }
                     break;

                  case (int)Specifications.WAVENetworkingServices:
                     {
                        this.AppendToSummaryTable(ref this.myDocumentModel, frmMxDsrcReportGenerator.SummaryTableTitle16093, regressionResults.SummaryResults);
                     }
                     break;

                  case (int)Specifications.WAVESecurityServices:
                     {
                        this.AppendToSummaryTable(ref this.myDocumentModel, frmMxDsrcReportGenerator.SummaryTableTitle16092, regressionResults.SummaryResults);
                     }
                     break;

                  case (int)Specifications.WAVEV2ITestSuite:
                     {

                     }
                     break;
               }
            }
            else
            {
               List<string> runTimeErrors = new List<string>();
               Processing80211RegressionResults regressionResults = new Processing80211RegressionResults(this.myRegressionReportFolder, ref runTimeErrors);
               int progress = 0;

               this.UpdateSummaryTable(ref this.myDocumentModel, regressionResults);

               foreach (MxDSRCReportGenerator.ResultSummary summary in regressionResults.ScenarioResults)
               {
                  List<List<Tuple<string, object, string>>> iterationInputs;
                  List<List<Tuple<string, object, string>>> iterationResults;
                  List<string> iterationVerdict;

                  regressionResults.GetIterationInformation(summary.ScenarioName, out iterationInputs, out iterationResults, out iterationVerdict);
                  List<Tuple<string, object>> testCaseInputs = regressionResults.GetTestCaseInputs(summary.ScenarioName);

                  Element[] tables = this.myDocumentModel.GetChildElements(true, ElementType.Table).ToArray();
                  Table resultsTable = (Table)tables.FirstOrDefault(item => ((Table)item).Metadata.Title == (summary.ScenarioName + " Verdict"));

                  int lastIteration = 0;
                  int lastIterationTableIndex = 0;
                  if (resultsTable == null)
                  {
                     this.CreateTestResultsTable(ref this.myDocumentModel, ref this.myDocumentMainSection, summary, testCaseInputs);
                  }
                  else
                  {
                     Table iterationTable = (Table)tables.LastOrDefault(item => ((Table)item).Metadata.Title.Contains($"{summary.ScenarioName} Iteration"));
                     string lastIterationString = iterationTable.Metadata.Title.ToString();
                     lastIterationTableIndex = this.myDocumentMainSection.Blocks.IndexOf(iterationTable);
                     lastIteration = int.Parse(lastIterationString.Substring(lastIterationString.LastIndexOf("#") + 1));
                  }

                  for (int iteration = 0; iteration < iterationInputs.Count; iteration++)
                  {
                     // Iteration Table
                     Table iterationTable = new Table(this.myDocumentModel);
                     List<string> additionalFiles = regressionResults.GetLogFilesFromMxOutFolder(summary.ScenarioName, iterationInputs[iteration], iteration);
                     this.CreateIterationTable(ref this.myDocumentModel, ref iterationTable, summary.ScenarioName, iteration, iterationInputs[iteration], iterationResults[iteration], iterationVerdict, additionalFiles);
                     if (lastIteration != 0)
                     {
                        iterationTable.Metadata.Title = $"{summary.ScenarioName} Iteration #{iteration + lastIteration + 1}";
                        iterationTable.Content.Replace($"Iteration #{iteration + 1}", $"Iteration #{iteration + lastIteration + 1}");
                        this.myDocumentMainSection.Blocks.Insert(lastIterationTableIndex + 1, iterationTable);
                     }
                     else
                     {
                        this.myDocumentMainSection.Blocks.Add(iterationTable);
                     }                     
                  }
                  this.ProgressLabel = (++progress * 100) / regressionResults.ScenarioResults.Count;
               }
               foreach (string error in runTimeErrors)
               {
                  this.AppendErrorMessage = error;
               }
            }

            this.PopulateTableOfContents(ref this.myDocumentModel);
            return ReportStatus.Success;
         }
         catch
         {
            this.AppendErrorMessage = "Error creating report. Possible reasons:\n" +
                                 "1. 802.11 Tests: Regression was not completed, has errors or execution was aborted.\n" +
                                 "2. 1609.X Tests: Report summary files (csv) are not in proper format.";
            return ReportStatus.Failure;
         }
      }
      private void AppendToSummaryTable(ref DocumentModel document, string metadataTitle, List<MxDSRCReportGenerator.ResultSummary> results)
      {
         ContentRange tableHeadingPlaceholder = document.Content.Find(metadataTitle).First();
         Element[] tables = document.GetChildElements(true, ElementType.Table).ToArray();
         Table summaryTable = (Table)tables.First(item => ((Table)item).Metadata.Title == metadataTitle);

         for (int i = 0; i < results.Count; i++)
         {
            TableRow row = new TableRow(document) { RowFormat = new TableRowFormat() { Height = new TableRowHeight(12, TableRowHeightRule.AtLeast) } };
            summaryTable.Rows.Add(row);

            Paragraph para1 = new Paragraph(document, results[i].ScenarioName) { ParagraphFormat = new ParagraphFormat() { Alignment = GemBox.Document.HorizontalAlignment.Left, Style = this.myTableCellStyle } };
            Paragraph para2 = new Paragraph(document, results[i].Result);
            para2.ParagraphFormat.Alignment = GemBox.Document.HorizontalAlignment.Left;
            if (results[i].Result.ToUpper().Equals("PASS"))
            {
               para2.ParagraphFormat.Style = this.myGreenCellStyle;
            }
            else
            {
               if (results[i].Result.ToUpper().Equals("FAIL"))
               {
                  para2.ParagraphFormat.Style = this.myRedCellStyle;
               }
               else
               {
                  para2.ParagraphFormat.Style = this.myBlueCellStyle;
               }
            }

            row.Cells.Add(new TableCell(document, para1) { CellFormat = new TableCellFormat() { TextDirection = TableCellTextDirection.LeftToRight } });
            row.Cells.Add(new TableCell(document, para2) { CellFormat = new TableCellFormat() { TextDirection = TableCellTextDirection.LeftToRight } });
         }

         tableHeadingPlaceholder.Start.InsertRange(summaryTable.Content);
      }
      private void AppendToSummaryTable(ref DocumentModel document, ref Table table, List<MxDSRCReportGenerator.ResultSummary> results)
      {
         for (int i = 0; i < results.Count; i++)
         {
            TableRow row = new TableRow(document) { RowFormat = new TableRowFormat() { Height = new TableRowHeight(12, TableRowHeightRule.AtLeast) } };
            table.Rows.Add(row);
            Paragraph para1 = new Paragraph(document, results[i].ScenarioName) { ParagraphFormat = new ParagraphFormat() { Alignment = GemBox.Document.HorizontalAlignment.Left, Style = this.myTableCellStyle } };
            Paragraph para2 = new Paragraph(document, results[i].Result);
            para2.ParagraphFormat.Alignment = GemBox.Document.HorizontalAlignment.Left;
            if (results[i].Result.ToUpper().Equals("PASS"))
            {
               para2.ParagraphFormat.Style = this.myGreenCellStyle;
            }
            else
            {
               if (results[i].Result.ToUpper().Equals("FAIL"))
               {
                  para2.ParagraphFormat.Style = this.myRedCellStyle;
               }
               else
               {
                  para2.ParagraphFormat.Style = this.myBlueCellStyle;
               }
            }

            row.Cells.Add(new TableCell(document, para1) { CellFormat = new TableCellFormat() { TextDirection = TableCellTextDirection.LeftToRight } });
            row.Cells.Add(new TableCell(document, para2) { CellFormat = new TableCellFormat() { TextDirection = TableCellTextDirection.LeftToRight } });

         }
      }
      private void btnAppendToFile_Click(object sender, EventArgs e)
      {
         this.ProgressLabel = 0;
         using (var fbd = new OpenFileDialog())
         {
            fbd.Filter = "Word Documents (*.docx)|*.docx";
            DialogResult result = fbd.ShowDialog();
            if (result == DialogResult.OK)
            {
               this.tbAppendToFile.BackColor = System.Drawing.Color.White;
               if (fbd.CheckFileExists)
               {
                  this.myAppendToFilePath = this.tbAppendToFile.Text = fbd.FileName;
               }
               else
               {
                  this.AppendErrorMessage = "File does not exist.";
               }
            }
         }
      }
      private void btnGenerateReport_Click(object sender, EventArgs e)
      {
         if (string.IsNullOrWhiteSpace(this.myRegressionReportFolder) || string.IsNullOrEmpty(this.myRegressionReportFolder))
         {
            this.AppendErrorMessage = "Reports Directory is either empty or null, unable to proceed";
            return;
         }

         rtbErrorWindow.Clear();
         if (this.chkAppendToFile.Checked)
         {
            this.btnGenerateReport.Enabled = false;
            this.myReportsDirectory = Directory.GetParent(this.myAppendToFilePath).FullName;
            this.myAppendTestReportHandler = new AppendTestReport(this.AppendToReport);
            this.myAppendTestReportHandler.BeginInvoke(new AsyncCallback(this.OpenReportFile), null);
         }
         else
         {
            this.btnGenerateReport.Enabled = false;
            if (!this.tbRegressionReportFolder.Text.Equals(this.myRegressionReportFolder))
            {
               this.tbRegressionReportFolder.Text = this.myRegressionReportFolder;
               this.tbRegressionReportFolder.BackColor = System.Drawing.Color.White;
            }
            this.myReportsDirectory = Directory.CreateDirectory(Path.Combine(this.myRegressionReportFolder, "CertificationTestReport")).FullName;
            this.myGenerateTestReportHandler = new GenerateTestReport(this.CreateNewCertificationReport);
            this.myGenerateTestReportHandler.BeginInvoke(new AsyncCallback(this.OpenReportFile), null);
         }
      }
      private void btnImportDeviceImage_Click(object sender, EventArgs e)
      {
         using (var ofd = new OpenFileDialog())
         {
            ofd.Filter = "JPEG files (*.jpeg) | *.jpeg |JPG files (*.jpg)|*.jpg|PNG files (*.png)|(*.png)|All files(*.*)|*.*";
            DialogResult result = ofd.ShowDialog();

            if (result == DialogResult.OK && ofd.CheckFileExists)
            {
               this.myDeviceImageFile = ofd.FileName;
               this.pbDeviceImage.Image = System.Drawing.Image.FromFile(this.myDeviceImageFile).GetThumbnailImage(this.pbDeviceImage.MaximumSize.Width, this.pbDeviceImage.MaximumSize.Height, null, IntPtr.Zero);
            }
         }
      }
      private void btnImportLabLogo_Click(object sender, EventArgs e)
      {
         using (var ofd = new OpenFileDialog())
         {
            ofd.Filter = "JPEG files (*.jpeg) | *.jpeg |JPG files (*.jpg)|*.jpg|PNG files (*.png)|(*.png)|All files(*.*)|*.*";
            DialogResult result = ofd.ShowDialog();

            if (result == DialogResult.OK && ofd.CheckFileExists)
            {
               this.myLabLogoFile = ofd.FileName;
               this.pbLabLogo.Image = System.Drawing.Image.FromFile(this.myLabLogoFile).GetThumbnailImage(this.pbLabLogo.MaximumSize.Width, this.pbLabLogo.MaximumSize.Height, null, IntPtr.Zero);
            }
         }
      }
      private void btnRegressionReportFolder_Click(object sender, EventArgs e)
      {
         this.ProgressLabel = 0;
         using (var fbd = new FolderBrowserDialog())
         {
            fbd.Description = "Select Regression Folder";
            DialogResult result = fbd.ShowDialog();
            if (result == DialogResult.OK)
            {
               this.tbRegressionReportFolder.BackColor = System.Drawing.Color.White;
               if (Directory.Exists(fbd.SelectedPath))
               {
                  this.tbRegressionReportFolder.Tag = "browse";
                  this.myRegressionReportFolder = this.tbRegressionReportFolder.Text = fbd.SelectedPath;
               }
               else
               {
                  this.AppendErrorMessage = "Selected Directory does not exist.";
               }
            }
         }
      }
      private void chkAppendToFile_CheckedChanged(object sender, EventArgs e)
      {
         if (this.chkAppendToFile.Checked && this.cmbSpecification.SelectedIndex == (int)Specifications.WAVE80211)
         {
            this.btnAppendToFile.Enabled = true;
            this.tbAppendToFile.Enabled = true;
         }
         else
         {
            this.btnAppendToFile.Enabled = false;
            this.tbAppendToFile.Enabled = false;
         }
      }
      private void cmbSpecification_SelectedIndexChanged(object sender, EventArgs e)
      {
         this.chkAppendToFile.Enabled = false;
         this.lblRegReportFolder.Text = "Test Report Folder";
         switch (this.cmbSpecification.SelectedIndex)
         {
            case (int)Specifications.SAEJ29451OnboardSystemRequirementsforV2VSafetyCommunications:
               {
                  this.tbMnemonic.Text = "J2945.1-TSS&TP";
               }
               break;

            case (int)Specifications.WAVE80211:
               {
                  this.tbMnemonic.Text = "WAVE802.11-TSS&TP";
                  this.chkAppendToFile.Enabled = true;
                  this.lblRegReportFolder.Text = "Regression Report Folder";
               }
               break;

            case (int)Specifications.WAVEMultiChannelOperation:
               {
                  this.tbMnemonic.Text = "WAVEMCO-TSS&TP";
               }
               break;

            case (int)Specifications.WAVENetworkingServices:
               {
                  this.tbMnemonic.Text = "WAVENS-TSS&TP";
               }
               break;

            case (int)Specifications.WAVESecurityServices:
               {
                  this.tbMnemonic.Text = "WAVE-1609.2-TSS&TP";
               }
               break;

            case (int)Specifications.WAVEV2ITestSuite:
               {
                  this.tbMnemonic.Text = "WAVEV2I-TSS&TP";
               }
               break;
         }
         this.chkAppendToFile_CheckedChanged(sender, e);
      }
      private string CreateGraph(string csvFile)
      {
         Chart chart = new Chart() { Size = new System.Drawing.Size(1920, 1080) };
         ChartArea chartArea = new ChartArea();
         chartArea.AxisX = new Axis() { LabelStyle = new LabelStyle() { Enabled = true, Font = new System.Drawing.Font("Consolas", 14) }, MajorGrid = new Grid() { LineColor = System.Drawing.Color.Gray } };
         chartArea.AxisY = new Axis() { LabelStyle = new LabelStyle() { Enabled = true, Font = new System.Drawing.Font("Consolas", 14) }, MajorGrid = new Grid() { LineColor = System.Drawing.Color.Gray } };

         System.IO.StreamReader fileReader = new StreamReader(csvFile, System.Text.Encoding.ASCII);
         Series series1 = new Series() { ChartType = SeriesChartType.Line, YValueType = ChartValueType.Double, Color = System.Drawing.Color.Brown, BorderWidth = 2, IsVisibleInLegend = true };
         Series series2 = new Series() { ChartType = SeriesChartType.Line, YValueType = ChartValueType.Double, Color = System.Drawing.Color.Red, BorderWidth = 2, IsVisibleInLegend = true };
         Series series3 = new Series() { ChartType = SeriesChartType.Line, YValueType = ChartValueType.Double, Color = System.Drawing.Color.DarkGreen, BorderWidth = 2, IsVisibleInLegend = true };

         while (!fileReader.EndOfStream)
         {
            string content = fileReader.ReadLine();
            string[] data = content.Split(',');
            if (data.Count() > 1)
            {
               double xValue;
               if (double.TryParse(data[0], out xValue))
               {
                  series1.Points.AddXY(xValue, double.Parse(data[1]));
                  series2.Points.AddXY(xValue, double.Parse(data[2]));
               }
               else
               {
                  chartArea.AxisX.Title = data[0];
                  chart.Legends.Add(new Legend(data[1]) { DockedToChartArea = chartArea.Name, IsDockedInsideChartArea = true });
                  chart.Legends.Add(new Legend(data[2]) { DockedToChartArea = chartArea.Name, IsDockedInsideChartArea = true });
                  series1.Legend = data[1];
                  series2.Legend = data[2];
                  series1.LegendText = data[1];
                  series2.LegendText = data[2];
               }
               if (data.Count() == 4)
               {
                  double yValue;
                  if (double.TryParse(data[3], out yValue))
                  {
                     series3.Points.AddXY(xValue, yValue);
                  }
                  else
                  {
                     chart.Legends.Add(new Legend(data[3]) { DockedToChartArea = chartArea.Name, IsDockedInsideChartArea = true, TextWrapThreshold = 30 });
                     series3.Legend = data[3];
                     series3.LegendText = data[3];
                  }
               }
            }
            else
            {
               chartArea.Name = content;
               chart.Titles.Add(content);
               if (content.Contains("TP-80211-TXT-PHY-BV-05"))
               {
                  series1.XValueType = ChartValueType.Int32;
                  series2.XValueType = ChartValueType.Int32;
                  series3.XValueType = ChartValueType.Int32;
                  chartArea.AxisX.Minimum = -30;
                  chartArea.AxisX.Maximum = 30;
                  chartArea.AxisX.MajorTickMark = new TickMark() { Enabled = false };
                  chartArea.AxisX.CustomLabels.Add(new CustomLabel(-25.5, -26.5, "-26", 0, LabelMarkStyle.None) { GridTicks = GridTickTypes.Gridline });
                  chartArea.AxisX.CustomLabels.Add(new CustomLabel(-17.5, -16.5, "-17", 0, LabelMarkStyle.None) { GridTicks = GridTickTypes.Gridline });
                  chartArea.AxisX.CustomLabels.Add(new CustomLabel(-16.5, -15.5, "-16", 0, LabelMarkStyle.None) { GridTicks = GridTickTypes.Gridline });
                  chartArea.AxisX.CustomLabels.Add(new CustomLabel(-0.5, 0.5, "0", 0, LabelMarkStyle.None) { GridTicks = GridTickTypes.Gridline });
                  chartArea.AxisX.CustomLabels.Add(new CustomLabel(15.5, 16.5, "16", 0, LabelMarkStyle.None) { GridTicks = GridTickTypes.Gridline });
                  chartArea.AxisX.CustomLabels.Add(new CustomLabel(16.5, 17.5, "17", 0, LabelMarkStyle.None) { GridTicks = GridTickTypes.Gridline });
                  chartArea.AxisX.CustomLabels.Add(new CustomLabel(25.5, 26.5, "26", 0, LabelMarkStyle.None) { GridTicks = GridTickTypes.Gridline });
               }
               else
               {
                  series1.XValueType = ChartValueType.Double;
                  series2.XValueType = ChartValueType.Double;
                  series1.Color = System.Drawing.Color.DarkGreen;
               }
            }
         }

         chart.ChartAreas.Add(chartArea);
         chart.Series.Add(series1);
         chart.Series.Add(series2);
         if (chartArea.Name.Contains("TP-80211-TXT-PHY-BV-05"))
         {
            chart.Series.Add(series3);
         }
         else
         {
            double y1Max = series1.Points.Max(item => item.YValues.Max());
            double y2Max = series2.Points.Max(item => item.YValues.Max());
            double y1Min = series1.Points.Min(item => item.YValues.Min());
            double y2Min = series2.Points.Min(item => item.YValues.Min());
            chartArea.AxisY.Maximum = Math.Max(y1Max, y2Max) + 2;
            chartArea.AxisY.Minimum = Math.Min(y1Min, y2Min) - 2;
         }

         string imageFile = Path.GetFullPath(csvFile).Replace(".csv", ".png");
         chart.SaveImage(imageFile, ChartImageFormat.Png);
         fileReader.Close();
         return imageFile;
      }
      private void CreateInformationTable(ref GemBox.Document.DocumentModel doc, ref Section mainSection)
      {
         Table table = new Table(doc);
         // This is the only way tables are visible in the document.
         table.TableFormat.PreferredWidth = new TableWidth(9 * frmMxDsrcReportGenerator.WidthPointsPerInch, TableWidthUnit.Point);
         TableMetadata metadata = table.Metadata;
         metadata.Title = "InformationTable";
         doc.Styles.Add((TableStyle)Style.CreateStyle(StyleTemplateType.LightShadingAccent2, doc));

         TableRow row = new TableRow(doc) { RowFormat = new TableRowFormat() { Height = new TableRowHeight(12, TableRowHeightRule.AtLeast) } };
         table.Rows.Add(row);
         Paragraph para = new Paragraph(doc, "Report Number") { ParagraphFormat = new ParagraphFormat() { Alignment = GemBox.Document.HorizontalAlignment.Left } };
         TableCell cell = new TableCell(doc, para) { CellFormat = new TableCellFormat() { PreferredWidth = new TableWidth(20, TableWidthUnit.Percentage) } };
         row.Cells.Add(cell);
         para = new Paragraph(doc, txtReportNumber.Text) { ParagraphFormat = new ParagraphFormat() { Alignment = GemBox.Document.HorizontalAlignment.Left } };
         cell = new TableCell(doc, para) { CellFormat = new TableCellFormat() { PreferredWidth = new TableWidth(80, TableWidthUnit.Percentage) } };
         row.Cells.Add(cell);

         row = new TableRow(doc);
         table.Rows.Add(row);
         para = new Paragraph(doc, "Product Name") { ParagraphFormat = new ParagraphFormat() { Alignment = GemBox.Document.HorizontalAlignment.Left } };
         row.Cells.Add(new TableCell(doc, para));
         para = new Paragraph(doc, txtProductName.Text) { ParagraphFormat = new ParagraphFormat() { Alignment = GemBox.Document.HorizontalAlignment.Left } };
         row.Cells.Add(new TableCell(doc, para));

         row = new TableRow(doc);
         table.Rows.Add(row);
         para = new Paragraph(doc, "Model Number") { ParagraphFormat = new ParagraphFormat() { Alignment = GemBox.Document.HorizontalAlignment.Left } };
         row.Cells.Add(new TableCell(doc, para));
         para = new Paragraph(doc, txtModelNumber.Text) { ParagraphFormat = new ParagraphFormat() { Alignment = GemBox.Document.HorizontalAlignment.Left } };
         row.Cells.Add(new TableCell(doc, para));

         row = new TableRow(doc);
         table.Rows.Add(row);
         para = new Paragraph(doc, "Serial Number") { ParagraphFormat = new ParagraphFormat() { Alignment = GemBox.Document.HorizontalAlignment.Left } };
         row.Cells.Add(new TableCell(doc, para));
         para = new Paragraph(doc, txtSerialNumber.Text) { ParagraphFormat = new ParagraphFormat() { Alignment = GemBox.Document.HorizontalAlignment.Left } };
         row.Cells.Add(new TableCell(doc, para));

         row = new TableRow(doc);
         table.Rows.Add(row);
         para = new Paragraph(doc, "Firmware Version") { ParagraphFormat = new ParagraphFormat() { Alignment = GemBox.Document.HorizontalAlignment.Left } };
         row.Cells.Add(new TableCell(doc, para));
         para = new Paragraph(doc, txtFirmwareVersion.Text) { ParagraphFormat = new ParagraphFormat() { Alignment = GemBox.Document.HorizontalAlignment.Left } };
         row.Cells.Add(new TableCell(doc, para));

         row = new TableRow(doc);
         table.Rows.Add(row);
         para = new Paragraph(doc, "Hardware Version") { ParagraphFormat = new ParagraphFormat() { Alignment = GemBox.Document.HorizontalAlignment.Left } };
         row.Cells.Add(new TableCell(doc, para));
         para = new Paragraph(doc, txtHardwareVersion.Text) { ParagraphFormat = new ParagraphFormat() { Alignment = GemBox.Document.HorizontalAlignment.Left } };
         row.Cells.Add(new TableCell(doc, para));

         row = new TableRow(doc);
         table.Rows.Add(row);
         para = new Paragraph(doc, "TCI Version") { ParagraphFormat = new ParagraphFormat() { Alignment = GemBox.Document.HorizontalAlignment.Left } };
         row.Cells.Add(new TableCell(doc, para));
         para = new Paragraph(doc, txtTciRev.Text) { ParagraphFormat = new ParagraphFormat() { Alignment = GemBox.Document.HorizontalAlignment.Left } };
         row.Cells.Add(new TableCell(doc, para));

         row = new TableRow(doc);
         table.Rows.Add(row);
         para = new Paragraph(doc, "Device Profile") { ParagraphFormat = new ParagraphFormat() { Alignment = GemBox.Document.HorizontalAlignment.Left } };
         row.Cells.Add(new TableCell(doc, para));
         para = new Paragraph(doc, txtDeviceProfile.Text) { ParagraphFormat = new ParagraphFormat() { Alignment = GemBox.Document.HorizontalAlignment.Left } };
         row.Cells.Add(new TableCell(doc, para));

         row = new TableRow(doc);
         table.Rows.Add(row);
         para = new Paragraph(doc, "Applicant Name") { ParagraphFormat = new ParagraphFormat() { Alignment = GemBox.Document.HorizontalAlignment.Left } };
         row.Cells.Add(new TableCell(doc, para));
         para = new Paragraph(doc, txtApplicantName.Text) { ParagraphFormat = new ParagraphFormat() { Alignment = GemBox.Document.HorizontalAlignment.Left } };
         row.Cells.Add(new TableCell(doc, para));

         row = new TableRow(doc);
         table.Rows.Add(row);
         para = new Paragraph(doc, "Applicant Address") { ParagraphFormat = new ParagraphFormat() { Alignment = GemBox.Document.HorizontalAlignment.Left } };
         row.Cells.Add(new TableCell(doc, para));
         para = new Paragraph(doc, txtApplicantAddress.Text.Replace('\r', ' ').Replace('\n', ' ')) { ParagraphFormat = new ParagraphFormat() { Alignment = GemBox.Document.HorizontalAlignment.Left } };
         row.Cells.Add(new TableCell(doc, para));

         row = new TableRow(doc);
         table.Rows.Add(row);
         para = new Paragraph(doc, "Test Lab") { ParagraphFormat = new ParagraphFormat() { Alignment = GemBox.Document.HorizontalAlignment.Left } };
         row.Cells.Add(new TableCell(doc, para));
         para = new Paragraph(doc, txtLabName.Text) { ParagraphFormat = new ParagraphFormat() { Alignment = GemBox.Document.HorizontalAlignment.Left } };
         row.Cells.Add(new TableCell(doc, para));

         row = new TableRow(doc);
         table.Rows.Add(row);
         para = new Paragraph(doc, "Lab Address") { ParagraphFormat = new ParagraphFormat() { Alignment = GemBox.Document.HorizontalAlignment.Left } };
         row.Cells.Add(new TableCell(doc, para));
         para = new Paragraph(doc, txtLabAddress.Text.Replace('\r', ' ').Replace('\n', ' ')) { ParagraphFormat = new ParagraphFormat() { Alignment = GemBox.Document.HorizontalAlignment.Left } };
         row.Cells.Add(new TableCell(doc, para));

         row = new TableRow(doc);
         table.Rows.Add(row);
         para = new Paragraph(doc, "Test Date") { ParagraphFormat = new ParagraphFormat() { Alignment = GemBox.Document.HorizontalAlignment.Left } };
         row.Cells.Add(new TableCell(doc, para));
         para = new Paragraph(doc, DateTime.Now.ToString("M/d/yyyy")) { ParagraphFormat = new ParagraphFormat() { Alignment = GemBox.Document.HorizontalAlignment.Left } };
         row.Cells.Add(new TableCell(doc, para));

         row = new TableRow(doc);
         table.Rows.Add(row);
         para = new Paragraph(doc, "Tested By") { ParagraphFormat = new ParagraphFormat() { Alignment = GemBox.Document.HorizontalAlignment.Left } };
         row.Cells.Add(new TableCell(doc, para));
         para = new Paragraph(doc, txtTestedBy.Text) { ParagraphFormat = new ParagraphFormat() { Alignment = GemBox.Document.HorizontalAlignment.Left } };
         row.Cells.Add(new TableCell(doc, para));

         if (!string.IsNullOrEmpty(this.tbMxSuiteVersion.Text))
         {
            row = new TableRow(doc);
            table.Rows.Add(row);
            para = new Paragraph(doc, "Software Version") { ParagraphFormat = new ParagraphFormat() { Alignment = GemBox.Document.HorizontalAlignment.Left } };
            row.Cells.Add(new TableCell(doc, para));
            para = new Paragraph(doc, this.tbMxSuiteVersion.Text) { ParagraphFormat = new ParagraphFormat() { Alignment = GemBox.Document.HorizontalAlignment.Left } };
            row.Cells.Add(new TableCell(doc, para));
         }
         if (!string.IsNullOrEmpty(this.txtVstSerialNumber.Text))
         {
            row = new TableRow(doc);
            table.Rows.Add(row);
            row.Cells.Add(new TableCell(doc, new Paragraph(doc, "NI5644R Serial Number") { ParagraphFormat = new ParagraphFormat() { Alignment = GemBox.Document.HorizontalAlignment.Left } }));
            row.Cells.Add(new TableCell(doc, new Paragraph(doc, this.txtVstSerialNumber.Text) { ParagraphFormat = new ParagraphFormat() { Alignment = GemBox.Document.HorizontalAlignment.Left } }));
         }
         if (!string.IsNullOrEmpty(this.txtVstCalibrationDate.Text))
         {
            row = new TableRow(doc);
            table.Rows.Add(row);
            row.Cells.Add(new TableCell(doc, new Paragraph(doc, "NI5644R Calibration Date") { ParagraphFormat = new ParagraphFormat() { Alignment = GemBox.Document.HorizontalAlignment.Left } }));
            row.Cells.Add(new TableCell(doc, new Paragraph(doc, this.txtVstCalibrationDate.Text) { ParagraphFormat = new ParagraphFormat() { Alignment = GemBox.Document.HorizontalAlignment.Left } }));
         }
         if (!string.IsNullOrEmpty(this.txtAttenuatorSerialNumber.Text))
         {
            row = new TableRow(doc);
            table.Rows.Add(row);
            row.Cells.Add(new TableCell(doc, new Paragraph(doc, "NI5695 Serial Number") { ParagraphFormat = new ParagraphFormat() { Alignment = GemBox.Document.HorizontalAlignment.Left } }));
            row.Cells.Add(new TableCell(doc, new Paragraph(doc, this.txtAttenuatorSerialNumber.Text) { ParagraphFormat = new ParagraphFormat() { Alignment = GemBox.Document.HorizontalAlignment.Left } }));
         }
         if (!string.IsNullOrEmpty(this.txtSwitchSerialNumber.Text))
         {
            row = new TableRow(doc);
            table.Rows.Add(row);
            row.Cells.Add(new TableCell(doc, new Paragraph(doc, "NI2790 Serial Number") { ParagraphFormat = new ParagraphFormat() { Alignment = GemBox.Document.HorizontalAlignment.Left } }));
            row.Cells.Add(new TableCell(doc, new Paragraph(doc, this.txtSwitchSerialNumber.Text) { ParagraphFormat = new ParagraphFormat() { Alignment = GemBox.Document.HorizontalAlignment.Left } }));
         }
         if (!string.IsNullOrEmpty(this.txtModemPartNumber.Text))
         {
            row = new TableRow(doc);
            table.Rows.Add(row);
            row.Cells.Add(new TableCell(doc, new Paragraph(doc, "Modem Part Number") { ParagraphFormat = new ParagraphFormat() { Alignment = GemBox.Document.HorizontalAlignment.Left } }));
            row.Cells.Add(new TableCell(doc, new Paragraph(doc, this.txtModemPartNumber.Text) { ParagraphFormat = new ParagraphFormat() { Alignment = GemBox.Document.HorizontalAlignment.Left } }));
         }
         if (!string.IsNullOrEmpty(this.txtModemSerialNumber.Text))
         {
            row = new TableRow(doc);
            table.Rows.Add(row);
            row.Cells.Add(new TableCell(doc, new Paragraph(doc, "Modem Serial Number") { ParagraphFormat = new ParagraphFormat() { Alignment = GemBox.Document.HorizontalAlignment.Left } }));
            row.Cells.Add(new TableCell(doc, new Paragraph(doc, this.txtModemSerialNumber.Text) { ParagraphFormat = new ParagraphFormat() { Alignment = GemBox.Document.HorizontalAlignment.Left } }));
         }
         if (!string.IsNullOrEmpty(this.txtModemFirmwareRev.Text))
         {
            row = new TableRow(doc);
            table.Rows.Add(row);
            row.Cells.Add(new TableCell(doc, new Paragraph(doc, "Modem Firmware Version") { ParagraphFormat = new ParagraphFormat() { Alignment = GemBox.Document.HorizontalAlignment.Left } }));
            row.Cells.Add(new TableCell(doc, new Paragraph(doc, this.txtModemFirmwareRev.Text) { ParagraphFormat = new ParagraphFormat() { Alignment = GemBox.Document.HorizontalAlignment.Left } }));
         }
         if (!string.IsNullOrEmpty(this.txtProxyRev.Text))
         {
            row = new TableRow(doc);
            table.Rows.Add(row);
            row.Cells.Add(new TableCell(doc, new Paragraph(doc, "Proxy Software Version") { ParagraphFormat = new ParagraphFormat() { Alignment = GemBox.Document.HorizontalAlignment.Left } }));
            row.Cells.Add(new TableCell(doc, new Paragraph(doc, this.txtProxyRev.Text) { ParagraphFormat = new ParagraphFormat() { Alignment = GemBox.Document.HorizontalAlignment.Left } }));
         }
         mainSection.Blocks.Add(table);
      }
      private void CreateIterationTable(ref GemBox.Document.DocumentModel doc, ref Table table, List<string[]> parsedTestResults, int lineNumber)
      {
         try
         {
            TableMetadata metadata = table.Metadata;

            table.TableFormat.AutomaticallyResizeToFitContents = false;
            table.TableFormat.Alignment = GemBox.Document.HorizontalAlignment.Left;
            table.Columns.Add(new TableColumn() { PreferredWidth = 54 });
            table.Columns.Add(new TableColumn() { PreferredWidth = 54 });
            table.Columns.Add(new TableColumn() { PreferredWidth = 243 });
            table.Columns.Add(new TableColumn() { PreferredWidth = 54 });
            table.Columns.Add(new TableColumn() { PreferredWidth = 243 });

            while (lineNumber < parsedTestResults.Count)
            {
               if (parsedTestResults[lineNumber][0].ToUpper().Contains("ITERATION"))
               {
                  // Iteration #
                  string[] iteration = parsedTestResults[lineNumber++];
                  this.AddRowWithFiveColumnsAndMergeInto1Column(ref doc, ref table, iteration);
                  metadata.Title = iteration[0];
                  // channel Number
                  this.AddRowWithFiveColumnsAndMergeInto2Columns(ref doc, ref table, parsedTestResults[lineNumber++]);
                  // Data Rate
                  this.AddRowWithFiveColumnsAndMergeInto2Columns(ref doc, ref table, parsedTestResults[lineNumber++]);
                  // Tx Power
                  this.AddRowWithFiveColumnsAndMergeInto2Columns(ref doc, ref table, parsedTestResults[lineNumber++]);
               }
               this.AddRowWithFiveColumns(ref doc, ref table, parsedTestResults[lineNumber++]);
            }
         }
         catch
         {
            this.AppendErrorMessage = "Unable to add Iteration Information in report. Possible reasons:\n" +
                                 "1. 802.11 Tests: Regression was not completed, has errors or execution was aborted.\n" +
                                 "2. 1609.X Tests: Report summary files (csv) are not in proper format.";
         }
      }
      private void CreateIterationTable(ref GemBox.Document.DocumentModel doc, ref Table table, string scenarioName, int iteration, List<Tuple<string, object, string>> inputs, List<Tuple<string, object, string>> results, List<string> verdicts, List<string> additionalFiles)
      {
         List<string> testPurpose80211List = TestPurposes.TP80211;
         TableMetadata metadata = table.Metadata;
         metadata.Title = $"{scenarioName} Iteration #{iteration + 1}";

         table.TableFormat.AutomaticallyResizeToFitContents = false;
         table.TableFormat.Alignment = GemBox.Document.HorizontalAlignment.Left;
         table.Columns.Add(new TableColumn() { PreferredWidth = 2 * frmMxDsrcReportGenerator.WidthPointsPerInch });
         table.Columns.Add(new TableColumn() { PreferredWidth = frmMxDsrcReportGenerator.WidthPointsPerInch });
         table.Columns.Add(new TableColumn() { PreferredWidth = 6 * frmMxDsrcReportGenerator.WidthPointsPerInch });

         // Inputs
         Tuple<string, object, string> channelWidth = inputs.Find(item => item.Item1 == Processing80211RegressionResults.ChannelWidth);
         Tuple<string, object, string> dataRate = inputs.Find(item => item.Item1 == Processing80211RegressionResults.DataRate);
         Tuple<string, object, string> testChannelNumber = inputs.Find(item => item.Item1 == Processing80211RegressionResults.TestChannelNumber);
         Tuple<string, object, string> testTxChannelPower = inputs.Find(item => item.Item1 == Processing80211RegressionResults.TestChannelTxPower);

         Paragraph para = new Paragraph(doc, $"Iteration #{iteration + 1}") { ParagraphFormat = new ParagraphFormat() { Alignment = GemBox.Document.HorizontalAlignment.Center, Style = this.myCentreBoldStyle } };
         TableRow row = new TableRow(doc, new TableCell(doc, para) { ColumnSpan = 3, CellFormat = new TableCellFormat() { TextDirection = TableCellTextDirection.LeftToRight } });
         row.RowFormat.Height = new TableRowHeight(12, TableRowHeightRule.AtLeast);
         table.Rows.Add(row);

         // Inputs For scenarios TXT-PHY-BV-01 to 06
         string[] tableInputs = new string[1];
         string commentColumn = string.Empty;
         if (scenarioName != TestPurposes.TP80211TxtPhy07Name && testPurpose80211List.FindAll(item => item.Contains("TXT-PHY")).Exists(item => item.Equals(scenarioName)))
         {
            tableInputs = new string[] { $"pChannel: {testChannelNumber.Item2.ToString()}", $"pDataRate: {Enum.GetName(typeof(DataRate), System.Convert.ToInt32(dataRate.Item2))}", $"pTxPowerDefault: {testTxChannelPower.Item2.ToString()} dBm" };
         }
         // Inputs For scenarios TXT-PHY-BV-07
         else if (scenarioName == TestPurposes.TP80211TxtPhy07Name)
         {
            Tuple<string, object, string> txPowerFirst = inputs.Find(item => item.Item1 == Processing80211RegressionResults.TestChannelTxPower);
            Tuple<string, object, string> txPowerLast = inputs.FindLast(item => item.Item1 == Processing80211RegressionResults.TestChannelTxPower);
            tableInputs = new string[] { $"pChannel: {testChannelNumber.Item2.ToString()}", $"pDataRate: {Enum.GetName(typeof(DataRate), System.Convert.ToInt32(dataRate.Item2))}", $"pPower: {txPowerFirst.Item2.ToString()} dBm to {txPowerLast.Item2.ToString()} dBm" };
         }
         // Inputs For scenarios RXT-PHY-BV-01 to 04
         else if (scenarioName != TestPurposes.TP80211RxtPhy05Name && testPurpose80211List.FindAll(item => item.Contains("RXT-PHY")).Exists(item => item.Equals(scenarioName)))
         {
            Tuple<string, object, string> testRxChannelPower = inputs.Find(item => item.Item1 == Processing80211RegressionResults.TestChannelRxPower);
            if (scenarioName == TestPurposes.TP80211RxtPhy02Name || scenarioName == TestPurposes.TP80211RxtPhy03Name)
            {
               Tuple<string, object, string> interferingChannelNumber = inputs.Find(item => item.Item1 == Processing80211RegressionResults.InterferingChannelNumber);
               Tuple<string, object, string> interferingChannelPower = inputs.Find(item => item.Item1 == Processing80211RegressionResults.InterferingChannelPower);
               tableInputs = new string[] { $"pChannel: {testChannelNumber.Item2.ToString()}", $"pDataRate: {Enum.GetName(typeof(DataRate), System.Convert.ToInt32(dataRate.Item2))}", $"Rx Channel Power: {testRxChannelPower.Item2.ToString()} dBm",
                                            $"{interferingChannelNumber.Item1}: {interferingChannelNumber.Item2.ToString()}", $"{interferingChannelPower.Item1}: {interferingChannelPower.Item2.ToString()} {interferingChannelPower.Item3}"};
            }
            else
            {
               tableInputs = new string[] { $"pChannel: {testChannelNumber.Item2.ToString()}", $"pDataRate: {Enum.GetName(typeof(DataRate), System.Convert.ToInt32(dataRate.Item2))}", $"Rx Channel Power: {testRxChannelPower.Item2.ToString()} dBm" };
            }
         }
         // Inputs For scenarios RXT-PHY-BV-05
         else if (scenarioName == TestPurposes.TP80211RxtPhy05Name)
         {
            Tuple<string, object, string> rxPowerFirst = inputs.Find(item => item.Item1 == Processing80211RegressionResults.TestChannelRxPower);
            Tuple<string, object, string> rxPowerLast = inputs.FindLast(item => item.Item1 == Processing80211RegressionResults.TestChannelRxPower);
            tableInputs = new string[] { $"pChannel: {testChannelNumber.Item2.ToString()}", $"pDataRate: {Enum.GetName(typeof(DataRate), System.Convert.ToInt32(dataRate.Item2))}", $"pPower: {rxPowerFirst.Item2.ToString()} dBm to {rxPowerLast.Item2.ToString()} dBm" };
         }
         else
         {
            tableInputs = new string[] { $"pChannel: {testChannelNumber.Item2.ToString()}", $"pDataRate: {Enum.GetName(typeof(DataRate), System.Convert.ToInt32(dataRate.Item2))}" };
         }

         if (scenarioName != TestPurposes.TP80211TxtPhy07Name && scenarioName != TestPurposes.TP80211RxtPhy05Name)
         {
            foreach (Tuple<string, object, string> item in results)
            {
               commentColumn += $"{item.Item1} = {item.Item2.ToString()} {item.Item3}\n";
            }
         }
         this.AddIterationData(ref doc, ref table, tableInputs, verdicts[iteration], commentColumn);

         List<string> destinationFiles = new List<string>(additionalFiles.Count());
         string logDirectory = string.Empty;
         foreach (string file in additionalFiles)
         {
            string fileName = Path.GetFileName(file);
            logDirectory = System.IO.Directory.CreateDirectory(Path.Combine(this.myReportsDirectory, $"{scenarioName} Logs")).FullName;
            string destinationFile = Path.Combine(logDirectory, fileName);
            destinationFiles.Add(destinationFile);
            File.Copy(file, destinationFile, true);

            if (fileName.Contains("_Trace"))
            {
               this.CreateGraph(destinationFile);
            }
         }
         if (destinationFiles.Count() != 0 && !string.IsNullOrEmpty(logDirectory))
         {
            Paragraph filehyperlink = new Paragraph(doc, new Hyperlink(doc, $".\\{scenarioName} Logs", $"{scenarioName} Logs")) { ParagraphFormat = new ParagraphFormat() { Alignment = GemBox.Document.HorizontalAlignment.Center } };
            table.Rows.Add(new TableRow(doc, new TableCell(doc, filehyperlink) { ColumnSpan = 3 }));
         }
      }
      private ReportStatus CreateNewCertificationReport()
      {
         try
         {
            this.myDocumentModel.DefaultCharacterFormat.Size = 11;
            this.myDocumentModel.DefaultCharacterFormat.FontName = "Calibri";

            this.CreateTitlePage(ref myDocumentModel);
            this.CreateTableOfContents(ref myDocumentModel);
            this.myDocumentMainSection = this.CreateSectionWithHeaderAndFooter(ref this.myDocumentModel, this.myLabLogoFile);
            this.myDocumentMainSection.PageSetup.PageMargins = this.SetDocumentMargines();
            this.myDocumentMainSection.PageSetup = this.SetDocumentPageProperties();
            this.myDocumentModel.Sections.Add(this.myDocumentMainSection);
            this.myDocumentMainSection.Blocks.Add(new GemBox.Document.Paragraph(this.myDocumentModel, "Certification Test Information Table") { ParagraphFormat = { Style = this.myHeading1Style } });

            this.CreateInformationTable(ref this.myDocumentModel, ref this.myDocumentMainSection);
            this.myDocumentMainSection.Blocks.Add(new GemBox.Document.Paragraph(this.myDocumentModel, new GemBox.Document.SpecialCharacter(this.myDocumentModel, GemBox.Document.SpecialCharacterType.LineBreak)));

            this.myDocumentMainSection.Blocks.Add(new GemBox.Document.Paragraph(this.myDocumentModel, "DSRC Standards Table") { ParagraphFormat = { Style = this.myHeading1Style } });
            this.CreateSpecificationsTable(ref this.myDocumentModel, ref this.myDocumentMainSection);
            this.myDocumentMainSection.Blocks.Add(new GemBox.Document.Paragraph(this.myDocumentModel, new GemBox.Document.SpecialCharacter(this.myDocumentModel, GemBox.Document.SpecialCharacterType.PageBreak)));

            this.AddPrologue(ref this.myDocumentModel, ref this.myDocumentMainSection);
            this.myDocumentMainSection.Blocks.Add(new GemBox.Document.Paragraph(this.myDocumentModel, new GemBox.Document.SpecialCharacter(this.myDocumentModel, GemBox.Document.SpecialCharacterType.PageBreak)));

            if (this.cmbSpecification.SelectedIndex != (int)Specifications.WAVE80211)
            {
               RegressionResults regressionResults = new RegressionResults(this.myRegressionReportFolder, (Specifications)this.cmbSpecification.SelectedIndex);
               switch (this.cmbSpecification.SelectedIndex)
               {
                  case (int)Specifications.SAEJ29451OnboardSystemRequirementsforV2VSafetyCommunications:
                     {
                        this.myDocumentMainSection.Blocks.Add(new GemBox.Document.Paragraph(this.myDocumentModel, frmMxDsrcReportGenerator.SummaryTableTitleJ2945) { ParagraphFormat = { Style = this.myHeading1Style } });
                        this.CreateSummaryTable(ref this.myDocumentModel, ref this.myDocumentMainSection, frmMxDsrcReportGenerator.SummaryTableTitleJ2945, regressionResults.SummaryResults, Specifications.SAEJ29451OnboardSystemRequirementsforV2VSafetyCommunications);
                     }
                     break;

                  case (int)Specifications.WAVEMultiChannelOperation:
                     {
                        this.myDocumentMainSection.Blocks.Add(new GemBox.Document.Paragraph(this.myDocumentModel, frmMxDsrcReportGenerator.SummaryTableTitle16094) { ParagraphFormat = { Style = this.myHeading1Style } });
                        this.CreateSummaryTable(ref this.myDocumentModel, ref this.myDocumentMainSection, frmMxDsrcReportGenerator.SummaryTableTitle16094, regressionResults.SummaryResults, Specifications.WAVEMultiChannelOperation);
                     }
                     break;

                  case (int)Specifications.WAVENetworkingServices:
                     {
                        this.myDocumentMainSection.Blocks.Add(new GemBox.Document.Paragraph(this.myDocumentModel, frmMxDsrcReportGenerator.SummaryTableTitle16093) { ParagraphFormat = { Style = this.myHeading1Style } });
                        this.CreateSummaryTable(ref this.myDocumentModel, ref this.myDocumentMainSection, frmMxDsrcReportGenerator.SummaryTableTitle16093, regressionResults.SummaryResults, Specifications.WAVENetworkingServices);
                     }
                     break;

                  case (int)Specifications.WAVESecurityServices:
                     {
                        this.myDocumentMainSection.Blocks.Add(new GemBox.Document.Paragraph(this.myDocumentModel, frmMxDsrcReportGenerator.SummaryTableTitle16092) { ParagraphFormat = { Style = this.myHeading1Style } });
                        this.CreateSummaryTable(ref this.myDocumentModel, ref this.myDocumentMainSection, frmMxDsrcReportGenerator.SummaryTableTitle16092, regressionResults.SummaryResults, Specifications.WAVESecurityServices);
                     }
                     break;

                  case (int)Specifications.WAVEV2ITestSuite:
                     {
                        this.myDocumentMainSection.Blocks.Add(new GemBox.Document.Paragraph(this.myDocumentModel, frmMxDsrcReportGenerator.SummaryTableTitleV2I) { ParagraphFormat = { Style = this.myHeading1Style } });
                        this.CreateSummaryTable(ref this.myDocumentModel, ref this.myDocumentMainSection, frmMxDsrcReportGenerator.SummaryTableTitleV2I, regressionResults.SummaryResults, Specifications.WAVEV2ITestSuite);
                     }
                     break;
               }

               this.GetExpandedTestCaseResults(ref this.myDocumentModel, ref this.myDocumentMainSection, regressionResults);
            }
            else
            {
               List<string> runTimeErrors = new List<string>();
               Processing80211RegressionResults regressionResults80211 = new Processing80211RegressionResults(this.myRegressionReportFolder, ref runTimeErrors);
               this.myDocumentMainSection.Blocks.Add(new GemBox.Document.Paragraph(this.myDocumentModel, frmMxDsrcReportGenerator.SummaryTableTitle80211) { ParagraphFormat = { Style = this.myHeading1Style } });
               this.CreateSummaryTable(ref this.myDocumentModel, ref this.myDocumentMainSection, frmMxDsrcReportGenerator.SummaryTableTitle80211, regressionResults80211.ScenarioResults, Specifications.WAVE80211);

               int progress = 0;
               foreach (MxDSRCReportGenerator.ResultSummary summary in regressionResults80211.ScenarioResults)
               {
                  List<List<Tuple<string, object, string>>> iterationInputs;
                  List<List<Tuple<string, object, string>>> iterationResults;
                  List<string> iterationVerdict;

                  regressionResults80211.GetIterationInformation(summary.ScenarioName, out iterationInputs, out iterationResults, out iterationVerdict);
                  List<Tuple<string, object>> testCaseInputs = regressionResults80211.GetTestCaseInputs(summary.ScenarioName);
                  this.CreateTestResultsTable(ref this.myDocumentModel, ref this.myDocumentMainSection, summary, testCaseInputs);

                  for (int iteration = 0; iteration < iterationInputs.Count; iteration++)
                  {
                     // Iteration Table
                     Table iterationTable = new Table(this.myDocumentModel);
                     List<string> additionalFiles = regressionResults80211.GetLogFilesFromMxOutFolder(summary.ScenarioName, iterationInputs[iteration], iteration);
                     this.CreateIterationTable(ref this.myDocumentModel, ref iterationTable, summary.ScenarioName, iteration, iterationInputs[iteration], iterationResults[iteration], iterationVerdict, additionalFiles);
                     this.myDocumentMainSection.Blocks.Add(iterationTable);
                  }
                  this.ProgressLabel = (++progress * 100) / regressionResults80211.ScenarioResults.Count;
               }

               foreach (string error in runTimeErrors)
               {
                  this.AppendErrorMessage = error;
               }
            }

            this.PopulateTableOfContents(ref this.myDocumentModel);

            return ReportStatus.Success;
         }
         catch (Exception e)
         {
            this.AppendErrorMessage = "Error creating report. Possible reasons:\n" +
                                 "1. 802.11 Tests: Regression was not completed, has errors or execution was aborted.\n" +
                                 "2. 1609.X Tests: Report summary files (csv) are not in proper format.";
            return ReportStatus.Failure;
         }
      }
      private GemBox.Document.Section CreateSectionWithHeaderAndFooter(ref GemBox.Document.DocumentModel doc, string logoPath)
      {
         try
         {
            GemBox.Document.Section section = new GemBox.Document.Section(doc);

            // Header
            GemBox.Document.Paragraph labInfo = new GemBox.Document.Paragraph(doc, new GemBox.Document.Run(doc, txtLabName.Text), new GemBox.Document.SpecialCharacter(doc, GemBox.Document.SpecialCharacterType.LineBreak));
            labInfo.ParagraphFormat.Alignment = GemBox.Document.HorizontalAlignment.Left;
            labInfo.ParagraphFormat.LineSpacing = 1;

            if (File.Exists(logoPath))
            {
               Picture picture = new Picture(doc, logoPath);
               picture.Layout = new FloatingLayout(new HorizontalPosition(HorizontalPositionType.Right, HorizontalPositionAnchor.Margin),
                                                   new VerticalPosition(0.4, LengthUnit.Inch, VerticalPositionAnchor.TopMargin), picture.Layout.Size)
               {
                  WrappingStyle = TextWrappingStyle.Square
               };

               labInfo.Inlines.Add(picture);
            }

            section.HeadersFooters.Add(new GemBox.Document.HeaderFooter(doc, GemBox.Document.HeaderFooterType.HeaderDefault, labInfo));

            // Footer
            string date = "Report Date: " + DateTime.Now.ToString("M/d/yyyy");
            GemBox.Document.Paragraph reportInfo = new GemBox.Document.Paragraph(doc, new GemBox.Document.Run(doc, "DSRC Certification Test Report"), new GemBox.Document.SpecialCharacter(doc, GemBox.Document.SpecialCharacterType.LineBreak), new GemBox.Document.Run(doc, date));
            reportInfo.ParagraphFormat.Alignment = GemBox.Document.HorizontalAlignment.Left;
            reportInfo.ParagraphFormat.LineSpacing = 1;

            GemBox.Document.Paragraph pageInfo = new Paragraph(doc, new Run(doc, "Page "), new Field(doc, FieldType.Page), new Run(doc, " of "), new Field(doc, FieldType.NumPages));
            pageInfo.ParagraphFormat.Alignment = GemBox.Document.HorizontalAlignment.Center;
            pageInfo.ParagraphFormat.LineSpacing = 1;

            section.HeadersFooters.Add(new GemBox.Document.HeaderFooter(doc, GemBox.Document.HeaderFooterType.FooterDefault, pageInfo, reportInfo));
            return section;
         }
         catch
         {
            this.AppendErrorMessage = "Unable to create Secion with Header and Footer in report. Possible reasons:\n" +
                                 "1. 802.11 Tests: Regression was not completed, has errors or execution was aborted.\n" +
                                 "2. 1609.X Tests: Report summary files (csv) are not in proper format.";
         }
         return null;
      }
      private void CreateSpecificationsTable(ref GemBox.Document.DocumentModel doc, ref Section mainSection)
      {
         Table table = new Table(doc);
         TableMetadata metadata = table.Metadata;
         metadata.Title = "SpecificationsTable";
         table.TableFormat.AutomaticallyResizeToFitContents = false;
         table.TableFormat.PreferredWidth = new TableWidth(9 * frmMxDsrcReportGenerator.WidthPointsPerInch, TableWidthUnit.Point);
         table.Columns.Add(new TableColumn() { PreferredWidth = 6 * frmMxDsrcReportGenerator.WidthPointsPerInch });
         table.Columns.Add(new TableColumn() { PreferredWidth = 2 * frmMxDsrcReportGenerator.WidthPointsPerInch });
         table.Columns.Add(new TableColumn() { PreferredWidth = frmMxDsrcReportGenerator.WidthPointsPerInch });

         // Row 1
         TableRow row = new TableRow(doc) { RowFormat = new TableRowFormat() { Height = new TableRowHeight(12, TableRowHeightRule.AtLeast) } };
         table.Rows.Add(row);

         Paragraph content = new Paragraph(doc, "Test Specifications") { ParagraphFormat = new ParagraphFormat() { Alignment = GemBox.Document.HorizontalAlignment.Center, Style = this.myTableHeadingStyle } };
         row.Cells.Add(new TableCell(doc, content) { ColumnSpan = 3, CellFormat = new TableCellFormat() { TextDirection = TableCellTextDirection.LeftToRight } });

         // Row 2
         row = new TableRow(doc) { RowFormat = new TableRowFormat() { Height = new TableRowHeight(12, TableRowHeightRule.AtLeast) } };
         table.Rows.Add(row);

         content = new Paragraph(doc, "Title") { ParagraphFormat = new ParagraphFormat() { Alignment = GemBox.Document.HorizontalAlignment.Center, Style = this.myTableHeadingStyle } };
         row.Cells.Add(new TableCell(doc, content) { CellFormat = new TableCellFormat() { TextDirection = TableCellTextDirection.LeftToRight, PreferredWidth = new TableWidth(70, TableWidthUnit.Percentage) } });

         content = new Paragraph(doc, "Mnemonic") { ParagraphFormat = new ParagraphFormat() { Alignment = GemBox.Document.HorizontalAlignment.Center, Style = this.myTableHeadingStyle } };
         row.Cells.Add(new TableCell(doc, content) { CellFormat = new TableCellFormat() { TextDirection = TableCellTextDirection.LeftToRight, PreferredWidth = new TableWidth(20, TableWidthUnit.Percentage) } });

         content = new Paragraph(doc, "Rev") { ParagraphFormat = new ParagraphFormat() { Alignment = GemBox.Document.HorizontalAlignment.Center, Style = this.myTableHeadingStyle } };
         row.Cells.Add(new TableCell(doc, content) { CellFormat = new TableCellFormat() { TextDirection = TableCellTextDirection.LeftToRight, PreferredWidth = new TableWidth(10, TableWidthUnit.Percentage) } });

         // Row 3
         row = new TableRow(doc) { RowFormat = new TableRowFormat() { Height = new TableRowHeight(12, TableRowHeightRule.AtLeast) } };
         table.Rows.Add(row);
         content = new Paragraph(doc, this.cmbSpecification.SelectedItem.ToString()) { ParagraphFormat = new ParagraphFormat() { Alignment = GemBox.Document.HorizontalAlignment.Left, Style = this.myTableCellStyle } };
         row.Cells.Add(new TableCell(doc, content) { CellFormat = new TableCellFormat() { TextDirection = TableCellTextDirection.LeftToRight, PreferredWidth = new TableWidth(70, TableWidthUnit.Percentage) } });

         content = new Paragraph(doc, this.tbMnemonic.Text) { ParagraphFormat = new ParagraphFormat() { Alignment = GemBox.Document.HorizontalAlignment.Left, Style = this.myTableCellStyle } };
         row.Cells.Add(new TableCell(doc, content) { CellFormat = new TableCellFormat() { TextDirection = TableCellTextDirection.LeftToRight, PreferredWidth = new TableWidth(20, TableWidthUnit.Percentage) } });

         /*710*/
         content = new Paragraph(doc, this.tbRevision.Text) { ParagraphFormat = new ParagraphFormat() { Alignment = GemBox.Document.HorizontalAlignment.Left, Style = this.myTableCellStyle } };
         row.Cells.Add(new TableCell(doc, content) { CellFormat = new TableCellFormat() { TextDirection = TableCellTextDirection.LeftToRight, PreferredWidth = new TableWidth(10, TableWidthUnit.Percentage) } });

         mainSection.Blocks.Add(table);
      }
      private void CreateSummaryTable(ref GemBox.Document.DocumentModel doc, ref Section mainSection, string tableTitle, List<MxDSRCReportGenerator.ResultSummary> results, Specifications specification)
      {
         try
         {
            Table table = new Table(doc);
            table.TableFormat.AutomaticallyResizeToFitContents = false;
            TableMetadata metadata = table.Metadata;
            metadata.Title = tableTitle;

            if (specification == Specifications.WAVE80211)
            {
               table.TableFormat.PreferredWidth = new TableWidth(9 * frmMxDsrcReportGenerator.WidthPointsPerInch, TableWidthUnit.Point);
               table.Columns.Add(new TableColumn() { PreferredWidth = 6 * frmMxDsrcReportGenerator.WidthPointsPerInch });
               table.Columns.Add(new TableColumn() { PreferredWidth = 3 * frmMxDsrcReportGenerator.WidthPointsPerInch });

               for (int i = 0; i < results.Count; i++)
               {
                  TableRow row = new TableRow(doc) { RowFormat = new TableRowFormat() { Height = new TableRowHeight(12, TableRowHeightRule.AtLeast) } };
                  table.Rows.Add(row);

                  Paragraph para = new Paragraph(doc, results[i].ScenarioName) { ParagraphFormat = new ParagraphFormat() { Alignment = GemBox.Document.HorizontalAlignment.Left, Style = this.myTableCellStyle } };
                  TableCell cell = new TableCell(doc, para) { CellFormat = new TableCellFormat() { TextDirection = TableCellTextDirection.LeftToRight, PreferredWidth = new TableWidth(60, TableWidthUnit.Percentage) } };
                  row.Cells.Add(cell);

                  para = new Paragraph(doc, results[i].Result);
                  para.ParagraphFormat.Alignment = GemBox.Document.HorizontalAlignment.Left;
                  if (results[i].Result.ToUpper().Equals("PASS"))
                  {
                     para.ParagraphFormat.Style = this.myGreenCellStyle;
                  }
                  else
                  {
                     if (results[i].Result.ToUpper().Equals("FAIL"))
                     {
                        para.ParagraphFormat.Style = this.myRedCellStyle;
                     }
                     else
                     {
                        para.ParagraphFormat.Style = this.myBlueCellStyle;
                     }
                  }
                  row.Cells.Add(new TableCell(doc, para) { CellFormat = new TableCellFormat() { TextDirection = TableCellTextDirection.LeftToRight, PreferredWidth = new TableWidth(40, TableWidthUnit.Percentage) } });
               }
            }
            else
            {
               table.Columns.Add(new TableColumn() { PreferredWidth = 330 });
               table.Columns.Add(new TableColumn() { PreferredWidth = 126 });

               for (int i = 0; i < results.Count; i++)
               {
                  TableRow row = new TableRow(doc) { RowFormat = new TableRowFormat() { Height = new TableRowHeight(12, TableRowHeightRule.AtLeast) } };
                  table.Rows.Add(row);

                  Paragraph para = new Paragraph(doc, results[i].ScenarioName) { ParagraphFormat = new ParagraphFormat() { Alignment = GemBox.Document.HorizontalAlignment.Left, Style = this.myTableCellStyle } };
                  row.Cells.Add(new TableCell(doc, para) { CellFormat = new TableCellFormat() { TextDirection = TableCellTextDirection.LeftToRight } });

                  para = new Paragraph(doc, results[i].Result) { ParagraphFormat = new ParagraphFormat() { Alignment = GemBox.Document.HorizontalAlignment.Left } };
                  if (results[i].Result.ToUpper().Equals("PASS"))
                  {
                     para.ParagraphFormat.Style = this.myGreenCellStyle;
                  }
                  else
                  {
                     if (results[i].Result.ToUpper().Equals("FAIL"))
                     {
                        para.ParagraphFormat.Style = this.myRedCellStyle;
                     }
                     else
                     {
                        para.ParagraphFormat.Style = this.myBlueCellStyle;
                     }
                  }
                  row.Cells.Add(new TableCell(doc, para) { CellFormat = new TableCellFormat() { TextDirection = TableCellTextDirection.LeftToRight } });
               }
            }
            mainSection.Blocks.Add(table);
         }
         catch
         {
            this.AppendErrorMessage = "Unable to add Summary Table in report. Possible reasons:\n" +
                                 "1. 802.11 Tests: Regression was not completed, has errors or execution was aborted.\n" +
                                 "2. 1609.X Tests: Report summary files (csv) are not in proper format.";
         }
      }
      private void CreateTableOfContents(ref GemBox.Document.DocumentModel doc)
      {
         GemBox.Document.ParagraphStyle tocHeading = (GemBox.Document.ParagraphStyle)GemBox.Document.Style.CreateStyle(GemBox.Document.StyleTemplateType.Heading1, doc);
         tocHeading.Name = "toc";
         tocHeading.ParagraphFormat.OutlineLevel = GemBox.Document.OutlineLevel.BodyText;
         doc.Styles.Add(tocHeading);

         GemBox.Document.Section section = new GemBox.Document.Section(doc);
         section.PageSetup.PageMargins = this.SetDocumentMargines();
         section.PageSetup = this.SetDocumentPageProperties();

         doc.Sections.Add(section);
         section.Blocks.Add(new GemBox.Document.Paragraph(doc, "Table of Contents") { ParagraphFormat = { Style = tocHeading } });
         section.Blocks.Add(new GemBox.Document.TableOfEntries(doc, GemBox.Document.FieldType.TOC));
         section.Blocks.Add(new GemBox.Document.Paragraph(doc, new GemBox.Document.SpecialCharacter(doc, GemBox.Document.SpecialCharacterType.PageBreak)));
      }
      private void CreateTestResultsTable(ref GemBox.Document.DocumentModel doc, ref Section mainSection, MxDSRCReportGenerator.ResultSummary summary, List<Tuple<string, object>> testCaseInputs)
      {
         TestPurposes testPurposesInstance = new TestPurposes();

         mainSection.Blocks.Add(new GemBox.Document.Paragraph(doc, summary.ScenarioName) { ParagraphFormat = { Style = this.myHeading1Style } });

         Table table = new Table(doc);
         TableMetadata metaData = table.Metadata;
         metaData.Title = summary.ScenarioName;
         table.TableFormat.AutomaticallyResizeToFitContents = false;
         table.Columns.Add(new TableColumn() { PreferredWidth = (1 * frmMxDsrcReportGenerator.WidthPointsPerInch) });
         table.Columns.Add(new TableColumn() { PreferredWidth = (8 * frmMxDsrcReportGenerator.WidthPointsPerInch) });
         this.AddRowWithTwoColumns(ref doc, ref table, new string[] { "Summary", testPurposesInstance.Get802dot11TestPurposeSummary(summary.ScenarioName) });
         mainSection.Blocks.Add(table);

         // Test Step, Description, Verdict and Results
         table = new Table(doc);
         TableMetadata metaData2 = table.Metadata;
         metaData2.Title = summary.ScenarioName + " TestStep";
         table.TableFormat.AutomaticallyResizeToFitContents = false;
         table.Columns.Add(new TableColumn() { PreferredWidth = (1 * frmMxDsrcReportGenerator.WidthPointsPerInch) });
         table.Columns.Add(new TableColumn() { PreferredWidth = (1 * frmMxDsrcReportGenerator.WidthPointsPerInch) });
         table.Columns.Add(new TableColumn() { PreferredWidth = (7 * frmMxDsrcReportGenerator.WidthPointsPerInch) });
         this.AddTestStepOrDescription(ref doc, ref table, new string[] { "Step", "Type", "Description" });
         if (summary.ScenarioName == TestPurposes.TP80211RxtPhy05Name)
         {
            this.AddTestStepOrDescription(ref doc, ref table, new string[] {testPurposesInstance.Get802dot11TestPurposeVerificationStep(summary.ScenarioName)[0],
                                                                                    "Verify",
                                                                                    testPurposesInstance.Get802dot11TestPurposeVerificationStepDescription(summary.ScenarioName)[0]});
         }
         else if (summary.ScenarioName == TestPurposes.TP80211TxtPhy07Name)
         {
            this.AddTestStepOrDescription(ref doc, ref table, new string[] {testPurposesInstance.Get802dot11TestPurposeVerificationStep(summary.ScenarioName)[0],
                                                                                    "Verify",
                                                                                    testPurposesInstance.Get802dot11TestPurposeVerificationStepDescription(summary.ScenarioName)[0]});

            this.AddTestStepOrDescription(ref doc, ref table, new string[] {testPurposesInstance.Get802dot11TestPurposeVerificationStep(summary.ScenarioName)[1],
                                                                                    "Verify",
                                                                                    testPurposesInstance.Get802dot11TestPurposeVerificationStepDescription(summary.ScenarioName)[1]});
         }
         else
         {
            this.AddTestStepOrDescription(ref doc, ref table, new string[] {testPurposesInstance.Get802dot11TestPurposeVerificationStep(summary.ScenarioName)[0],
                                                                                    "Verify",
                                                                                    testPurposesInstance.Get802dot11TestPurposeVerificationStepDescription(summary.ScenarioName)[0]});
         }
         mainSection.Blocks.Add(table);

         // Test Case Verdict
         table = new Table(doc);
         TableMetadata metaData3 = table.Metadata;
         metaData3.Title = summary.ScenarioName + " Verdict";
         table.TableFormat.AutomaticallyResizeToFitContents = false;
         table.Columns.Add(new TableColumn() { PreferredWidth = (2 * frmMxDsrcReportGenerator.WidthPointsPerInch) }); // 2 inch
         table.Columns.Add(new TableColumn() { PreferredWidth = (7 * frmMxDsrcReportGenerator.WidthPointsPerInch) }); // 7 inch
         this.AddRowWithTwoColumns(ref doc, ref table, new string[] { "Test Case Verdict", summary.Result });

         // Test Case Inputs
         this.AddRowWithTwoColumnsAndMerge(ref doc, ref table, new string[] { "Test Case Inputs" });
         foreach (Tuple<string, object> item in testCaseInputs)
         {
            if (item.Item1.ToLowerInvariant().Contains("slot"))
            {
               this.AddRowWithTwoColumns(ref doc, ref table, new string[] { item.Item1, Enum.GetName(typeof(TimeSlot), System.Convert.ToInt32(item.Item2)) });
            }
            else
            {
               this.AddRowWithTwoColumns(ref doc, ref table, new string[] { item.Item1, item.Item2.ToString() });
            }
         }

         mainSection.Blocks.Add(table);
      }
      private void CreateTestResultsTable(ref GemBox.Document.DocumentModel doc, ref Section mainSection, List<string[]> parsedTestResults)
      {
         int lineNumber = 0;
         try
         {
            mainSection.Blocks.Add(new GemBox.Document.Paragraph(doc, parsedTestResults[lineNumber++][0]) { ParagraphFormat = { Style = myHeading1Style } });

            Table table = new Table(doc);
            TableMetadata metaData = table.Metadata;
            metaData.Title = parsedTestResults[0][0];
            table.TableFormat.AutomaticallyResizeToFitContents = false;
            table.Columns.Add(new TableColumn() { PreferredWidth = (1 * frmMxDsrcReportGenerator.WidthPointsPerInch) });
            table.Columns.Add(new TableColumn() { PreferredWidth = (8 * frmMxDsrcReportGenerator.WidthPointsPerInch) });

            while (!parsedTestResults[lineNumber][0].ToUpper().Contains("ITERATION"))
            {
               if (parsedTestResults[lineNumber][0].ToUpper().Contains("TEST CASE INPUTS"))
               {
                  this.AddRowWithTwoColumnsAndMerge(ref doc, ref table, parsedTestResults[lineNumber++]);
               }
               else
               {
                  this.AddRowWithTwoColumns(ref doc, ref table, parsedTestResults[lineNumber++]);
               }
            }

            mainSection.Blocks.Add(table);

            Table iterationTable = new Table(doc);
            this.CreateIterationTable(ref doc, ref iterationTable, parsedTestResults, lineNumber);
            mainSection.Blocks.Add(iterationTable);
         }
         catch
         {
            this.AppendErrorMessage = "Unable to add Test Results Table in report. Possible reasons: 1609.X Tests: Report summary files (csv) are not in proper format.";
         }
      }
      private void CreateTitlePage(ref GemBox.Document.DocumentModel doc)
      {
         GemBox.Document.Paragraph para1 = new GemBox.Document.Paragraph(doc,
                 new GemBox.Document.Run(doc, ""), new GemBox.Document.SpecialCharacter(doc, GemBox.Document.SpecialCharacterType.LineBreak),
                 new GemBox.Document.Run(doc, ""), new GemBox.Document.SpecialCharacter(doc, GemBox.Document.SpecialCharacterType.LineBreak),
                 new GemBox.Document.Run(doc, ""), new GemBox.Document.SpecialCharacter(doc, GemBox.Document.SpecialCharacterType.LineBreak),
                 new GemBox.Document.Run(doc, ""), new GemBox.Document.SpecialCharacter(doc, GemBox.Document.SpecialCharacterType.LineBreak),
                 new GemBox.Document.Run(doc, ""), new GemBox.Document.SpecialCharacter(doc, GemBox.Document.SpecialCharacterType.LineBreak),
                 new GemBox.Document.Run(doc, "DSRC Certification Test Report"), new GemBox.Document.SpecialCharacter(doc, GemBox.Document.SpecialCharacterType.LineBreak));
         GemBox.Document.ParagraphStyle style = new GemBox.Document.ParagraphStyle("para1Style") { CharacterFormat = new CharacterFormat() { Bold = true, FontName = "Calibri", Size = 28D } };
         doc.Styles.Add(style);
         para1.ParagraphFormat.Style = style;
         para1.ParagraphFormat.Alignment = GemBox.Document.HorizontalAlignment.Center;

         GemBox.Document.Paragraph para2 = new GemBox.Document.Paragraph(doc,
                 new GemBox.Document.Run(doc, "Requested by: " + txtApplicantName.Text), new GemBox.Document.SpecialCharacter(doc, GemBox.Document.SpecialCharacterType.LineBreak),
                 new GemBox.Document.Run(doc, txtApplicantAddress.Text.Replace('\r', ' ').Replace('\n', ' ')), new GemBox.Document.SpecialCharacter(doc, GemBox.Document.SpecialCharacterType.LineBreak));
         GemBox.Document.ParagraphStyle style2 = new GemBox.Document.ParagraphStyle("para2Style") { CharacterFormat = new CharacterFormat() { Bold = true, FontName = "Calibri", Size = 18D } };
         doc.Styles.Add(style2);
         para2.ParagraphFormat.Style = style2;
         para2.ParagraphFormat.Alignment = GemBox.Document.HorizontalAlignment.Center;

         GemBox.Document.Section section = new GemBox.Document.Section(doc);
         section.PageSetup.PageMargins = this.SetDocumentMargines();
         section.PageSetup = this.SetDocumentPageProperties();
         section.Blocks.Add(para1);
         section.Blocks.Add(para2);
         section.Blocks.Add(new GemBox.Document.Paragraph(doc, new GemBox.Document.SpecialCharacter(doc, GemBox.Document.SpecialCharacterType.PageBreak)));
         doc.Sections.Add(section);
      }
      private bool FileInUse(string path)
      {
         try
         {
            //if file is not locked then below statement will successfully executed otherwise it's goes to catch.
            using (FileStream fs = new FileStream(path, FileMode.OpenOrCreate, FileAccess.Read))
            {
            }
            return false;
         }
         catch (IOException)
         {
            return true;
         }
      }
      private void GetExpandedTestCaseResults(ref GemBox.Document.DocumentModel doc, ref Section mainSection, RegressionResults results)
      {
         List<string> listOfFileNames = results.TestResultsFiles;
         try
         {
            for (int j = 0; j < listOfFileNames.Count; j++)
            {
               string fileName = listOfFileNames[j];
               // the parsing is in ParsedTestResults
               results.ParseTestResultsFromAFile(listOfFileNames[j]);
               // Create the test case result table
               this.CreateTestResultsTable(ref doc, ref mainSection, results.ParsedTestResults);
               // Loop through the iteration table

               this.ProgressLabel = (++j * 100) / listOfFileNames.Count;
            }
         }
         catch
         {
            this.AppendErrorMessage = "Unable to parse 1609.X Test Report summary files (csv) are not in proper format.";
         }
      }
      private void InitializeDocumentModel()
      {
         // add styles here
         this.myDocumentModel = new GemBox.Document.DocumentModel();
         this.myHeading1Style = (GemBox.Document.ParagraphStyle)Style.CreateStyle(StyleTemplateType.Heading1, this.myDocumentModel);
         this.myDocumentModel.Styles.Add(this.myHeading1Style);

         this.myTableHeadingStyle = new GemBox.Document.ParagraphStyle("tableHeadingStyle");
         this.myTableHeadingStyle.CharacterFormat = new GemBox.Document.CharacterFormat();
         this.myTableHeadingStyle.CharacterFormat.Bold = true;
         this.myTableHeadingStyle.CharacterFormat.FontName = "Calibri";
         this.myTableHeadingStyle.CharacterFormat.Size = 14D;
         this.myDocumentModel.Styles.Add(this.myTableHeadingStyle);

         this.myTableCellStyle = new GemBox.Document.ParagraphStyle("tableCellStyle");
         this.myTableCellStyle.CharacterFormat = new GemBox.Document.CharacterFormat();
         this.myTableCellStyle.CharacterFormat.Bold = false;
         this.myTableCellStyle.CharacterFormat.FontName = "Calibri";
         this.myTableCellStyle.CharacterFormat.Size = 11D;
         this.myDocumentModel.Styles.Add(this.myTableCellStyle);

         this.myRedCellStyle = new GemBox.Document.ParagraphStyle("tableRedCellStyle");
         this.myRedCellStyle.CharacterFormat = new GemBox.Document.CharacterFormat();
         this.myRedCellStyle.CharacterFormat.Bold = true;
         this.myRedCellStyle.CharacterFormat.FontName = "Calibri";
         this.myRedCellStyle.CharacterFormat.Size = 11D;
         this.myRedCellStyle.CharacterFormat.FontColor = GemBox.Document.Color.Red;
         this.myDocumentModel.Styles.Add(this.myRedCellStyle);

         this.myGreenCellStyle = new GemBox.Document.ParagraphStyle("tableGreenCellStyle");
         this.myGreenCellStyle.CharacterFormat = new GemBox.Document.CharacterFormat();
         this.myGreenCellStyle.CharacterFormat.Bold = true;
         this.myGreenCellStyle.CharacterFormat.FontName = "Calibri";
         this.myGreenCellStyle.CharacterFormat.Size = 11D;
         this.myGreenCellStyle.CharacterFormat.FontColor = GemBox.Document.Color.DarkGreen;
         this.myDocumentModel.Styles.Add(this.myGreenCellStyle);

         this.myBlueCellStyle = new GemBox.Document.ParagraphStyle("tableBlueCellStyle");
         this.myBlueCellStyle.CharacterFormat = new GemBox.Document.CharacterFormat();
         this.myBlueCellStyle.CharacterFormat.Bold = true;
         this.myBlueCellStyle.CharacterFormat.FontName = "Calibri";
         this.myBlueCellStyle.CharacterFormat.Size = 11D;
         this.myBlueCellStyle.CharacterFormat.FontColor = GemBox.Document.Color.DarkBlue;
         this.myDocumentModel.Styles.Add(this.myBlueCellStyle);

         this.myCentreBoldStyle = new GemBox.Document.ParagraphStyle("tableCentreBoldCellStyle");
         this.myCentreBoldStyle.CharacterFormat = new GemBox.Document.CharacterFormat();
         this.myCentreBoldStyle.CharacterFormat.Bold = true;
         this.myCentreBoldStyle.CharacterFormat.FontName = "Calibri";
         this.myCentreBoldStyle.CharacterFormat.Size = 14D;
         this.myCentreBoldStyle.CharacterFormat.FontColor = GemBox.Document.Color.Black;
         this.myDocumentModel.Styles.Add(this.myCentreBoldStyle);
      }
      private void OpenReportFile(IAsyncResult ar)
      {
         bool fileOpen = true;
         string file = string.Empty;
         ReportStatus status;

         // Retrieve the delegate.
         GenerateTestReport newReport = ((AsyncResult)ar).AsyncDelegate as GenerateTestReport;
         if (newReport != null)
         {
            file = Path.Combine(this.myReportsDirectory, $"CertificationTestReport_{this.tbMnemonic.Text}.docx");
            status = newReport.EndInvoke(ar);
         }
         else
         {
            file = this.myAppendToFilePath;
            AppendTestReport appendReport = ((AsyncResult)ar).AsyncDelegate as AppendTestReport;
            status = appendReport.EndInvoke(ar);
         }

         if (status != ReportStatus.Failure)
         {
            this.AppendErrorMessage = "\nResult processing complete. Saving file...\n";
            if (File.Exists(file))
            {
               while (fileOpen)
               {
                  if (fileOpen = FileInUse(file))
                  {
                     this.AppendErrorMessage = "File is open, please close it";
                  }
                  Thread.Sleep(1000);
               }
               File.Delete(file);
            }
            this.myDocumentModel.Save(file);
            this.AppendErrorMessage = "Report generation complete.\n";
         }
         else
         {
            this.AppendErrorMessage = "\nReport not saved. Encountered errors while creating report.";
         }

         // Get Ready for new file generation
         this.btnGenerateReport.Enabled = true;
         this.tbRegressionReportFolder.Tag = "new";
         this.myDocumentModel = null;
         this.InitializeDocumentModel();
      }
      private void PopulateTableOfContents(ref GemBox.Document.DocumentModel doc)
      {
         // Update TOC (TOC can be updated only after all doc content is added).
         var toc = (TableOfEntries)doc.GetChildElements(true, ElementType.TableOfEntries).First();
         toc.Update();

         // Update TOC's page numbers.
         // NOTE: This is not necessary when printing and saving to PDF, XPS or an image format.
         // Page numbers are automatically updated in that case.
         doc.GetPaginator(new PaginatorOptions() { UpdateFields = true });
      }
      private void rtbErrorWindow_KeyUp(object sender, KeyEventArgs e)
      {
         if (e.KeyData == CopyKeys && !string.IsNullOrEmpty(this.rtbErrorWindow.SelectedText))
         {
            Clipboard.Clear();
            Clipboard.SetText(this.rtbErrorWindow.SelectedText);
         }
      }
      private GemBox.Document.PageMargins SetDocumentMargines()
      {
         GemBox.Document.PageMargins margines = new GemBox.Document.PageMargins();
         margines.Bottom = 54F;  // in points. 72 points in an inch
         margines.Top = 54F;
         margines.Right = 36F;
         margines.Left = 72F;
         margines.Gutter = 0F;
         margines.Header = 45F;
         margines.Footer = 45F;
         return margines;
      }
      private GemBox.Document.PageSetup SetDocumentPageProperties()
      {
         GemBox.Document.PageSetup page = new GemBox.Document.PageSetup();
         page.PageMargins = this.SetDocumentMargines();
         page.PageHeight = 612;    // 8.5"
         page.PageWidth = 792;     // 11"
         page.Orientation = GemBox.Document.Orientation.Landscape;
         page.PaperType = GemBox.Document.PaperType.Letter;
         return page;
      }
      private void textBox_KeyDown(object sender, KeyEventArgs e)
      {
         System.Windows.Forms.TextBox textBox = sender as System.Windows.Forms.TextBox;
         if (textBox != null)
         {
            if (e.KeyCode == Keys.Enter)
            {
               textBox.BackColor = System.Drawing.Color.White;
               this.ProgressLabel = 0;
               rtbErrorWindow.Clear();
               if (textBox.Equals(this.tbRegressionReportFolder))
               {
                  if (Directory.Exists(this.tbRegressionReportFolder.Text))
                  {
                     this.myRegressionReportFolder = this.tbRegressionReportFolder.Text;
                  }
                  else
                  {
                     this.AppendErrorMessage = "Directory does not exist or Path is empty.";
                  }
               }
               else if (textBox.Equals(this.tbAppendToFile))
               {
                  if (File.Exists(this.tbAppendToFile.Text))
                  {
                     this.myAppendToFilePath = this.tbAppendToFile.Text;
                  }
                  else
                  {
                     this.AppendErrorMessage = "File does not exist or Path is empty.";
                  }
               }
            }
            else if (e.KeyCode == Keys.Back)
            {
               textBox.Tag = "textChange";
            }
         }
      }
      private void textBox_TextChanged(object sender, EventArgs e)
      {
         if (sender.Equals(this.tbRegressionReportFolder))
         {
            if (!((System.Windows.Forms.TextBox)sender).Tag.Equals("browse"))
            {
               this.tbRegressionReportFolder.BackColor = System.Drawing.Color.Yellow;
            }
         }
         else
         {
            System.Windows.Forms.TextBox textBox = sender as System.Windows.Forms.TextBox;
            if (textBox != null)
            {
               textBox.BackColor = System.Drawing.Color.Yellow;
            }
         }
      }
      private void tcReport_Selected(object sender, TabControlEventArgs e)
      {
         if (this.tcReport.SelectedTab == this.tpMxSuite)
         {
            if (!string.IsNullOrEmpty(this.tbRegressionReportFolder.Text))
            {
               this.myRegressionReportFolder = this.tbRegressionReportFolder.Text;
            }
            if (!string.IsNullOrEmpty(this.tbAppendToFile.Text))
            {
               this.myAppendToFilePath = this.tbAppendToFile.Text;
            }
            if (this.chkAppendToFile.Checked)
            {
               this.btnAppendToFile.Enabled = true;
               this.tbAppendToFile.Enabled = true;
            }
            else
            {
               this.btnAppendToFile.Enabled = false;
               this.tbAppendToFile.Enabled = false;
            }
         }
      }
      private void UpdateSummaryTable(ref DocumentModel document, Processing80211RegressionResults regressionResults)
      {
         ContentRange summaryTableContent = this.myDocumentModel.Content.Find(frmMxDsrcReportGenerator.SummaryTableTitle80211).First();
         Element[] tables = this.myDocumentModel.GetChildElements(true, ElementType.Table).ToArray();
         Table summaryTable = (Table)tables.First(item => ((Table)item).Metadata.Title == frmMxDsrcReportGenerator.SummaryTableTitle80211);
         List<MxDSRCReportGenerator.ResultSummary> scenariosNotFound = new List<MxDSRCReportGenerator.ResultSummary>();

         foreach (MxDSRCReportGenerator.ResultSummary summary in regressionResults.ScenarioResults)
         {
            if (summaryTable.Rows.Any(row => row.Cells[0].Content.Find(summary.ScenarioName).Count() != 0))
            {
               foreach (TableRow row in summaryTable.Rows)
               {
                  if (row.Cells[0].Content.Find(summary.ScenarioName).Count() != 0 && summary.Result.ToLowerInvariant() == "fail")
                  {
                     row.Cells[1].Content.LoadText(summary.Result, new CharacterFormat() { Bold = true, FontColor = Color.Red });
                     break;
                  }
               }
            }
            else
            {
               scenariosNotFound.Add(summary);
            }
         }

         if (scenariosNotFound.Count != 0)
         {
            this.AppendToSummaryTable(ref this.myDocumentModel, ref summaryTable, scenariosNotFound);
         }

         summaryTableContent.Start.InsertRange(summaryTable.Content);
      }
   }
}