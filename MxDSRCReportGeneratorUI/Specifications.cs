﻿//******************************************************************************************
//*   Proprietary program material                                                         *
//*                                                                                        *
//*   This material is proprietary to Danlaw, Inc., and is not to be reproduced,  used     *
//*   or disclosed except in accordance with a written license agreement with Danlaw, Inc. *
//*                                                                                        *
//*   (C) Copyright 2017 Danlaw, Inc.  All rights reserved.                                *
//*                                                                                        *
//*   Danlaw, Inc., believes that the material furnished herewith is accurate and          *
//*   reliable.  However, no responsibility, financial or otherwise, can be accepted for   *
//*   any consequences arising out of the use of this material.                            *
//*                                                                                        *
//******************************************************************************************

namespace MxDSRCReportGenerator
{
   public enum Specifications
   {
      WAVE80211,  // 802.11
      WAVESecurityServices,   //1609.2
      WAVENetworkingServices, // 1609.3
      WAVEMultiChannelOperation, //1609.4
      SAEJ29451OnboardSystemRequirementsforV2VSafetyCommunications,
      WAVEV2ITestSuite  //RSU - Not supported yet
   }
}