﻿//******************************************************************************************
//*   Proprietary program material                                                         *
//*                                                                                        *
//*   This material is proprietary to Danlaw, Inc., and is not to be reproduced,  used     *
//*   or disclosed except in accordance with a written license agreement with Danlaw, Inc. *
//*                                                                                        *
//*   (C) Copyright 2017 Danlaw, Inc.  All rights reserved.                                *
//*                                                                                        *
//*   Danlaw, Inc., believes that the material furnished herewith is accurate and          *
//*   reliable.  However, no responsibility, financial or otherwise, can be accepted for   *
//*   any consequences arising out of the use of this material.                            *
//*                                                                                        *
//******************************************************************************************

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace MxDSRCReportGenerator
{
   internal class RegressionResults
   {
      private List<string[]> myParsedTestResults = new List<string[]>();
      private List<FileInfo> myRegressionFileNameByDate = new List<FileInfo>();
      private string[] myRegressionSubDirectoriesNames;
      private Specifications mySpecification;
      private List<MxDSRCReportGenerator.ResultSummary> mySummaryresults = new List<MxDSRCReportGenerator.ResultSummary>();
      private TestPurposes myTestPurposesList;
      private List<string> myTestResultsFiles = new List<string>();

      public RegressionResults(string directoryPath, Specifications specification)
      {
         this.myTestPurposesList = new TestPurposes();
         this.mySpecification = specification;
         this.SortFilesAndDirectories(directoryPath);
         this.mySummaryresults = this.SetTestCaseSummaryResults();
      }

      public List<string[]> ParsedTestResults
      {
         get { return this.myParsedTestResults; }
      }
      public List<MxDSRCReportGenerator.ResultSummary> SummaryResults
      {
         get { return this.mySummaryresults; }
      }
      public List<string> TestResultsFiles
      {
         get { return this.myTestResultsFiles; }
      }

      private void FindRegressionSubDirectoriesNames(string dirName)
      {
         if (!(string.IsNullOrEmpty(dirName)) || (string.IsNullOrWhiteSpace(dirName)))
         {
            this.myRegressionSubDirectoriesNames = System.IO.Directory.GetDirectories(dirName).Where(item => !item.Contains("CertificationTestReport")).ToArray();
         }
      }
      public List<string[]> ParseTestResultsFromAFile(string fileName)
      {
         string line;
         string[] parsedCsvLines;
         this.myParsedTestResults.Clear();
         System.IO.StreamReader file = new System.IO.StreamReader(@fileName);
         while ((line = file.ReadLine()) != null)
         {
            parsedCsvLines = line.Split(',');
            this.myParsedTestResults.Add(parsedCsvLines);

         }
         return this.myParsedTestResults;
      }
      private List<MxDSRCReportGenerator.ResultSummary> SetTestCaseSummaryResults()
      {
         int i = 0;
         bool found = false;
         MxDSRCReportGenerator.ResultSummary result;
         string fileName = "";
         string line;
         string testCaseName = "";
         string testCaseResult = "Not Run";
         List<MxDSRCReportGenerator.ResultSummary> summaryResults = new List<MxDSRCReportGenerator.ResultSummary>();

         List<string> list = TestPurposes.TP16094;
         switch (this.mySpecification)
         {
            case Specifications.SAEJ29451OnboardSystemRequirementsforV2VSafetyCommunications:
               {
                  list = TestPurposes.TP29451;
               }
               break;

            case Specifications.WAVEMultiChannelOperation:
               {
                  list = TestPurposes.TP16094;
               }
               break;

            case Specifications.WAVENetworkingServices:
               {
                  list = TestPurposes.TP16093;
               }
               break;

            case Specifications.WAVESecurityServices:
               {
                  list = TestPurposes.TP16092;
               }
               break;

            case Specifications.WAVEV2ITestSuite:
               {
                  list = TestPurposes.TPV2I;
               }
               break;
         }

         for (i = 0; i < list.Count; i++)
         {
            // Find file in the list of tests ran
            // If it does not exist, set it to Not run
            // if exists, open file, read the line that says Test Case Verdict and fill with the results
            found = false;
            testCaseResult = "Not Run";
            testCaseName = list[i];
            for (int j = 0; j < this.myTestResultsFiles.Count; j++)
            {
               string file = this.myTestResultsFiles[j];
               file = file.Replace('_', '-');
               if (file.Contains(testCaseName))
               {
                  found = true;
                  fileName = this.myTestResultsFiles[j];
                  break;
               }
            }
            if (found)
            {
               // Read the file and display it line by line.
               System.IO.StreamReader file = new System.IO.StreamReader(@fileName);
               while ((line = file.ReadLine()) != null)
               {
                  line = line.ToUpper();
                  if (line.Contains("TEST CASE VERDICT"))
                  {
                     if (line.Contains("PASS"))
                     {
                        testCaseResult = "PASS";
                     }

                     if (line.Contains("FAIL"))
                     {
                        testCaseResult = "FAIL";
                     }
                  }
               }
            }
            result = new MxDSRCReportGenerator.ResultSummary(testCaseName, testCaseResult);
            summaryResults.Add(result);
         }

         return summaryResults;
      }
      private void SortFilesAndDirectories(string dirName)
      {
         this.FindRegressionSubDirectoriesNames(dirName);
         this.myTestResultsFiles.Clear();

         for (int j = 0; j < myRegressionSubDirectoriesNames.Length; j++)
         {
            // Sort files according to date
            this.SortRegressionSubDirectoryFileNamesByDate(this.myRegressionSubDirectoriesNames[j]);
            // Get the last modified file
            this.myTestResultsFiles.Add(this.myRegressionFileNameByDate[this.myRegressionFileNameByDate.Count - 1].FullName);
         }
      }
      private void SortRegressionSubDirectoryFileNamesByDate(string directoryName)
      {
         this.myRegressionFileNameByDate.Clear();

         var sortedFiles = new DirectoryInfo(@directoryName).GetFiles()
         .OrderBy(f => f.LastWriteTime)
         .ToList();
         this.myRegressionFileNameByDate.AddRange(sortedFiles);
      }
   }
}

