﻿namespace MxDSRCReportGenerator
{
   partial class frmMxDsrcReportGenerator
   {
      /// <summary>
      /// Required designer variable.
      /// </summary>
      private System.ComponentModel.IContainer components = null;

      /// <summary>
      /// Clean up any resources being used.
      /// </summary>
      /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
      protected override void Dispose(bool disposing)
      {
         if (disposing && (components != null))
         {
            components.Dispose();
         }
         base.Dispose(disposing);
      }

      #region Windows Form Designer generated code

      /// <summary>
      /// Required method for Designer support - do not modify
      /// the contents of this method with the code editor.
      /// </summary>
      private void InitializeComponent()
      {
         System.Windows.Forms.Label lblAppendToFile;
         System.Windows.Forms.Label lblMxSuiteVersion;
         System.Windows.Forms.TabPage tpReportParameters;
         System.Windows.Forms.GroupBox grpApplicantInfo;
         System.Windows.Forms.Label lblApplicantName;
         System.Windows.Forms.Label lblApplicantAddress;
         System.Windows.Forms.GroupBox grpLabInfo;
         System.Windows.Forms.Label lblLabLogo;
         System.Windows.Forms.Label lblLabName;
         System.Windows.Forms.Label lblLabAddress;
         System.Windows.Forms.Label lblTestedBy;
         System.Windows.Forms.Label lblReportNumber;
         System.Windows.Forms.GroupBox grpDevInfo;
         System.Windows.Forms.Label lblDeviceImage;
         System.Windows.Forms.Label lblDeviceDescription;
         System.Windows.Forms.Label lblSerialNumber;
         System.Windows.Forms.Label lblDeviceProfile;
         System.Windows.Forms.Label lblTciVersion;
         System.Windows.Forms.Label lblHardwareVersion;
         System.Windows.Forms.Label lblFirmwareVersion;
         System.Windows.Forms.Label lblModelNumber;
         System.Windows.Forms.Label lblProductName;
         System.Windows.Forms.TabPage tpTestHardware;
         System.Windows.Forms.GroupBox grpModem;
         System.Windows.Forms.Label lblProxyRev;
         System.Windows.Forms.Label lblModemFirmwareRev;
         System.Windows.Forms.Label lblModemSerialNumber;
         System.Windows.Forms.Label lblModemPartNumber;
         System.Windows.Forms.GroupBox grpNationalInstruments;
         System.Windows.Forms.GroupBox grpNISwitch;
         System.Windows.Forms.Label lblSwitchSerialNumber;
         System.Windows.Forms.GroupBox grpAttenuator;
         System.Windows.Forms.Label lblAttenuatorSerialNumber;
         System.Windows.Forms.GroupBox grpVst;
         System.Windows.Forms.Label lblVstCalibrationDate;
         System.Windows.Forms.Label lblVstSerialNumber;
         System.Windows.Forms.Label label1;
         System.Windows.Forms.Label label5;
         System.Windows.Forms.Label label8;
         this.txtApplicantAddress = new System.Windows.Forms.TextBox();
         this.txtApplicantName = new System.Windows.Forms.TextBox();
         this.pbLabLogo = new System.Windows.Forms.PictureBox();
         this.btnImportLabLogo = new System.Windows.Forms.Button();
         this.txtLabAddress = new System.Windows.Forms.TextBox();
         this.txtLabName = new System.Windows.Forms.TextBox();
         this.txtTestedBy = new System.Windows.Forms.TextBox();
         this.txtReportNumber = new System.Windows.Forms.TextBox();
         this.pbDeviceImage = new System.Windows.Forms.PictureBox();
         this.btnImportDeviceImage = new System.Windows.Forms.Button();
         this.txtDeviceDescription = new System.Windows.Forms.TextBox();
         this.txtSerialNumber = new System.Windows.Forms.TextBox();
         this.txtDeviceProfile = new System.Windows.Forms.TextBox();
         this.txtTciRev = new System.Windows.Forms.TextBox();
         this.txtHardwareVersion = new System.Windows.Forms.TextBox();
         this.txtFirmwareVersion = new System.Windows.Forms.TextBox();
         this.txtProductName = new System.Windows.Forms.TextBox();
         this.txtModelNumber = new System.Windows.Forms.TextBox();
         this.txtProxyRev = new System.Windows.Forms.TextBox();
         this.txtModemFirmwareRev = new System.Windows.Forms.TextBox();
         this.txtModemSerialNumber = new System.Windows.Forms.TextBox();
         this.txtModemPartNumber = new System.Windows.Forms.TextBox();
         this.txtSwitchSerialNumber = new System.Windows.Forms.TextBox();
         this.txtAttenuatorSerialNumber = new System.Windows.Forms.TextBox();
         this.txtVstSerialNumber = new System.Windows.Forms.TextBox();
         this.txtVstCalibrationDate = new System.Windows.Forms.TextBox();
         this.lblRegReportFolder = new System.Windows.Forms.Label();
         this.tpMxSuite = new System.Windows.Forms.TabPage();
         this.rtbErrorWindow = new System.Windows.Forms.RichTextBox();
         this.tbRevision = new System.Windows.Forms.TextBox();
         this.tbMnemonic = new System.Windows.Forms.TextBox();
         this.cmbSpecification = new System.Windows.Forms.ComboBox();
         this.ctlProgressBar = new System.Windows.Forms.ProgressBar();
         this.btnGenerateReport = new System.Windows.Forms.Button();
         this.btnAppendToFile = new System.Windows.Forms.Button();
         this.tbAppendToFile = new System.Windows.Forms.TextBox();
         this.chkAppendToFile = new System.Windows.Forms.CheckBox();
         this.tbRegressionReportFolder = new System.Windows.Forms.TextBox();
         this.tbMxSuiteVersion = new System.Windows.Forms.TextBox();
         this.btnRegressionReportFolder = new System.Windows.Forms.Button();
         this.tcReport = new System.Windows.Forms.TabControl();
         lblAppendToFile = new System.Windows.Forms.Label();
         lblMxSuiteVersion = new System.Windows.Forms.Label();
         tpReportParameters = new System.Windows.Forms.TabPage();
         grpApplicantInfo = new System.Windows.Forms.GroupBox();
         lblApplicantName = new System.Windows.Forms.Label();
         lblApplicantAddress = new System.Windows.Forms.Label();
         grpLabInfo = new System.Windows.Forms.GroupBox();
         lblLabLogo = new System.Windows.Forms.Label();
         lblLabName = new System.Windows.Forms.Label();
         lblLabAddress = new System.Windows.Forms.Label();
         lblTestedBy = new System.Windows.Forms.Label();
         lblReportNumber = new System.Windows.Forms.Label();
         grpDevInfo = new System.Windows.Forms.GroupBox();
         lblDeviceImage = new System.Windows.Forms.Label();
         lblDeviceDescription = new System.Windows.Forms.Label();
         lblSerialNumber = new System.Windows.Forms.Label();
         lblDeviceProfile = new System.Windows.Forms.Label();
         lblTciVersion = new System.Windows.Forms.Label();
         lblHardwareVersion = new System.Windows.Forms.Label();
         lblFirmwareVersion = new System.Windows.Forms.Label();
         lblModelNumber = new System.Windows.Forms.Label();
         lblProductName = new System.Windows.Forms.Label();
         tpTestHardware = new System.Windows.Forms.TabPage();
         grpModem = new System.Windows.Forms.GroupBox();
         lblProxyRev = new System.Windows.Forms.Label();
         lblModemFirmwareRev = new System.Windows.Forms.Label();
         lblModemSerialNumber = new System.Windows.Forms.Label();
         lblModemPartNumber = new System.Windows.Forms.Label();
         grpNationalInstruments = new System.Windows.Forms.GroupBox();
         grpNISwitch = new System.Windows.Forms.GroupBox();
         lblSwitchSerialNumber = new System.Windows.Forms.Label();
         grpAttenuator = new System.Windows.Forms.GroupBox();
         lblAttenuatorSerialNumber = new System.Windows.Forms.Label();
         grpVst = new System.Windows.Forms.GroupBox();
         lblVstCalibrationDate = new System.Windows.Forms.Label();
         lblVstSerialNumber = new System.Windows.Forms.Label();
         label1 = new System.Windows.Forms.Label();
         label5 = new System.Windows.Forms.Label();
         label8 = new System.Windows.Forms.Label();
         tpReportParameters.SuspendLayout();
         grpApplicantInfo.SuspendLayout();
         grpLabInfo.SuspendLayout();
         ((System.ComponentModel.ISupportInitialize)(this.pbLabLogo)).BeginInit();
         grpDevInfo.SuspendLayout();
         ((System.ComponentModel.ISupportInitialize)(this.pbDeviceImage)).BeginInit();
         tpTestHardware.SuspendLayout();
         grpModem.SuspendLayout();
         grpNationalInstruments.SuspendLayout();
         grpNISwitch.SuspendLayout();
         grpAttenuator.SuspendLayout();
         grpVst.SuspendLayout();
         this.tpMxSuite.SuspendLayout();
         this.tcReport.SuspendLayout();
         this.SuspendLayout();
         // 
         // lblAppendToFile
         // 
         lblAppendToFile.AutoSize = true;
         lblAppendToFile.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
         lblAppendToFile.Location = new System.Drawing.Point(35, 181);
         lblAppendToFile.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
         lblAppendToFile.Name = "lblAppendToFile";
         lblAppendToFile.Size = new System.Drawing.Size(158, 26);
         lblAppendToFile.TabIndex = 30;
         lblAppendToFile.Text = "Append To File";
         lblAppendToFile.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
         // 
         // lblMxSuiteVersion
         // 
         lblMxSuiteVersion.AutoSize = true;
         lblMxSuiteVersion.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
         lblMxSuiteVersion.Location = new System.Drawing.Point(35, 38);
         lblMxSuiteVersion.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
         lblMxSuiteVersion.Name = "lblMxSuiteVersion";
         lblMxSuiteVersion.Size = new System.Drawing.Size(178, 26);
         lblMxSuiteVersion.TabIndex = 22;
         lblMxSuiteVersion.Text = "Software Version";
         lblMxSuiteVersion.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
         // 
         // tpReportParameters
         // 
         tpReportParameters.BackColor = System.Drawing.Color.SkyBlue;
         tpReportParameters.Controls.Add(grpApplicantInfo);
         tpReportParameters.Controls.Add(grpLabInfo);
         tpReportParameters.Controls.Add(grpDevInfo);
         tpReportParameters.Location = new System.Drawing.Point(4, 35);
         tpReportParameters.Name = "tpReportParameters";
         tpReportParameters.Padding = new System.Windows.Forms.Padding(3);
         tpReportParameters.Size = new System.Drawing.Size(1119, 510);
         tpReportParameters.TabIndex = 0;
         tpReportParameters.Text = "Report Parameters";
         // 
         // grpApplicantInfo
         // 
         grpApplicantInfo.BackColor = System.Drawing.Color.Honeydew;
         grpApplicantInfo.Controls.Add(this.txtApplicantAddress);
         grpApplicantInfo.Controls.Add(lblApplicantName);
         grpApplicantInfo.Controls.Add(this.txtApplicantName);
         grpApplicantInfo.Controls.Add(lblApplicantAddress);
         grpApplicantInfo.Location = new System.Drawing.Point(600, 342);
         grpApplicantInfo.Name = "grpApplicantInfo";
         grpApplicantInfo.Size = new System.Drawing.Size(486, 148);
         grpApplicantInfo.TabIndex = 39;
         grpApplicantInfo.TabStop = false;
         grpApplicantInfo.Text = "Applicant Information";
         // 
         // txtApplicantAddress
         // 
         this.txtApplicantAddress.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
         this.txtApplicantAddress.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.txtApplicantAddress.Location = new System.Drawing.Point(165, 73);
         this.txtApplicantAddress.Margin = new System.Windows.Forms.Padding(4);
         this.txtApplicantAddress.Multiline = true;
         this.txtApplicantAddress.Name = "txtApplicantAddress";
         this.txtApplicantAddress.Size = new System.Drawing.Size(300, 67);
         this.txtApplicantAddress.TabIndex = 33;
         this.txtApplicantAddress.TextChanged += new System.EventHandler(this.textBox_TextChanged);
         this.txtApplicantAddress.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBox_KeyDown);
         // 
         // lblApplicantName
         // 
         lblApplicantName.AutoSize = true;
         lblApplicantName.Location = new System.Drawing.Point(7, 38);
         lblApplicantName.Margin = new System.Windows.Forms.Padding(4);
         lblApplicantName.Name = "lblApplicantName";
         lblApplicantName.Size = new System.Drawing.Size(167, 26);
         lblApplicantName.TabIndex = 14;
         lblApplicantName.Text = "Applicant Name";
         // 
         // txtApplicantName
         // 
         this.txtApplicantName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
         this.txtApplicantName.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.txtApplicantName.Location = new System.Drawing.Point(165, 34);
         this.txtApplicantName.Margin = new System.Windows.Forms.Padding(4);
         this.txtApplicantName.Name = "txtApplicantName";
         this.txtApplicantName.Size = new System.Drawing.Size(300, 35);
         this.txtApplicantName.TabIndex = 15;
         this.txtApplicantName.TextChanged += new System.EventHandler(this.textBox_TextChanged);
         this.txtApplicantName.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBox_KeyDown);
         // 
         // lblApplicantAddress
         // 
         lblApplicantAddress.AutoSize = true;
         lblApplicantAddress.Location = new System.Drawing.Point(7, 77);
         lblApplicantAddress.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
         lblApplicantAddress.Name = "lblApplicantAddress";
         lblApplicantAddress.Size = new System.Drawing.Size(92, 26);
         lblApplicantAddress.TabIndex = 32;
         lblApplicantAddress.Text = "Address";
         // 
         // grpLabInfo
         // 
         grpLabInfo.BackColor = System.Drawing.Color.Honeydew;
         grpLabInfo.Controls.Add(lblLabLogo);
         grpLabInfo.Controls.Add(this.pbLabLogo);
         grpLabInfo.Controls.Add(this.btnImportLabLogo);
         grpLabInfo.Controls.Add(this.txtLabAddress);
         grpLabInfo.Controls.Add(lblLabName);
         grpLabInfo.Controls.Add(this.txtLabName);
         grpLabInfo.Controls.Add(lblLabAddress);
         grpLabInfo.Controls.Add(this.txtTestedBy);
         grpLabInfo.Controls.Add(lblTestedBy);
         grpLabInfo.Controls.Add(this.txtReportNumber);
         grpLabInfo.Controls.Add(lblReportNumber);
         grpLabInfo.Location = new System.Drawing.Point(600, 19);
         grpLabInfo.Name = "grpLabInfo";
         grpLabInfo.Size = new System.Drawing.Size(486, 311);
         grpLabInfo.TabIndex = 38;
         grpLabInfo.TabStop = false;
         grpLabInfo.Text = "Lab Information";
         // 
         // lblLabLogo
         // 
         lblLabLogo.AutoSize = true;
         lblLabLogo.Location = new System.Drawing.Point(11, 235);
         lblLabLogo.Name = "lblLabLogo";
         lblLabLogo.Size = new System.Drawing.Size(73, 26);
         lblLabLogo.TabIndex = 31;
         lblLabLogo.Text = "Image";
         // 
         // pbLabLogo
         // 
         this.pbLabLogo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
         this.pbLabLogo.Location = new System.Drawing.Point(246, 225);
         this.pbLabLogo.MaximumSize = new System.Drawing.Size(200, 62);
         this.pbLabLogo.Name = "pbLabLogo";
         this.pbLabLogo.Size = new System.Drawing.Size(200, 62);
         this.pbLabLogo.TabIndex = 32;
         this.pbLabLogo.TabStop = false;
         // 
         // btnImportLabLogo
         // 
         this.btnImportLabLogo.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
         this.btnImportLabLogo.Location = new System.Drawing.Point(165, 233);
         this.btnImportLabLogo.Name = "btnImportLabLogo";
         this.btnImportLabLogo.Size = new System.Drawing.Size(75, 29);
         this.btnImportLabLogo.TabIndex = 31;
         this.btnImportLabLogo.Text = "Import";
         this.btnImportLabLogo.UseVisualStyleBackColor = true;
         this.btnImportLabLogo.Click += new System.EventHandler(this.btnImportLabLogo_Click);
         // 
         // txtLabAddress
         // 
         this.txtLabAddress.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
         this.txtLabAddress.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.txtLabAddress.Location = new System.Drawing.Point(165, 73);
         this.txtLabAddress.Margin = new System.Windows.Forms.Padding(4);
         this.txtLabAddress.Multiline = true;
         this.txtLabAddress.Name = "txtLabAddress";
         this.txtLabAddress.Size = new System.Drawing.Size(300, 67);
         this.txtLabAddress.TabIndex = 19;
         this.txtLabAddress.TextChanged += new System.EventHandler(this.textBox_TextChanged);
         this.txtLabAddress.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBox_KeyDown);
         // 
         // lblLabName
         // 
         lblLabName.AutoSize = true;
         lblLabName.Location = new System.Drawing.Point(7, 38);
         lblLabName.Margin = new System.Windows.Forms.Padding(4);
         lblLabName.Name = "lblLabName";
         lblLabName.Size = new System.Drawing.Size(113, 26);
         lblLabName.TabIndex = 16;
         lblLabName.Text = "Lab Name";
         // 
         // txtLabName
         // 
         this.txtLabName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
         this.txtLabName.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.txtLabName.Location = new System.Drawing.Point(165, 34);
         this.txtLabName.Margin = new System.Windows.Forms.Padding(4);
         this.txtLabName.Name = "txtLabName";
         this.txtLabName.Size = new System.Drawing.Size(300, 35);
         this.txtLabName.TabIndex = 17;
         this.txtLabName.TextChanged += new System.EventHandler(this.textBox_TextChanged);
         this.txtLabName.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBox_KeyDown);
         // 
         // lblLabAddress
         // 
         lblLabAddress.AutoSize = true;
         lblLabAddress.Location = new System.Drawing.Point(7, 77);
         lblLabAddress.Margin = new System.Windows.Forms.Padding(4);
         lblLabAddress.Name = "lblLabAddress";
         lblLabAddress.Size = new System.Drawing.Size(92, 26);
         lblLabAddress.TabIndex = 18;
         lblLabAddress.Text = "Address";
         // 
         // txtTestedBy
         // 
         this.txtTestedBy.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
         this.txtTestedBy.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.txtTestedBy.Location = new System.Drawing.Point(165, 147);
         this.txtTestedBy.Margin = new System.Windows.Forms.Padding(4);
         this.txtTestedBy.Name = "txtTestedBy";
         this.txtTestedBy.Size = new System.Drawing.Size(300, 35);
         this.txtTestedBy.TabIndex = 25;
         this.txtTestedBy.TextChanged += new System.EventHandler(this.textBox_TextChanged);
         this.txtTestedBy.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBox_KeyDown);
         // 
         // lblTestedBy
         // 
         lblTestedBy.AutoSize = true;
         lblTestedBy.Location = new System.Drawing.Point(10, 151);
         lblTestedBy.Margin = new System.Windows.Forms.Padding(4);
         lblTestedBy.Name = "lblTestedBy";
         lblTestedBy.Size = new System.Drawing.Size(109, 26);
         lblTestedBy.TabIndex = 24;
         lblTestedBy.Text = "Tested By";
         // 
         // txtReportNumber
         // 
         this.txtReportNumber.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
         this.txtReportNumber.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.txtReportNumber.Location = new System.Drawing.Point(165, 184);
         this.txtReportNumber.Margin = new System.Windows.Forms.Padding(4);
         this.txtReportNumber.Name = "txtReportNumber";
         this.txtReportNumber.Size = new System.Drawing.Size(300, 35);
         this.txtReportNumber.TabIndex = 1;
         this.txtReportNumber.TextChanged += new System.EventHandler(this.textBox_TextChanged);
         this.txtReportNumber.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBox_KeyDown);
         // 
         // lblReportNumber
         // 
         lblReportNumber.AutoSize = true;
         lblReportNumber.Location = new System.Drawing.Point(11, 187);
         lblReportNumber.Margin = new System.Windows.Forms.Padding(4);
         lblReportNumber.Name = "lblReportNumber";
         lblReportNumber.Size = new System.Drawing.Size(161, 26);
         lblReportNumber.TabIndex = 0;
         lblReportNumber.Text = "Report Number";
         // 
         // grpDevInfo
         // 
         grpDevInfo.BackColor = System.Drawing.Color.Honeydew;
         grpDevInfo.Controls.Add(this.pbDeviceImage);
         grpDevInfo.Controls.Add(this.btnImportDeviceImage);
         grpDevInfo.Controls.Add(lblDeviceImage);
         grpDevInfo.Controls.Add(lblDeviceDescription);
         grpDevInfo.Controls.Add(this.txtDeviceDescription);
         grpDevInfo.Controls.Add(this.txtSerialNumber);
         grpDevInfo.Controls.Add(lblSerialNumber);
         grpDevInfo.Controls.Add(lblDeviceProfile);
         grpDevInfo.Controls.Add(this.txtDeviceProfile);
         grpDevInfo.Controls.Add(lblTciVersion);
         grpDevInfo.Controls.Add(this.txtTciRev);
         grpDevInfo.Controls.Add(lblHardwareVersion);
         grpDevInfo.Controls.Add(this.txtHardwareVersion);
         grpDevInfo.Controls.Add(lblFirmwareVersion);
         grpDevInfo.Controls.Add(this.txtFirmwareVersion);
         grpDevInfo.Controls.Add(lblModelNumber);
         grpDevInfo.Controls.Add(this.txtProductName);
         grpDevInfo.Controls.Add(lblProductName);
         grpDevInfo.Controls.Add(this.txtModelNumber);
         grpDevInfo.Location = new System.Drawing.Point(28, 19);
         grpDevInfo.Name = "grpDevInfo";
         grpDevInfo.Size = new System.Drawing.Size(479, 472);
         grpDevInfo.TabIndex = 40;
         grpDevInfo.TabStop = false;
         grpDevInfo.Text = "Device Information";
         // 
         // pbDeviceImage
         // 
         this.pbDeviceImage.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
         this.pbDeviceImage.Location = new System.Drawing.Point(276, 409);
         this.pbDeviceImage.MaximumSize = new System.Drawing.Size(150, 43);
         this.pbDeviceImage.Name = "pbDeviceImage";
         this.pbDeviceImage.Size = new System.Drawing.Size(150, 43);
         this.pbDeviceImage.TabIndex = 30;
         this.pbDeviceImage.TabStop = false;
         // 
         // btnImportDeviceImage
         // 
         this.btnImportDeviceImage.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
         this.btnImportDeviceImage.Location = new System.Drawing.Point(195, 418);
         this.btnImportDeviceImage.Name = "btnImportDeviceImage";
         this.btnImportDeviceImage.Size = new System.Drawing.Size(75, 29);
         this.btnImportDeviceImage.TabIndex = 29;
         this.btnImportDeviceImage.Text = "Import";
         this.btnImportDeviceImage.UseVisualStyleBackColor = true;
         this.btnImportDeviceImage.Click += new System.EventHandler(this.btnImportDeviceImage_Click);
         // 
         // lblDeviceImage
         // 
         lblDeviceImage.AutoSize = true;
         lblDeviceImage.Location = new System.Drawing.Point(14, 418);
         lblDeviceImage.Name = "lblDeviceImage";
         lblDeviceImage.Size = new System.Drawing.Size(73, 26);
         lblDeviceImage.TabIndex = 28;
         lblDeviceImage.Text = "Image";
         // 
         // lblDeviceDescription
         // 
         lblDeviceDescription.AutoSize = true;
         lblDeviceDescription.Location = new System.Drawing.Point(10, 306);
         lblDeviceDescription.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
         lblDeviceDescription.Name = "lblDeviceDescription";
         lblDeviceDescription.Size = new System.Drawing.Size(121, 26);
         lblDeviceDescription.TabIndex = 27;
         lblDeviceDescription.Text = "Description";
         // 
         // txtDeviceDescription
         // 
         this.txtDeviceDescription.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.txtDeviceDescription.Location = new System.Drawing.Point(194, 302);
         this.txtDeviceDescription.Margin = new System.Windows.Forms.Padding(4);
         this.txtDeviceDescription.Multiline = true;
         this.txtDeviceDescription.Name = "txtDeviceDescription";
         this.txtDeviceDescription.Size = new System.Drawing.Size(250, 101);
         this.txtDeviceDescription.TabIndex = 26;
         this.txtDeviceDescription.TextChanged += new System.EventHandler(this.textBox_TextChanged);
         this.txtDeviceDescription.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBox_KeyDown);
         // 
         // txtSerialNumber
         // 
         this.txtSerialNumber.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
         this.txtSerialNumber.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.txtSerialNumber.Location = new System.Drawing.Point(194, 111);
         this.txtSerialNumber.Margin = new System.Windows.Forms.Padding(4);
         this.txtSerialNumber.Name = "txtSerialNumber";
         this.txtSerialNumber.Size = new System.Drawing.Size(250, 35);
         this.txtSerialNumber.TabIndex = 15;
         this.txtSerialNumber.TextChanged += new System.EventHandler(this.textBox_TextChanged);
         this.txtSerialNumber.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBox_KeyDown);
         // 
         // lblSerialNumber
         // 
         lblSerialNumber.AutoSize = true;
         lblSerialNumber.Location = new System.Drawing.Point(10, 115);
         lblSerialNumber.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
         lblSerialNumber.Name = "lblSerialNumber";
         lblSerialNumber.Size = new System.Drawing.Size(152, 26);
         lblSerialNumber.TabIndex = 14;
         lblSerialNumber.Text = "Serial Number";
         // 
         // lblDeviceProfile
         // 
         lblDeviceProfile.AutoSize = true;
         lblDeviceProfile.Location = new System.Drawing.Point(10, 268);
         lblDeviceProfile.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
         lblDeviceProfile.Name = "lblDeviceProfile";
         lblDeviceProfile.Size = new System.Drawing.Size(147, 26);
         lblDeviceProfile.TabIndex = 12;
         lblDeviceProfile.Text = "Device Profile";
         // 
         // txtDeviceProfile
         // 
         this.txtDeviceProfile.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
         this.txtDeviceProfile.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.txtDeviceProfile.Location = new System.Drawing.Point(194, 264);
         this.txtDeviceProfile.Margin = new System.Windows.Forms.Padding(4);
         this.txtDeviceProfile.Name = "txtDeviceProfile";
         this.txtDeviceProfile.Size = new System.Drawing.Size(250, 35);
         this.txtDeviceProfile.TabIndex = 13;
         this.txtDeviceProfile.TextChanged += new System.EventHandler(this.textBox_TextChanged);
         this.txtDeviceProfile.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBox_KeyDown);
         // 
         // lblTciVersion
         // 
         lblTciVersion.AutoSize = true;
         lblTciVersion.Location = new System.Drawing.Point(10, 230);
         lblTciVersion.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
         lblTciVersion.Name = "lblTciVersion";
         lblTciVersion.Size = new System.Drawing.Size(126, 26);
         lblTciVersion.TabIndex = 10;
         lblTciVersion.Text = "TCI Version";
         // 
         // txtTciRev
         // 
         this.txtTciRev.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
         this.txtTciRev.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.txtTciRev.Location = new System.Drawing.Point(194, 226);
         this.txtTciRev.Margin = new System.Windows.Forms.Padding(4);
         this.txtTciRev.Name = "txtTciRev";
         this.txtTciRev.Size = new System.Drawing.Size(250, 35);
         this.txtTciRev.TabIndex = 11;
         this.txtTciRev.TextChanged += new System.EventHandler(this.textBox_TextChanged);
         this.txtTciRev.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBox_KeyDown);
         // 
         // lblHardwareVersion
         // 
         lblHardwareVersion.AutoSize = true;
         lblHardwareVersion.Location = new System.Drawing.Point(10, 191);
         lblHardwareVersion.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
         lblHardwareVersion.Name = "lblHardwareVersion";
         lblHardwareVersion.Size = new System.Drawing.Size(186, 26);
         lblHardwareVersion.TabIndex = 8;
         lblHardwareVersion.Text = "Hardware Version";
         // 
         // txtHardwareVersion
         // 
         this.txtHardwareVersion.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
         this.txtHardwareVersion.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.txtHardwareVersion.Location = new System.Drawing.Point(194, 187);
         this.txtHardwareVersion.Margin = new System.Windows.Forms.Padding(4);
         this.txtHardwareVersion.Name = "txtHardwareVersion";
         this.txtHardwareVersion.Size = new System.Drawing.Size(250, 35);
         this.txtHardwareVersion.TabIndex = 9;
         this.txtHardwareVersion.TextChanged += new System.EventHandler(this.textBox_TextChanged);
         this.txtHardwareVersion.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBox_KeyDown);
         // 
         // lblFirmwareVersion
         // 
         lblFirmwareVersion.AutoSize = true;
         lblFirmwareVersion.Location = new System.Drawing.Point(10, 153);
         lblFirmwareVersion.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
         lblFirmwareVersion.Name = "lblFirmwareVersion";
         lblFirmwareVersion.Size = new System.Drawing.Size(183, 26);
         lblFirmwareVersion.TabIndex = 6;
         lblFirmwareVersion.Text = "Firmware Version";
         // 
         // txtFirmwareVersion
         // 
         this.txtFirmwareVersion.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
         this.txtFirmwareVersion.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.txtFirmwareVersion.Location = new System.Drawing.Point(194, 149);
         this.txtFirmwareVersion.Margin = new System.Windows.Forms.Padding(4);
         this.txtFirmwareVersion.Name = "txtFirmwareVersion";
         this.txtFirmwareVersion.Size = new System.Drawing.Size(250, 35);
         this.txtFirmwareVersion.TabIndex = 7;
         this.txtFirmwareVersion.TextChanged += new System.EventHandler(this.textBox_TextChanged);
         this.txtFirmwareVersion.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBox_KeyDown);
         // 
         // lblModelNumber
         // 
         lblModelNumber.AutoSize = true;
         lblModelNumber.Location = new System.Drawing.Point(10, 77);
         lblModelNumber.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
         lblModelNumber.Name = "lblModelNumber";
         lblModelNumber.Size = new System.Drawing.Size(155, 26);
         lblModelNumber.TabIndex = 4;
         lblModelNumber.Text = "Model Number";
         lblModelNumber.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
         // 
         // txtProductName
         // 
         this.txtProductName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
         this.txtProductName.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.txtProductName.Location = new System.Drawing.Point(194, 33);
         this.txtProductName.Margin = new System.Windows.Forms.Padding(4);
         this.txtProductName.Name = "txtProductName";
         this.txtProductName.Size = new System.Drawing.Size(250, 35);
         this.txtProductName.TabIndex = 3;
         this.txtProductName.TextChanged += new System.EventHandler(this.textBox_TextChanged);
         this.txtProductName.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBox_KeyDown);
         // 
         // lblProductName
         // 
         lblProductName.AutoSize = true;
         lblProductName.Location = new System.Drawing.Point(10, 38);
         lblProductName.Margin = new System.Windows.Forms.Padding(4);
         lblProductName.Name = "lblProductName";
         lblProductName.Size = new System.Drawing.Size(152, 26);
         lblProductName.TabIndex = 2;
         lblProductName.Text = "Product Name";
         lblProductName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
         // 
         // txtModelNumber
         // 
         this.txtModelNumber.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
         this.txtModelNumber.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.txtModelNumber.Location = new System.Drawing.Point(195, 73);
         this.txtModelNumber.Margin = new System.Windows.Forms.Padding(4);
         this.txtModelNumber.Name = "txtModelNumber";
         this.txtModelNumber.Size = new System.Drawing.Size(250, 35);
         this.txtModelNumber.TabIndex = 5;
         this.txtModelNumber.TextChanged += new System.EventHandler(this.textBox_TextChanged);
         this.txtModelNumber.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBox_KeyDown);
         // 
         // tpTestHardware
         // 
         tpTestHardware.BackColor = System.Drawing.Color.Honeydew;
         tpTestHardware.Controls.Add(grpModem);
         tpTestHardware.Controls.Add(grpNationalInstruments);
         tpTestHardware.Location = new System.Drawing.Point(4, 35);
         tpTestHardware.Name = "tpTestHardware";
         tpTestHardware.Size = new System.Drawing.Size(1119, 510);
         tpTestHardware.TabIndex = 2;
         tpTestHardware.Text = "Test Hardware";
         // 
         // grpModem
         // 
         grpModem.BackColor = System.Drawing.Color.SkyBlue;
         grpModem.Controls.Add(this.txtProxyRev);
         grpModem.Controls.Add(lblProxyRev);
         grpModem.Controls.Add(this.txtModemFirmwareRev);
         grpModem.Controls.Add(lblModemFirmwareRev);
         grpModem.Controls.Add(this.txtModemSerialNumber);
         grpModem.Controls.Add(lblModemSerialNumber);
         grpModem.Controls.Add(this.txtModemPartNumber);
         grpModem.Controls.Add(lblModemPartNumber);
         grpModem.Location = new System.Drawing.Point(671, 47);
         grpModem.Name = "grpModem";
         grpModem.Size = new System.Drawing.Size(417, 193);
         grpModem.TabIndex = 1;
         grpModem.TabStop = false;
         grpModem.Text = "Modem";
         // 
         // txtProxyRev
         // 
         this.txtProxyRev.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
         this.txtProxyRev.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.txtProxyRev.Location = new System.Drawing.Point(193, 149);
         this.txtProxyRev.Margin = new System.Windows.Forms.Padding(4);
         this.txtProxyRev.Name = "txtProxyRev";
         this.txtProxyRev.Size = new System.Drawing.Size(200, 35);
         this.txtProxyRev.TabIndex = 23;
         this.txtProxyRev.TextChanged += new System.EventHandler(this.textBox_TextChanged);
         this.txtProxyRev.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBox_KeyDown);
         // 
         // lblProxyRev
         // 
         lblProxyRev.AutoSize = true;
         lblProxyRev.Location = new System.Drawing.Point(15, 153);
         lblProxyRev.Margin = new System.Windows.Forms.Padding(4);
         lblProxyRev.Name = "lblProxyRev";
         lblProxyRev.Size = new System.Drawing.Size(158, 26);
         lblProxyRev.TabIndex = 22;
         lblProxyRev.Text = "Proxy Revision";
         // 
         // txtModemFirmwareRev
         // 
         this.txtModemFirmwareRev.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
         this.txtModemFirmwareRev.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.txtModemFirmwareRev.Location = new System.Drawing.Point(193, 111);
         this.txtModemFirmwareRev.Margin = new System.Windows.Forms.Padding(4);
         this.txtModemFirmwareRev.Name = "txtModemFirmwareRev";
         this.txtModemFirmwareRev.Size = new System.Drawing.Size(200, 35);
         this.txtModemFirmwareRev.TabIndex = 21;
         this.txtModemFirmwareRev.TextChanged += new System.EventHandler(this.textBox_TextChanged);
         this.txtModemFirmwareRev.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBox_KeyDown);
         // 
         // lblModemFirmwareRev
         // 
         lblModemFirmwareRev.AutoSize = true;
         lblModemFirmwareRev.Location = new System.Drawing.Point(15, 115);
         lblModemFirmwareRev.Margin = new System.Windows.Forms.Padding(4);
         lblModemFirmwareRev.Name = "lblModemFirmwareRev";
         lblModemFirmwareRev.Size = new System.Drawing.Size(193, 26);
         lblModemFirmwareRev.TabIndex = 20;
         lblModemFirmwareRev.Text = "Firmware Revision";
         // 
         // txtModemSerialNumber
         // 
         this.txtModemSerialNumber.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
         this.txtModemSerialNumber.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.txtModemSerialNumber.Location = new System.Drawing.Point(193, 73);
         this.txtModemSerialNumber.Margin = new System.Windows.Forms.Padding(4);
         this.txtModemSerialNumber.Name = "txtModemSerialNumber";
         this.txtModemSerialNumber.Size = new System.Drawing.Size(200, 35);
         this.txtModemSerialNumber.TabIndex = 19;
         this.txtModemSerialNumber.TextChanged += new System.EventHandler(this.textBox_TextChanged);
         this.txtModemSerialNumber.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBox_KeyDown);
         // 
         // lblModemSerialNumber
         // 
         lblModemSerialNumber.AutoSize = true;
         lblModemSerialNumber.Location = new System.Drawing.Point(15, 77);
         lblModemSerialNumber.Margin = new System.Windows.Forms.Padding(4);
         lblModemSerialNumber.Name = "lblModemSerialNumber";
         lblModemSerialNumber.Size = new System.Drawing.Size(152, 26);
         lblModemSerialNumber.TabIndex = 18;
         lblModemSerialNumber.Text = "Serial Number";
         // 
         // txtModemPartNumber
         // 
         this.txtModemPartNumber.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
         this.txtModemPartNumber.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.txtModemPartNumber.Location = new System.Drawing.Point(193, 34);
         this.txtModemPartNumber.Margin = new System.Windows.Forms.Padding(4);
         this.txtModemPartNumber.Name = "txtModemPartNumber";
         this.txtModemPartNumber.Size = new System.Drawing.Size(200, 35);
         this.txtModemPartNumber.TabIndex = 17;
         this.txtModemPartNumber.TextChanged += new System.EventHandler(this.textBox_TextChanged);
         this.txtModemPartNumber.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBox_KeyDown);
         // 
         // lblModemPartNumber
         // 
         lblModemPartNumber.AutoSize = true;
         lblModemPartNumber.Location = new System.Drawing.Point(15, 38);
         lblModemPartNumber.Margin = new System.Windows.Forms.Padding(4);
         lblModemPartNumber.Name = "lblModemPartNumber";
         lblModemPartNumber.Size = new System.Drawing.Size(136, 26);
         lblModemPartNumber.TabIndex = 16;
         lblModemPartNumber.Text = "Part Number";
         // 
         // grpNationalInstruments
         // 
         grpNationalInstruments.BackColor = System.Drawing.Color.SkyBlue;
         grpNationalInstruments.Controls.Add(grpNISwitch);
         grpNationalInstruments.Controls.Add(grpAttenuator);
         grpNationalInstruments.Controls.Add(grpVst);
         grpNationalInstruments.Location = new System.Drawing.Point(33, 47);
         grpNationalInstruments.Name = "grpNationalInstruments";
         grpNationalInstruments.Size = new System.Drawing.Size(487, 364);
         grpNationalInstruments.TabIndex = 0;
         grpNationalInstruments.TabStop = false;
         grpNationalInstruments.Text = "National Instruments";
         // 
         // grpNISwitch
         // 
         grpNISwitch.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
         grpNISwitch.Controls.Add(this.txtSwitchSerialNumber);
         grpNISwitch.Controls.Add(lblSwitchSerialNumber);
         grpNISwitch.Location = new System.Drawing.Point(18, 261);
         grpNISwitch.Name = "grpNISwitch";
         grpNISwitch.Size = new System.Drawing.Size(448, 83);
         grpNISwitch.TabIndex = 22;
         grpNISwitch.TabStop = false;
         grpNISwitch.Text = "NI 2790";
         // 
         // txtSwitchSerialNumber
         // 
         this.txtSwitchSerialNumber.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
         this.txtSwitchSerialNumber.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.txtSwitchSerialNumber.Location = new System.Drawing.Point(218, 30);
         this.txtSwitchSerialNumber.Margin = new System.Windows.Forms.Padding(4);
         this.txtSwitchSerialNumber.Name = "txtSwitchSerialNumber";
         this.txtSwitchSerialNumber.Size = new System.Drawing.Size(200, 35);
         this.txtSwitchSerialNumber.TabIndex = 11;
         this.txtSwitchSerialNumber.TextChanged += new System.EventHandler(this.textBox_TextChanged);
         this.txtSwitchSerialNumber.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBox_KeyDown);
         // 
         // lblSwitchSerialNumber
         // 
         lblSwitchSerialNumber.AutoSize = true;
         lblSwitchSerialNumber.Location = new System.Drawing.Point(15, 33);
         lblSwitchSerialNumber.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
         lblSwitchSerialNumber.Name = "lblSwitchSerialNumber";
         lblSwitchSerialNumber.Size = new System.Drawing.Size(152, 26);
         lblSwitchSerialNumber.TabIndex = 10;
         lblSwitchSerialNumber.Text = "Serial Number";
         lblSwitchSerialNumber.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
         // 
         // grpAttenuator
         // 
         grpAttenuator.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
         grpAttenuator.Controls.Add(this.txtAttenuatorSerialNumber);
         grpAttenuator.Controls.Add(lblAttenuatorSerialNumber);
         grpAttenuator.Location = new System.Drawing.Point(18, 160);
         grpAttenuator.Name = "grpAttenuator";
         grpAttenuator.Size = new System.Drawing.Size(448, 83);
         grpAttenuator.TabIndex = 21;
         grpAttenuator.TabStop = false;
         grpAttenuator.Text = "NI 5695";
         // 
         // txtAttenuatorSerialNumber
         // 
         this.txtAttenuatorSerialNumber.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
         this.txtAttenuatorSerialNumber.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.txtAttenuatorSerialNumber.Location = new System.Drawing.Point(218, 30);
         this.txtAttenuatorSerialNumber.Margin = new System.Windows.Forms.Padding(4);
         this.txtAttenuatorSerialNumber.Name = "txtAttenuatorSerialNumber";
         this.txtAttenuatorSerialNumber.Size = new System.Drawing.Size(200, 35);
         this.txtAttenuatorSerialNumber.TabIndex = 11;
         this.txtAttenuatorSerialNumber.TextChanged += new System.EventHandler(this.textBox_TextChanged);
         this.txtAttenuatorSerialNumber.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBox_KeyDown);
         // 
         // lblAttenuatorSerialNumber
         // 
         lblAttenuatorSerialNumber.AutoSize = true;
         lblAttenuatorSerialNumber.Location = new System.Drawing.Point(15, 33);
         lblAttenuatorSerialNumber.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
         lblAttenuatorSerialNumber.Name = "lblAttenuatorSerialNumber";
         lblAttenuatorSerialNumber.Size = new System.Drawing.Size(152, 26);
         lblAttenuatorSerialNumber.TabIndex = 10;
         lblAttenuatorSerialNumber.Text = "Serial Number";
         lblAttenuatorSerialNumber.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
         // 
         // grpVst
         // 
         grpVst.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
         grpVst.Controls.Add(this.txtVstSerialNumber);
         grpVst.Controls.Add(this.txtVstCalibrationDate);
         grpVst.Controls.Add(lblVstCalibrationDate);
         grpVst.Controls.Add(lblVstSerialNumber);
         grpVst.Location = new System.Drawing.Point(18, 34);
         grpVst.Name = "grpVst";
         grpVst.Size = new System.Drawing.Size(448, 115);
         grpVst.TabIndex = 20;
         grpVst.TabStop = false;
         grpVst.Text = "NI 5644R";
         // 
         // txtVstSerialNumber
         // 
         this.txtVstSerialNumber.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
         this.txtVstSerialNumber.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.txtVstSerialNumber.Location = new System.Drawing.Point(218, 30);
         this.txtVstSerialNumber.Margin = new System.Windows.Forms.Padding(4);
         this.txtVstSerialNumber.Name = "txtVstSerialNumber";
         this.txtVstSerialNumber.Size = new System.Drawing.Size(200, 35);
         this.txtVstSerialNumber.TabIndex = 11;
         // 
         // txtVstCalibrationDate
         // 
         this.txtVstCalibrationDate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
         this.txtVstCalibrationDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.txtVstCalibrationDate.Location = new System.Drawing.Point(218, 68);
         this.txtVstCalibrationDate.Margin = new System.Windows.Forms.Padding(4);
         this.txtVstCalibrationDate.Name = "txtVstCalibrationDate";
         this.txtVstCalibrationDate.Size = new System.Drawing.Size(200, 35);
         this.txtVstCalibrationDate.TabIndex = 13;
         this.txtVstCalibrationDate.TextChanged += new System.EventHandler(this.textBox_TextChanged);
         this.txtVstCalibrationDate.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBox_KeyDown);
         // 
         // lblVstCalibrationDate
         // 
         lblVstCalibrationDate.AutoSize = true;
         lblVstCalibrationDate.Location = new System.Drawing.Point(15, 72);
         lblVstCalibrationDate.Margin = new System.Windows.Forms.Padding(4);
         lblVstCalibrationDate.Name = "lblVstCalibrationDate";
         lblVstCalibrationDate.Size = new System.Drawing.Size(168, 26);
         lblVstCalibrationDate.TabIndex = 12;
         lblVstCalibrationDate.Text = "Calibration Date";
         lblVstCalibrationDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
         // 
         // lblVstSerialNumber
         // 
         lblVstSerialNumber.AutoSize = true;
         lblVstSerialNumber.Location = new System.Drawing.Point(15, 33);
         lblVstSerialNumber.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
         lblVstSerialNumber.Name = "lblVstSerialNumber";
         lblVstSerialNumber.Size = new System.Drawing.Size(152, 26);
         lblVstSerialNumber.TabIndex = 10;
         lblVstSerialNumber.Text = "Serial Number";
         // 
         // label1
         // 
         label1.AutoSize = true;
         label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
         label1.Location = new System.Drawing.Point(35, 77);
         label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
         label1.Name = "label1";
         label1.Size = new System.Drawing.Size(136, 26);
         label1.TabIndex = 96;
         label1.Text = "Specification";
         label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
         // 
         // label5
         // 
         label5.AutoSize = true;
         label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
         label5.Location = new System.Drawing.Point(619, 109);
         label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
         label5.Name = "label5";
         label5.Size = new System.Drawing.Size(96, 26);
         label5.TabIndex = 98;
         label5.Text = "Revision";
         label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
         // 
         // label8
         // 
         label8.AutoSize = true;
         label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
         label8.Location = new System.Drawing.Point(270, 109);
         label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
         label8.Name = "label8";
         label8.Size = new System.Drawing.Size(113, 26);
         label8.TabIndex = 99;
         label8.Text = "Mnemonic";
         label8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
         // 
         // lblRegReportFolder
         // 
         this.lblRegReportFolder.AutoSize = true;
         this.lblRegReportFolder.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
         this.lblRegReportFolder.Location = new System.Drawing.Point(35, 144);
         this.lblRegReportFolder.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
         this.lblRegReportFolder.Name = "lblRegReportFolder";
         this.lblRegReportFolder.Size = new System.Drawing.Size(260, 26);
         this.lblRegReportFolder.TabIndex = 24;
         this.lblRegReportFolder.Text = "Regression Report Folder";
         this.lblRegReportFolder.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
         // 
         // tpMxSuite
         // 
         this.tpMxSuite.BackColor = System.Drawing.Color.SkyBlue;
         this.tpMxSuite.Controls.Add(this.rtbErrorWindow);
         this.tpMxSuite.Controls.Add(label8);
         this.tpMxSuite.Controls.Add(label5);
         this.tpMxSuite.Controls.Add(label1);
         this.tpMxSuite.Controls.Add(this.tbRevision);
         this.tpMxSuite.Controls.Add(this.tbMnemonic);
         this.tpMxSuite.Controls.Add(this.cmbSpecification);
         this.tpMxSuite.Controls.Add(this.ctlProgressBar);
         this.tpMxSuite.Controls.Add(this.btnGenerateReport);
         this.tpMxSuite.Controls.Add(this.btnAppendToFile);
         this.tpMxSuite.Controls.Add(this.tbAppendToFile);
         this.tpMxSuite.Controls.Add(lblAppendToFile);
         this.tpMxSuite.Controls.Add(this.chkAppendToFile);
         this.tpMxSuite.Controls.Add(this.tbRegressionReportFolder);
         this.tpMxSuite.Controls.Add(this.tbMxSuiteVersion);
         this.tpMxSuite.Controls.Add(this.btnRegressionReportFolder);
         this.tpMxSuite.Controls.Add(this.lblRegReportFolder);
         this.tpMxSuite.Controls.Add(lblMxSuiteVersion);
         this.tpMxSuite.Location = new System.Drawing.Point(4, 35);
         this.tpMxSuite.Name = "tpMxSuite";
         this.tpMxSuite.Size = new System.Drawing.Size(1119, 510);
         this.tpMxSuite.TabIndex = 3;
         this.tpMxSuite.Text = "Mx-Suite";
         // 
         // rtbErrorWindow
         // 
         this.rtbErrorWindow.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
         this.rtbErrorWindow.Location = new System.Drawing.Point(40, 292);
         this.rtbErrorWindow.Name = "rtbErrorWindow";
         this.rtbErrorWindow.ReadOnly = true;
         this.rtbErrorWindow.ShortcutsEnabled = false;
         this.rtbErrorWindow.Size = new System.Drawing.Size(1038, 187);
         this.rtbErrorWindow.TabIndex = 100;
         this.rtbErrorWindow.TabStop = false;
         this.rtbErrorWindow.Text = "";
         this.rtbErrorWindow.ZoomFactor = 0.1F;
         this.rtbErrorWindow.KeyUp += new System.Windows.Forms.KeyEventHandler(this.rtbErrorWindow_KeyUp);
         // 
         // tbRevision
         // 
         this.tbRevision.Location = new System.Drawing.Point(712, 107);
         this.tbRevision.Name = "tbRevision";
         this.tbRevision.Size = new System.Drawing.Size(86, 32);
         this.tbRevision.TabIndex = 95;
         this.tbRevision.TextChanged += new System.EventHandler(this.textBox_TextChanged);
         this.tbRevision.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBox_KeyDown);
         // 
         // tbMnemonic
         // 
         this.tbMnemonic.Location = new System.Drawing.Point(380, 107);
         this.tbMnemonic.Name = "tbMnemonic";
         this.tbMnemonic.ReadOnly = true;
         this.tbMnemonic.Size = new System.Drawing.Size(232, 32);
         this.tbMnemonic.TabIndex = 94;
         // 
         // cmbSpecification
         // 
         this.cmbSpecification.FormattingEnabled = true;
         this.cmbSpecification.Items.AddRange(new object[] {
            "Wireless Access in Vehicular Environments (WAVE) — 802.11",
            "Wireless Access in Vehicular Environments (WAVE) — Security Services",
            "Wireless Access in Vehicular Environments (WAVE) — Networking Services",
            "Wireless Access in Vehicular Environments (WAVE) — Multi-channel Operation",
            "SAE J2945/1 - On-board System Requirements for V2V Safety Communications"});
         this.cmbSpecification.Location = new System.Drawing.Point(275, 71);
         this.cmbSpecification.Name = "cmbSpecification";
         this.cmbSpecification.Size = new System.Drawing.Size(803, 34);
         this.cmbSpecification.TabIndex = 93;
         this.cmbSpecification.SelectedIndexChanged += new System.EventHandler(this.cmbSpecification_SelectedIndexChanged);
         // 
         // ctlProgressBar
         // 
         this.ctlProgressBar.Location = new System.Drawing.Point(40, 258);
         this.ctlProgressBar.Name = "ctlProgressBar";
         this.ctlProgressBar.Size = new System.Drawing.Size(1038, 22);
         this.ctlProgressBar.TabIndex = 38;
         // 
         // btnGenerateReport
         // 
         this.btnGenerateReport.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
         this.btnGenerateReport.Location = new System.Drawing.Point(477, 220);
         this.btnGenerateReport.Name = "btnGenerateReport";
         this.btnGenerateReport.Size = new System.Drawing.Size(160, 29);
         this.btnGenerateReport.TabIndex = 34;
         this.btnGenerateReport.Text = "Generate Report";
         this.btnGenerateReport.UseVisualStyleBackColor = true;
         this.btnGenerateReport.Click += new System.EventHandler(this.btnGenerateReport_Click);
         // 
         // btnAppendToFile
         // 
         this.btnAppendToFile.Enabled = false;
         this.btnAppendToFile.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
         this.btnAppendToFile.Location = new System.Drawing.Point(987, 177);
         this.btnAppendToFile.Name = "btnAppendToFile";
         this.btnAppendToFile.Size = new System.Drawing.Size(87, 32);
         this.btnAppendToFile.TabIndex = 32;
         this.btnAppendToFile.Text = "Browse";
         this.btnAppendToFile.UseVisualStyleBackColor = true;
         this.btnAppendToFile.Click += new System.EventHandler(this.btnAppendToFile_Click);
         // 
         // tbAppendToFile
         // 
         this.tbAppendToFile.Enabled = false;
         this.tbAppendToFile.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
         this.tbAppendToFile.Location = new System.Drawing.Point(275, 178);
         this.tbAppendToFile.Margin = new System.Windows.Forms.Padding(4);
         this.tbAppendToFile.Name = "tbAppendToFile";
         this.tbAppendToFile.Size = new System.Drawing.Size(705, 32);
         this.tbAppendToFile.TabIndex = 31;
         this.tbAppendToFile.Tag = "new";
         this.tbAppendToFile.TextChanged += new System.EventHandler(this.textBox_TextChanged);
         this.tbAppendToFile.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBox_KeyDown);
         // 
         // chkAppendToFile
         // 
         this.chkAppendToFile.AutoSize = true;
         this.chkAppendToFile.Location = new System.Drawing.Point(188, 187);
         this.chkAppendToFile.Name = "chkAppendToFile";
         this.chkAppendToFile.Size = new System.Drawing.Size(22, 21);
         this.chkAppendToFile.TabIndex = 29;
         this.chkAppendToFile.UseVisualStyleBackColor = true;
         this.chkAppendToFile.CheckedChanged += new System.EventHandler(this.chkAppendToFile_CheckedChanged);
         // 
         // tbRegressionReportFolder
         // 
         this.tbRegressionReportFolder.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
         this.tbRegressionReportFolder.Location = new System.Drawing.Point(275, 142);
         this.tbRegressionReportFolder.Margin = new System.Windows.Forms.Padding(4);
         this.tbRegressionReportFolder.Name = "tbRegressionReportFolder";
         this.tbRegressionReportFolder.Size = new System.Drawing.Size(705, 32);
         this.tbRegressionReportFolder.TabIndex = 25;
         this.tbRegressionReportFolder.Tag = "new";
         this.tbRegressionReportFolder.TextChanged += new System.EventHandler(this.textBox_TextChanged);
         this.tbRegressionReportFolder.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBox_KeyDown);
         // 
         // tbMxSuiteVersion
         // 
         this.tbMxSuiteVersion.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
         this.tbMxSuiteVersion.Location = new System.Drawing.Point(275, 35);
         this.tbMxSuiteVersion.Margin = new System.Windows.Forms.Padding(4);
         this.tbMxSuiteVersion.Name = "tbMxSuiteVersion";
         this.tbMxSuiteVersion.Size = new System.Drawing.Size(335, 32);
         this.tbMxSuiteVersion.TabIndex = 23;
         this.tbMxSuiteVersion.TextChanged += new System.EventHandler(this.textBox_TextChanged);
         this.tbMxSuiteVersion.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBox_KeyDown);
         // 
         // btnRegressionReportFolder
         // 
         this.btnRegressionReportFolder.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
         this.btnRegressionReportFolder.Location = new System.Drawing.Point(987, 141);
         this.btnRegressionReportFolder.Name = "btnRegressionReportFolder";
         this.btnRegressionReportFolder.Size = new System.Drawing.Size(87, 32);
         this.btnRegressionReportFolder.TabIndex = 28;
         this.btnRegressionReportFolder.Text = "Browse";
         this.btnRegressionReportFolder.UseVisualStyleBackColor = true;
         this.btnRegressionReportFolder.Click += new System.EventHandler(this.btnRegressionReportFolder_Click);
         // 
         // tcReport
         // 
         this.tcReport.Controls.Add(tpReportParameters);
         this.tcReport.Controls.Add(tpTestHardware);
         this.tcReport.Controls.Add(this.tpMxSuite);
         this.tcReport.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
         this.tcReport.Location = new System.Drawing.Point(30, 27);
         this.tcReport.Name = "tcReport";
         this.tcReport.SelectedIndex = 0;
         this.tcReport.Size = new System.Drawing.Size(1127, 549);
         this.tcReport.TabIndex = 2;
         this.tcReport.Selected += new System.Windows.Forms.TabControlEventHandler(this.tcReport_Selected);
         // 
         // Form1
         // 
         this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 27F);
         this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
         this.AutoSize = true;
         this.ClientSize = new System.Drawing.Size(1198, 598);
         this.Controls.Add(this.tcReport);
         this.Font = new System.Drawing.Font("Tahoma", 11F);
         this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
         this.Margin = new System.Windows.Forms.Padding(4);
         this.MaximizeBox = false;
         this.Name = "Form1";
         this.ShowIcon = false;
         this.Text = "MxDSRC Report Generator";
         tpReportParameters.ResumeLayout(false);
         grpApplicantInfo.ResumeLayout(false);
         grpApplicantInfo.PerformLayout();
         grpLabInfo.ResumeLayout(false);
         grpLabInfo.PerformLayout();
         ((System.ComponentModel.ISupportInitialize)(this.pbLabLogo)).EndInit();
         grpDevInfo.ResumeLayout(false);
         grpDevInfo.PerformLayout();
         ((System.ComponentModel.ISupportInitialize)(this.pbDeviceImage)).EndInit();
         tpTestHardware.ResumeLayout(false);
         grpModem.ResumeLayout(false);
         grpModem.PerformLayout();
         grpNationalInstruments.ResumeLayout(false);
         grpNISwitch.ResumeLayout(false);
         grpNISwitch.PerformLayout();
         grpAttenuator.ResumeLayout(false);
         grpAttenuator.PerformLayout();
         grpVst.ResumeLayout(false);
         grpVst.PerformLayout();
         this.tpMxSuite.ResumeLayout(false);
         this.tpMxSuite.PerformLayout();
         this.tcReport.ResumeLayout(false);
         this.ResumeLayout(false);

      }

      #endregion
      private System.Windows.Forms.TextBox tbRegressionReportFolder;
      private System.Windows.Forms.TextBox tbMxSuiteVersion;
      private System.Windows.Forms.Button btnRegressionReportFolder;
      private System.Windows.Forms.TextBox txtModemFirmwareRev;
      private System.Windows.Forms.TextBox txtModemSerialNumber;
      private System.Windows.Forms.TextBox txtModemPartNumber;
      private System.Windows.Forms.TextBox txtAttenuatorSerialNumber;
      private System.Windows.Forms.TextBox txtVstCalibrationDate;
      private System.Windows.Forms.TextBox txtVstSerialNumber;
      private System.Windows.Forms.TextBox txtApplicantAddress;
      private System.Windows.Forms.TextBox txtApplicantName;
      private System.Windows.Forms.TextBox txtLabAddress;
      private System.Windows.Forms.TextBox txtLabName;
      private System.Windows.Forms.TextBox txtTestedBy;
      private System.Windows.Forms.TextBox txtReportNumber;
      private System.Windows.Forms.TextBox txtSerialNumber;
      private System.Windows.Forms.TextBox txtDeviceProfile;
      private System.Windows.Forms.TextBox txtTciRev;
      private System.Windows.Forms.TextBox txtHardwareVersion;
      private System.Windows.Forms.TextBox txtFirmwareVersion;
      private System.Windows.Forms.TextBox txtProductName;
      private System.Windows.Forms.TextBox txtModelNumber;
      private System.Windows.Forms.TextBox txtProxyRev;
      private System.Windows.Forms.CheckBox chkAppendToFile;
      private System.Windows.Forms.Button btnAppendToFile;
      private System.Windows.Forms.TextBox tbAppendToFile;
      private System.Windows.Forms.TextBox txtDeviceDescription;
      private System.Windows.Forms.Button btnGenerateReport;
      private System.Windows.Forms.Button btnImportDeviceImage;
      private System.Windows.Forms.TabControl tcReport;
      private System.Windows.Forms.TabPage tpMxSuite;
      private System.Windows.Forms.PictureBox pbDeviceImage;
      private System.Windows.Forms.ProgressBar ctlProgressBar;
      private System.Windows.Forms.TextBox tbRevision;
      private System.Windows.Forms.TextBox tbMnemonic;
      private System.Windows.Forms.ComboBox cmbSpecification;
      private System.Windows.Forms.PictureBox pbLabLogo;
      private System.Windows.Forms.Button btnImportLabLogo;
      private System.Windows.Forms.Label lblRegReportFolder;
      private System.Windows.Forms.RichTextBox rtbErrorWindow;
      private System.Windows.Forms.TextBox txtSwitchSerialNumber;
   }
}

