﻿//******************************************************************************************
//*   Proprietary program material                                                         *
//*                                                                                        *
//*   This material is proprietary to Danlaw, Inc., and is not to be reproduced,  used     *
//*   or disclosed except in accordance with a written license agreement with Danlaw, Inc. *
//*                                                                                        *
//*   (C) Copyright 2017 Danlaw, Inc.  All rights reserved.                                *
//*                                                                                        *
//*   Danlaw, Inc., believes that the material furnished herewith is accurate and          *
//*   reliable.  However, no responsibility, financial or otherwise, can be accepted for   *
//*   any consequences arising out of the use of this material.                            *
//*                                                                                        *
//******************************************************************************************

namespace MxDSRCReportGenerator
{
   public enum DataRate
   {
      R6Mbps12BPSK,
      R9Mbps34BPSK,
      R12Mbps12QPSK, 
      R18Mbps34QPSK,
      R24Mbps1216QAM,
      R36Mbps3416QAM,
      R48Mbps2364QAM,
      R54Mbps3464QAM
   }
}