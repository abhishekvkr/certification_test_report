﻿//******************************************************************************************
//*   Proprietary program material                                                         *
//*                                                                                        *
//*   This material is proprietary to Danlaw, Inc., and is not to be reproduced,  used     *
//*   or disclosed except in accordance with a written license agreement with Danlaw, Inc. *
//*                                                                                        *
//*   (C) Copyright 2017 Danlaw, Inc.  All rights reserved.                                *
//*                                                                                        *
//*   Danlaw, Inc., believes that the material furnished herewith is accurate and          *
//*   reliable.  However, no responsibility, financial or otherwise, can be accepted for   *
//*   any consequences arising out of the use of this material.                            *
//*                                                                                        *
//******************************************************************************************

using System;
using System.Collections.Generic;
using System.Linq;

namespace MxDSRCReportGenerator
{
   public sealed class TestPurposes
   {
      public const string TP80211RxtMac01Name = "6.2.1.1 TP-80211-RXT-MAC-BV-01";
      private const string TP80211RxtMac01Summary = "Transmit WSM to IUT with MAC field values as per J2945/1 V5, Table 5 (section 8.3.2.1) and verify that IUT receives a frame.";
      private const string TP80211RxtMac01VerificationStep = "5";
      private const string TP80211RxtMac01VerificationStepDescription = "The IUT receives a WSM.";
      private const string TP80211RxtPhy01Summary = "Verify the receiver can operate at the specified minimum input sensitivity depending on the channel spacing and number.";
      private const string TP80211RxtPhy01VerificationStep = "5";
      private const string TP80211RxtPhy0XVerificationStepDescription = "The test equipment calculates the received PER and confirms it less than 10%.";
      public const string TP80211RxtPhy02Name = "6.2.4.2 TP-80211-RXT-PHY-BV-02";
      private const string TP80211RxtPhy02Summary = "To verify the OFDM adjacent channel rejection of the IUT is within conformance limits";
      private const string TP80211RxtPhy02VerificationStep = "6";
      public const string TP80211RxtPhy03Name = "6.2.4.3 TP-80211-RXT-PHY-BV-03";
      private const string TP80211RxtPhy03Summary = "Verify the OFDM non-adjacent channel rejection of IUT is within conformance limits";
      private const string TP80211RxtPhy03VerificationStep = "6";
      private const string TP80211RxtPhy04Summary = "To verify the receiver maximum input level of the IUT is within conformance limits";
      private const string TP80211RxtPhy04VerificationStep = "4";
      public const string TP80211RxtPhy05Name = "6.2.4.5 TP-80211-RXT-PHY-BV-05";
      private const string TP80211RxtPhy05Summary = "To verify the received channel power indicator (RCPI) measurement of the IUT is within conformance limits and specified dynamic range of the receiver.";
      private const string TP80211RxtPhy05VerificationStep = "4";
      private const string TP80211RxtPhy05VerificationStepDescription = "RCPI shall be a monotonically increasing, logarithmic function of the received power level and shall be equal to the received RF power within an accuracy of ±5 dB within the specified dynamic range of the receiver.";
      public const string TP80211TxtMac01Name = "6.2.2.1 TP-80211-TXT-MAC-BV-01";
      private const string TP80211TxtMac01Summary = "Transmit WSM from IUT and verify 802.11 QoS MAC formats and MAC field values";
      private const string TP80211TxtMac01VerificationStep = "4";
      private const string TP80211TxtMac01VerificationStepDescription = "Frame Control:Protocol Version = 0"
                                                                        + "\nFrame Control:Type = 2"
                                                                        + "\nFrame Control:Subtype = 8"
                                                                        + "\nFrame Control:ToDS = 0"
                                                                        + "\nFrame Control:FromDS = 0"
                                                                        + "\nFrame Control:More Fragments = 0"
                                                                        + "\nFrame Control:Retry = 0"
                                                                        + "\nFrame Control:Power Management = 0"
                                                                        + "\nFrame Control:More Data = 0"
                                                                        + "\nFrame Control:Protected Frame = 0"
                                                                        + "\nFrame Control:Order = 0"
                                                                        + "\nDuration ID = 0"
                                                                        + "\nDestination Address = FF FF FF FF FF FF"
                                                                        + "\nBSSID Address = FF FF FF FF FF FF"
                                                                        + "\nSequence Control:Fragment Number = 0"
                                                                        + "\nSequence Control:Sequence Number = Increments per frame"
                                                                        + "\nQoS Control:TID = 0 - 7"
                                                                        + "\nQoS Control:EOSP = 0"
                                                                        + "\nQoS Control:Ack Policy = 1"
                                                                        + "\nQoS Control:AMSDU = 0"
                                                                        + "\nQoS Control:TxOP Duration req = 0";
      private const string TP80211TxtPhy01Summary = "Verify Transmit spectral mask using spectrum analyzer during IUT transmission";
      private const string TP80211TxtPhy01VerificationStep = "3";
      private const string TP80211TxtPhy01VerificationStepDescription = "The Signal Analyzer captures pNumberOfFrames frames and computes the Average Spectral Mask of transmitted waveform containing the Power Spectral Density of the emissions attenuated below the output power of the transmitter as given in [2] Annex D, Table D-5(STA transmit power class C).";
      private const string TP80211TxtPhy02Summary = "Verify center frequency tolerance of IUT is within conformance limit";
      private const string TP80211TxtPhy02VerificationStep = "3";
      private const string TP80211TxtPhy02VerificationStepDescription = "The average center frequency tolerance should not exceed +/-20ppm.";
      private const string TP80211TxtPhy03Summary = "Verify symbol clock frequency tolerance of IUT is within conformance limit";
      private const string TP80211TxtPhy03VerificationStep = "3";
      private const string TP80211TxtPhy03VerificationStepDescription = "The average symbol clock frequency should not exceed +/-20ppm.";
      private const string TP80211TxtPhy04Summary = "Verify the relative constellation RMS error and transmit modulation accuracy of IUT is within conformance limit";
      private const string TP80211TxtPhy04VerificationStep = "3";
      private const string TP80211TxtPhy04VerificationStepDescription = "EVM value should not br greater than the values as shown in Relative Constellation Error Table.";
      private const string TP80211TxtPhy05Summary = "Verify the observed power is within spectral flatness conformance limits.";
      private const string TP80211TxtPhy05VerificationStep = "3";
      private const string TP80211TxtPhy05VerificationStepDescription = "Energy magnitude in each subcarrier to be within limits defined in Average Energy Deviation Table.";
      private const string TP80211TxtPhy06Summary = "Verify transmitter center frequency leakage is within conformance limits.";
      private const string TP80211TxtPhy06VerificationStep = "3";
      private const string TP80211TxtPhy06VerificationStepDescription = "Average power at center frequency shall not exceed overall transmit power of -15dB or value shall not exceed the average energy of rest of the sub carriers +2dB.";
      public const string TP80211TxtPhy07Name = "6.2.3.7 TP-80211-TXT-PHY-BV-07";
      private const string TP80211TxtPhy07Summary = "Verify transmitter power is a monotonically increasing function of vTxPwrCtrlStep.";
      private const string TP80211TxtPhy07VerificationStep3 = "3";
      private const string TP80211TxtPhy07VerificationStep3Description = "Transmit power measured at the antenna connector shall be within +/-2dB of its setting value set to pPower over 95% of test measurements";
      private const string TP80211TxtPhy07VerificationStep6 = "6";
      private const string TP80211TxtPhy07VerificationStep6Description = "A series of arithmetic mean calculated from transmit powers measured by the Signal Analyzer in step 3 for each pPower value defined in step 5 is monotonically increasing (i.e. preceding value is smaller or equal then the next value in the series)";

      public static readonly List<string> TP16092 = new List<string> { "TP-16092-SPDUBSM-SEND-BV-01",        "TP-16092-SPDUBSM-SEND-BV-02",        "TP-16092-SPDUBSM-SEND-BV-03",        "TP-16092-SPDUBSM-SEND-BV-04",
                                                                "TP-16092-SPDUBSM-SEND-BV-05",        "TP-16092-SPDUBSM-SEND-BV-06",        "TP-16092-SPDUBSM-RECV-BV-01",        "TP-16092-SPDUBSM-RECV-BV-02",
                                                                "TP-16092-SPDUBSM-RECV-BV-03",        "TP-16092-SPDUBSM-RECV-BV-04",        "TP-16092-SPDUBSM-RECV-BV-05",        "TP-16092-SPDUBSM-CERTCHG-BV-01",
                                                                "TP-16092-SPDUBSM-RECV-BI-01",        "TP-16092-SPDUBSM-RECV-BI-02",        "TP-16092-SPDUWSA-SEND-BV-01",        "TP-16092-SPDUWSA-SEND-BV-02",
                                                                "TP-16092-SPDUWSA-SEND-BV-03",        "TP-16092-SPDUWSA-SEND-BV-04",        "TP-16092-SPDUWSA-RECV-BV-01",        "TP-16092-SPDUWSA-RECV-BV-02",
                                                                "TP-16092-SPDUWSA-RECV-BV-03",        "TP-16092-SPDUWSA-RECV-BI-01"};
      public static readonly List<string> TP16093 = new List<string> { "TP-16093-WSM-MST-BV-01",             "TP-16093-WSM-MST-BV-02",             "TP-16093-WSM-ROP-BV-01",             "TP-16093-WSM-ROP-BV-02",
                                                                "TP-16093-WSM-ROP-BV-03",             "TP-16093-WSM-PP-BV-01",              "TP-16093-WSM-PP-BV-02",              "TP-16093-WSM-COM-BV-01",
                                                                "TP-16093-WSM-COM-BV-02",             "TP-16093-WSM-COM-BV-03",             "TP-16093-WSM-COM-BV-04",             "TP-16093-WSM-COM-BV-05",
                                                                "TP-16093-WSM-POP-BI-01",             "TP-16093-WSA-MST-BV-01",             "TP-16093-WSA-MST-BV-02",             "TP-16093-WSA-MST-BV-03",
                                                                "TP-16093-WSA-MST-BV-04-A",           "TP-16093-WSA-MST-BV-04-B",           "TP-16093-WSA-MST-BV-04-C",           "TP-16093-WSA-MST-BV-05-A",
                                                                "TP-16093-WSA-MST-BV-05-B",           "TP-16093-WSA-MST-BV-05-C",           "TP-16093-WSA-MST-BV-05-D",           "TP-16093-WSA-MST-BV-05-E",
                                                                "TP-16093-WSA-MST-BV-05-F",           "TP-16093-WSA-MST-BV-05-G",           "TP-16093-WSA-MST-BV-06-A",           "TP-16093-WSA-MST-BV-06-B",
                                                                "TP-16093-WSA-MST-BV-07-A",           "TP-16093-WSA-MST-BV-07-B",           "TP-16093-WSA-MST-BV-08",             "TP-16093-WSA-PP-BV-01",
                                                                "TP-16093-WSA-PP-BV-02",              "TP-16093-WSA-PP-BV-03",              "TP-16093-WSA-PP-BV-04",              "TP-16093-WSA-ROP-BV-01",
                                                                "TP-16093-WSA-CHG-BV-01",             "TP-16093-WSA-CHG-BV-02",             "TP-16093-IP-CFG-BV-01",              "TP-16093-IP-CFG-BV-02",
                                                                "TP-16093-IP-CHG-BV-01",              "TP-16093-IP-CHG-BV-02",              "TP-16093-IP-COM-BV-01",              "TP-16093-IP-COM-BV-02"};
      public static readonly List<string> TP16094 = new List<string> { "TP-16094-RXT-MDE-BV-01",             "TP-16094-RXT-MDE-BV-02",             "TP-16094-TXT-MDE-BV-01",             "TP-16094-TXT-MDE-BV-02",
                                                                "TP-16094-TXT-IP6-BV-01",             "TP-16094-TXT-PER-BV-01",             "TP-16094-TXT-PER-BV-02",             "TP-16094-TXT-PER-BV-03" };
      public static readonly List<string> TP29451 = new List<string> { "TP-BSM-ST-BV-01-01",                 "TP-BSM-ST-BV-01-02",                 "TP-BSM-ST-BV-01-03",                 "TP-BSM-ST-BV-01-04",
                                                                "TP-BSM-ST-BV-01-05",                 "TP-BSM-ST-BV-01-06",                 "TP-BSM-ST-BV-01-07",                 "TP-BSM-ST-BV-01-08",
                                                                "TP-BSM-ST-BV-01-09",                 "TP-BSM-ST-BV-01-10",                 "TP-BSM-ST-BV-01-11",                 "TP-BSM-ST-BV-01-12",
                                                                "TP-BSM-ST-BV-01-13",                 "TP-BSM-ST-BV-01-14",                 "TP-BSM-ST-BV-01-15",                 "TP-BSM-ST-BV-02",
                                                                "TP-BSM-ST-BV-03-01",                 "TP-BSM-ST-BV-03-02",                 "TP-BSM-ST-BV-03-03",                 "TP-BSM-ST-BV-03-04",
                                                                "TP-BSM-ST-BV-03-05",                 "TP-BSM-ST-BV-03-06",                 "TP-BSM-ST-BV-03-07",                 "TP-BSM-ST-BV-03-08",
                                                                "TP-BSM-ST-BV-03-09",                 "TP-BSM-ST-BV-03-10",                 "TP-BSM-ST-BV-03-11",                 "TP-BSM-ST-BV-03-12",
                                                                "TP-BSM-ST-BV-03-13",                 "TP-BSM-ST-BV-03-14",                 "TP-BSM-ST-BV-03-15",                 "TP-BSM-ST-BV-03-16",
                                                                "TP-BSM-ST-BV-03-17",                 "TP-BSM-ST-BV-03-18",                 "TP-BSM-ST-BV-03-19",                 "TP-BSM-ST-BV-03-20",
                                                                "TP-BSM-ST-BV-03-21",                 "TP-BSM-ST-BV-03-22",                 "TP-BSM-ST-BV-03-23",                 "TP-BSM-ST-BV-03-24",
                                                                "TP-BSM-ST-BV-03-25",                 "TP-BSM-ST-BV-03-26",                 "TP-BSM-ST-BV-03-27",                 "TP-BSM-ST-BV-03-28",
                                                                "TP-BSM-ST-BV-03-29",                 "TP-BSM-ST-BV-03-30",                 "TP-BSM-ST-BV-03-31",                 "TP-BSM-ST-BV-03-32",
                                                                "TP-BSM-ST-BV-03-33",                 "TP-BSM-ST-BV-03-34",                 "TP-BSM-ST-BV-03-35",                 "TP-BSM-ST-BV-03-36",
                                                                "TP-BSM-ST-BV-03-37",                 "TP-BSM-ST-BV-03-38",                 "TP-BSM-ST-BV-04",                    "TP-BSM-ST-BV-05",
                                                                "TP-BSM-ST-BV-06",                    "TP-BSM-ST-BV-07",                    "TP-BSM-ST-BV-08",                    "TP-BSM-ST-BV-09",
                                                                "TP-BSM-ST-BV-10-01",                 "TP-BSM-ST-BV-10-02",                 "TP-BSM-ST-BV-10-03",                 "TP-BSM-ST-BV-10-04",
                                                                "TP-BSM-ST-BV-10-05",                 "TP-BSM-ST-BV-10-06",                 "TP-BSM-ST-BV-10-07",                 "TP-BSM-ST-BV-10-08",
                                                                "TP-BSM-ST-BV-10-09",                 "TP-BSM-ST-BV-10-10",                 "TP-BSM-ST-BV-10-11",                 "TP-BSM-ST-BV-10-12",
                                                                "TP-BSM-ST-BV-10-13",                 "TP-BSM-ST-BV-10-14",                 "TP-BSM-ST-BV-10-15",                 "TP-BSM-ST-BV-11",
                                                                "TP-BSM-ST-BV-12",                    "TP-BSM-ST-BV-13",                    "TP-BSM-ST-BV-15",                    "TP-BSM-ST-BV-17-01",
                                                                "TP-BSM-ST-BV-17-02",                 "TP-BSM-ST-BV-17-03",                 "TP-BSM-ST-BV-17-04",                 "TP-BSM-ST-BV-17-05",
                                                                "TP-BSM-ST-BV-17-06",                 "TP-BSM-ST-BV-17-07",                 "TP-BSM-ST-BV-17-08",                 "TP-BSM-ST-BV-17-09",
                                                                "TP-BSM-ST-BV-17-10",                 "TP-BSM-ST-BV-17-11",                 "TP-BSM-ST-BV-17-12",                 "TP-BSM-ST-BV-17-13",
                                                                "TP-BSM-ST-BV-17-14",                 "TP-BSM-ST-BV-17-15",                 "TP-BSM-MV-BV-01",                    "TP-BSM-ST-BV-18",
                                                                "TP-BSM-MV-BV-03",                    "TP-BSM-MV-BV-04",                    "TP-BSM-MV-BV-05",                    "TP-BSM-MV-BV-06",
                                                                "TP-BSM-MV-BV-07-01",                 "TP-BSM-MV-BV-07-02",                 "TP-BSM-MV-BV-07-03",                 "TP-BSM-MV-BV-07-04",
                                                                "TP-BSM-MV-BV-07-05",                 "TP-BSM-MV-BV-07-06",                 "TP-BSM-MV-BV-07-07",                 "TP-BSM-MV-BV-07-08",
                                                                "TP-BSM-MV-BV-07-09",                 "TP-BSM-MV-BV-07-10",                 "TP-BSM-MV-BV-07-11",                 "TP-BSM-MV-BV-07-12",
                                                                "TP-BSM-MV-BV-07-13",                 "TP-BSM-MV-BV-07-14",                 "TP-BSM-MV-BV-07-15",                 "TP-BSM-MV-BV-08",
                                                                "TP-BSM-MV-BV-09",                    "TP-BSM-MV-BV-10",                    "TP-BSM-MV-BV-11",                    "TP-BSM-MV-BV-12",
                                                                "TP-BSM-MV-BV-13",                    "TP-BSM-MV-BV-14",                    "TP-BSM-ST-BI-19" };
      public static readonly List<string> TP80211 = new List<string> { "6.2.2.1 TP-80211-TXT-MAC-BV-01",     "6.2.3.1 TP-80211-TXT-PHY-BV-01",     "6.2.3.2 TP-80211-TXT-PHY-BV-02",     "6.2.3.3 TP-80211-TXT-PHY-BV-03",
                                                                "6.2.3.4 TP-80211-TXT-PHY-BV-04",     "6.2.3.5 TP-80211-TXT-PHY-BV-05",     "6.2.3.6 TP-80211-TXT-PHY-BV-06",     TestPurposes.TP80211TxtPhy07Name,
                                                                TestPurposes.TP80211RxtMac01Name, "6.2.4.1 TP-80211-RXT-PHY-BV-01",     TestPurposes.TP80211RxtPhy02Name, TestPurposes.TP80211RxtPhy03Name,
                                                                "6.2.4.4 TP-80211-RXT-PHY-BV-04",     TestPurposes.TP80211RxtPhy05Name };
      private readonly List<string[]> TP80211VerificationSteps = new List<string[]> {  new string[] { TestPurposes.TP80211TxtMac01VerificationStep },
                                                                                       new string[] { TestPurposes.TP80211TxtPhy01VerificationStep },
                                                                                       new string[] { TestPurposes.TP80211TxtPhy02VerificationStep },
                                                                                       new string[] { TestPurposes.TP80211TxtPhy03VerificationStep },
                                                                                       new string[] { TestPurposes.TP80211TxtPhy04VerificationStep },
                                                                                       new string[] { TestPurposes.TP80211TxtPhy05VerificationStep },
                                                                                       new string[] { TestPurposes.TP80211TxtPhy06VerificationStep },
                                                                                       new string[] { TestPurposes.TP80211TxtPhy07VerificationStep3, TestPurposes.TP80211TxtPhy07VerificationStep6},
                                                                                       new string[] { TestPurposes.TP80211RxtMac01VerificationStep },
                                                                                       new string[] { TestPurposes.TP80211RxtPhy01VerificationStep },
                                                                                       new string[] { TestPurposes.TP80211RxtPhy02VerificationStep },
                                                                                       new string[] { TestPurposes.TP80211RxtPhy03VerificationStep },
                                                                                       new string[] { TestPurposes.TP80211RxtPhy04VerificationStep },
                                                                                       new string[] { TestPurposes.TP80211RxtPhy05VerificationStep }};
      private readonly List<string[]> TP80211VerificationStepDescriptions = new List<string[]> { new string[] { TestPurposes.TP80211TxtMac01VerificationStepDescription },
                                                                                                 new string[] { TestPurposes.TP80211TxtPhy01VerificationStepDescription },
                                                                                                 new string[] { TestPurposes.TP80211TxtPhy02VerificationStepDescription },
                                                                                                 new string[] { TestPurposes.TP80211TxtPhy03VerificationStepDescription },
                                                                                                 new string[] { TestPurposes.TP80211TxtPhy04VerificationStepDescription },
                                                                                                 new string[] { TestPurposes.TP80211TxtPhy05VerificationStepDescription },
                                                                                                 new string[] { TestPurposes.TP80211TxtPhy06VerificationStepDescription },
                                                                                                 new string[] { TestPurposes.TP80211TxtPhy07VerificationStep3Description, TestPurposes.TP80211TxtPhy07VerificationStep6Description},
                                                                                                 new string[] { TestPurposes.TP80211RxtMac01VerificationStepDescription },
                                                                                                 new string[] { TestPurposes.TP80211RxtPhy0XVerificationStepDescription },
                                                                                                 new string[] { TestPurposes.TP80211RxtPhy0XVerificationStepDescription },
                                                                                                 new string[] { TestPurposes.TP80211RxtPhy0XVerificationStepDescription },
                                                                                                 new string[] { TestPurposes.TP80211RxtPhy0XVerificationStepDescription },
                                                                                                 new string[] { TestPurposes.TP80211RxtPhy05VerificationStepDescription }};      
      public static readonly List<string> TPV2I = new List<string> { "TP-RSU-SNMP-FUN-BV-01",   "TP-RSU-SNMP-FUN-BV-02",       "TP-RSU-SNMP-POS-BV-01", "TP-RSU-SNMP-SAR-BV-01", "TP-RSU-SNMP-NoT-BV-01", "TP-RSU-SNMP-NoT-BV-02", "TP-RSU-SNMP-NoT-BV-03",
                                                              "TP-OBU-MSG-BV-01",        "TP-RSU-MSG-BV-02",            "TP-RSU-MSG-BV-01",      "TP-RSU-MSG-BV-02",      "TP-RSU-MSG-BV-03",      "TP-RSU-MSG-BV-04",      "TP-RSU-16094-MCTXRX-BV-01",
                                                              "TP-RSU-1609-4-TXT-BV-01", "TP-RSU-1609-4-RXT-TXT-BV-01", "TP-RSU-POS-FUN-BV-01",  "TP-RSU-POS-FUN-BV-02",  "TP-RSU-POS-FUN-BV-03",  "TP-RSU-WSA-V2I-BV-01",  "TP-RSU-WSA-FUN-BV-01",
                                                              "TP-RSU-WSA-FUN-BV-02",    "TP-RSU-WSA-FUN-BV-03",        "TP-OBU-WSA-FUN-BV-01",  "TP-OBU-WSA-FUN-BV-02",  "TP-OBU-WSA-FUN-BV-03"};

      private List<string> my80211Summaries = new List<string> { TestPurposes.TP80211TxtMac01Summary, TestPurposes.TP80211TxtPhy01Summary, TestPurposes.TP80211TxtPhy02Summary,
                                                                 TestPurposes.TP80211TxtPhy03Summary, TestPurposes.TP80211TxtPhy04Summary, TestPurposes.TP80211TxtPhy05Summary,
                                                                 TestPurposes.TP80211TxtPhy06Summary, TestPurposes.TP80211TxtPhy07Summary, TestPurposes.TP80211RxtMac01Summary,
                                                                 TestPurposes.TP80211RxtPhy01Summary, TestPurposes.TP80211RxtPhy02Summary, TestPurposes.TP80211RxtPhy03Summary,
                                                                 TestPurposes.TP80211RxtPhy04Summary, TestPurposes.TP80211RxtPhy05Summary };
      private Dictionary<string, string> my80211Summary = new Dictionary<string, string>();
      private Dictionary<string, string[]> my80211VerificationStep = new Dictionary<string, string[]>();
      private Dictionary<string, string[]> my80211VerificationStepDescription = new Dictionary<string, string[]>();

      public TestPurposes()
      {
         for (int index = 0; index < TestPurposes.TP80211.Count; index++)
         {
            this.my80211Summary.Add(TestPurposes.TP80211[index], this.my80211Summaries[index]);
            this.my80211VerificationStep.Add(TestPurposes.TP80211[index], this.TP80211VerificationSteps[index]);
            this.my80211VerificationStepDescription.Add(TestPurposes.TP80211[index], this.TP80211VerificationStepDescriptions[index]);
         }
      }

      public string Get802dot11TestPurposeSummary(string testPurpose)
      {
         string summary = string.Empty;
         this.my80211Summary?.TryGetValue(testPurpose, out summary);
         return summary;
      }
      public string[] Get802dot11TestPurposeVerificationStep(string testPurpose)
      {
         string[] steps = new string[2];
         this.my80211VerificationStep?.TryGetValue(testPurpose, out steps);
         return steps;
      }
      public string[] Get802dot11TestPurposeVerificationStepDescription(string testPurpose)
      {
         string[] descriptions = new string[2];
         this.my80211VerificationStepDescription?.TryGetValue(testPurpose, out descriptions);
         return descriptions;
      }
   }
}
