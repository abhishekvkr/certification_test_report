﻿//******************************************************************************************
//*   Proprietary program material                                                         *
//*                                                                                        *
//*   This material is proprietary to Danlaw, Inc., and is not to be reproduced,  used     *
//*   or disclosed except in accordance with a written license agreement with Danlaw, Inc. *
//*                                                                                        *
//*   (C) Copyright 2017 Danlaw, Inc.  All rights reserved.                                *
//*                                                                                        *
//*   Danlaw, Inc., believes that the material furnished herewith is accurate and          *
//*   reliable.  However, no responsibility, financial or otherwise, can be accepted for   *
//*   any consequences arising out of the use of this material.                            *
//*                                                                                        *
//******************************************************************************************

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml;

namespace MxDSRCReportGenerator
{
   internal class Processing80211RegressionResults
   {
      private const string CsvFileSearchString = "*.csv";
      public const string InterferingChannelNumber = "Interfering Signal Channel Number";
      public const string InterferingChannelPower = "Interfering Signal Channel Power";
      public const string ChannelWidth = "Test Channel Width";
      public const string DataRate = "SUT Data Rate";
      private const string IsTestStepMonotonic = "IsTestStepMonotonic";
      private const string IsMonotonic = "IsMonotonic";
      private const string NumberOfSamples = "pNumberOfFrames";
      private const string Payload = "Payload";
      private const string PduData = "Pdu Data";
      private const string PER = "PER";
      private const string PowerLevelAvg = "Power Level Avg";
      private const string PowerLevelMax = "Power Level Max";
      private const string PowerLevelMin = "Power Level Min";
      private const string PowerLevelStdDev = "Power Level StdDev";
      private const string RegressionLogFileSearchString = "*.mxrlg";
      private const string RCPIAccuracy = "RCPIAccuracy";
      private const string RCPIAvg = "RCPI Avg";
      private const string RCPIStdDev = "RCPI StdDev";
      private const string ScenarioFolderName = "ScenariosAndTestCases";
      private const string ScenarioLogFileSearchString = "*.mxlog";
      private const string ScenarioOutputDirectorySearchString = "*.mxout";
      private const string ScenarioRunEventsFileSearchString = "*.mxevt";
      private const string SequenceNumber = "Sequence Number Out";
      public const string TestChannelNumber = "Test Channel Number";
      public const string TestChannelRxPower = "Test Channel Power";
      public const string TestChannelTxPower = "pTxPowerDefault";
      private const string TxPowerAnalysis = "TxPwrAccAnalysis";
      private const string TestAnalysisOut = "TestAnalysisOut";
      private const string MxVRunTimeEvent = "MxV RunTime Event";
      private readonly List<string> StimulusSignals = new List<string> { InterferingChannelNumber,             InterferingChannelPower,        ChannelWidth,                   DataRate,
                                                                         TestChannelNumber,                    TestChannelTxPower,             TestChannelRxPower,             NumberOfSamples };
      private readonly List<string> ResultSignals = new List<string>   { "BSSID Out",                          "Carrier Frequency Offset Avg", "Carrier Frequency Offset Max", "Carrier Frequency Offset Min",
                                                                         "Carrier Leakage Avg",                "Carrier Leakage Max",          "Carrier Leakage Min",          "Destination Mac Out",
                                                                         "Duration Out",                       "Error Vector Magnitude Rms",   "Fragment Number Out",          "Frame Control More Data Out",
                                                                         "Frame Control From Ds Out",          "Frame Control More Flags Out", "Frame Control Order Out",      "Frame Control Power Management Out",
                                                                         "Frame Control Protocol Version Out", "Frame Control Retry Out",      "Frame Control Subtype Out",    "Frame Control To Ds Out",
                                                                         "Frame Control Type Out",             "Frame Control Wep Out",        PER,                            IsTestStepMonotonic,
                                                                         IsMonotonic,                          PduData,                        PowerLevelAvg,                  PowerLevelMax,
                                                                         PowerLevelMin,                        PowerLevelStdDev,               RCPIAccuracy,                   RCPIAvg,
                                                                         RCPIStdDev,                           "Sample Clock Offset Avg",      "Sample Clock Offset Max",      "Sample Clock Offset Min",
                                                                         SequenceNumber,                       "SFM Subcarrier Index",         "Spectral Flatness Margin Avg", "Spectral Flatness Margin Max",
                                                                         "Spectral Flatness Margin Min",       "Spectral Mask Violation",      "SUT Rx Indications Count",     TestAnalysisOut,
                                                                         TxPowerAnalysis,                      "QoS Ack Policy Out",           "QoS EOSp Out",                 "QoS AMSDU Out",
                                                                         "QoS TID Out",                        "Qos TXOP Duration Req Out",    MxVRunTimeEvent};
      private readonly List<string> TestCaseInputs = new List<string>  { "Destination MAC Address",            "PSID",                         Payload,                        "Radio",
                                                                         "Security Signer Id Type",            "Time Slot",                    "User Priority", };

      private string myMxVRegressionLogFile = string.Empty;
      private string myMxVRegressionFolder = string.Empty;
      private List<string> myMxOutFolders = new List<string>();
      private string myScenariosFolder = string.Empty;
      private List<string> myScenarioLogs = new List<string>();
      private List<MxDSRCReportGenerator.ResultSummary> myScenarioResults = new List<MxDSRCReportGenerator.ResultSummary>();

      public Processing80211RegressionResults(string regressionFolder, ref List<string> runTimeErrors)
      {
         this.myMxVRegressionFolder = regressionFolder;
         this.myMxVRegressionLogFile = Directory.GetFiles(regressionFolder, Processing80211RegressionResults.RegressionLogFileSearchString).FirstOrDefault();
         this.myScenariosFolder = Directory.GetDirectories(regressionFolder, Processing80211RegressionResults.ScenarioFolderName).FirstOrDefault();
         this.myMxOutFolders = Directory.GetDirectories(this.myScenariosFolder, Processing80211RegressionResults.ScenarioOutputDirectorySearchString).ToList();
         this.myScenarioLogs = Directory.GetFiles(this.myScenariosFolder, Processing80211RegressionResults.ScenarioLogFileSearchString).ToList();
         List<string> scenarioRunEventsFiles = Directory.GetFiles(this.myScenariosFolder, Processing80211RegressionResults.ScenarioRunEventsFileSearchString).ToList();
         if (scenarioRunEventsFiles?.Count > 0)
         {
            this.ReadMxVEvtFiles(scenarioRunEventsFiles, out runTimeErrors);
         }
         this.ReadMxVRegressionLogFile();
      }

      public List<MxDSRCReportGenerator.ResultSummary> ScenarioResults
      {
         get { return this.myScenarioResults; }
      }

      private void CreateLogFileForRxtPhyBV05(List<List<Tuple<string, object, string>>> iterationInputs, List<List<Tuple<string, object, string>>> iterationResults)
      {
         string scenarioLog = this.myScenarioLogs.FirstOrDefault(item => item.Contains(TestPurposes.TP80211RxtPhy05Name));
         string folder = this.myMxOutFolders.FirstOrDefault(item => item.Contains(TestPurposes.TP80211RxtPhy05Name));
         string file = string.Empty;
         FileStream logFileStream;
         string text;
         byte[] buffer;

         for (int iteration = 0; iteration < iterationInputs.Count; iteration++)
         {
            string channel = iterationInputs[iteration].Find(item => item.Item1 == Processing80211RegressionResults.TestChannelNumber).Item2.ToString();
            int datarate = System.Convert.ToInt32(iterationInputs[iteration].Find(item => item.Item1 == Processing80211RegressionResults.DataRate).Item2);
            string datarateName = Enum.GetName(typeof(DataRate), datarate);
            file = Path.Combine(folder, $"{TestPurposes.TP80211RxtPhy05Name} Summary_{channel}_{datarateName}.csv");
            logFileStream = File.Create(file);
            text = "pPower,RCPI Avg,RCPI StdDev,Measured Power Avg,Measured Power StdDev,(pPower-Avg Pwr),Rx Power Accuracy (+/-5dB),Is Monotonic\r\n";
            buffer = System.Text.ASCIIEncoding.ASCII.GetBytes(text);
            logFileStream.Write(buffer, 0, buffer.Length);
            logFileStream.Flush();

            List<Tuple<string, object, string>> pPower = iterationInputs[iteration].FindAll(item => item.Item1 == Processing80211RegressionResults.TestChannelRxPower);
            List<Tuple<string, object, string>> rcpiAvg = iterationResults[iteration].FindAll(item => item.Item1 == Processing80211RegressionResults.RCPIAvg);
            List<Tuple<string, object, string>> rcpiStdDev = iterationResults[iteration].FindAll(item => item.Item1 == Processing80211RegressionResults.RCPIStdDev);
            List<Tuple<string, object, string>> powerAvg = iterationResults[iteration].FindAll(item => item.Item1 == Processing80211RegressionResults.PowerLevelAvg);
            List<Tuple<string, object, string>> powerStdDev = iterationResults[iteration].FindAll(item => item.Item1 == Processing80211RegressionResults.PowerLevelStdDev);
            List<Tuple<string, object, string>> testStepMonotonicity = iterationResults[iteration].FindAll(item => item.Item1 == Processing80211RegressionResults.IsTestStepMonotonic);
            List<Tuple<string, object, string>> rcpiAccuracy = iterationResults[iteration].FindAll(item => item.Item1 == Processing80211RegressionResults.RCPIAccuracy);

            for (int i = 0; i < pPower.Count; i++)
            {
               // First test step can never be false.
               string isTestStepMonotonic = i > 0 ? (!System.Convert.ToBoolean(testStepMonotonicity[i].Item2)).ToString() : string.Empty;
               string isRcpiAccurate = (!System.Convert.ToBoolean(rcpiAccuracy[i].Item2)).ToString();
               double diff = (double)pPower[i].Item2 - (double)powerAvg[i].Item2;
               text = $"{pPower[i].Item2.ToString()},{rcpiAvg[i].Item2.ToString()},{rcpiStdDev[i].Item2.ToString()},{powerAvg[i].Item2.ToString()},{powerStdDev[i].Item2.ToString()},{diff},{isRcpiAccurate},{isTestStepMonotonic}\r\n";
               buffer = System.Text.ASCIIEncoding.ASCII.GetBytes(text);
               logFileStream.Write(buffer, 0, buffer.Length);
               logFileStream.Flush();
            }

            logFileStream.Close();
            logFileStream.Dispose();
         }
      }
      private void CreateLogFileForTxtPhyBV07(List<List<Tuple<string, object, string>>> iterationInputs, List<List<Tuple<string, object, string>>> iterationResults)
      {
         string scenarioLog = this.myScenarioLogs.FirstOrDefault(item => item.Contains(TestPurposes.TP80211TxtPhy07Name));
         string folder = this.myMxOutFolders.FirstOrDefault(item => item.Contains(TestPurposes.TP80211TxtPhy07Name));
         string file = string.Empty;
         FileStream logFileStream;
         string text;
         byte[] buffer;

         for (int iteration = 0; iteration < iterationInputs.Count; iteration++)
         {
            string channel = iterationInputs[iteration].Find(item => item.Item1 == Processing80211RegressionResults.TestChannelNumber).Item2.ToString();
            int datarate = System.Convert.ToInt32(iterationInputs[iteration].Find(item => item.Item1 == Processing80211RegressionResults.DataRate).Item2);
            string datarateName = Enum.GetName(typeof(DataRate), datarate);
            int numberOfSamples = System.Convert.ToInt32(iterationInputs[iteration].Find(item => item.Item1 == Processing80211RegressionResults.NumberOfSamples).Item2); ;

            file = Path.Combine(folder, $"{TestPurposes.TP80211TxtPhy07Name} Summary_{channel}_{datarateName}.csv");
            logFileStream = File.Create(file);
            text = "Tx Power Accuracy is determined by: Mean upper limit<(pPower+2) AND Mean lower limit>(pPower-2)\r\n"
               + "For 95% Confidence interval:\r\n"
               + "Mean upper limit = Measured Power Avg + 1.96 * Measured Power StdDev\r\n"
               + "Mean lower limit = Measured Power Avg - 1.96 * Measured Power StdDev\r\n"
               + $"NumberOfSamples= {numberOfSamples}\r\n\r\n";
            buffer = System.Text.ASCIIEncoding.ASCII.GetBytes(text);
            logFileStream.Write(buffer, 0, buffer.Length);

            text = "pPower,Measured Power Avg,Measured Power Max,Measured Power Min,Measured Power StdDev,Mean upper limit,Mean lower limit,Tx Power Accuracy,Is Monotonic\r\n";
            buffer = System.Text.ASCIIEncoding.ASCII.GetBytes(text);
            logFileStream.Write(buffer, 0, buffer.Length);
            logFileStream.Flush();

            List<Tuple<string, object, string>> pPower = iterationInputs[iteration].FindAll(item => item.Item1 == Processing80211RegressionResults.TestChannelTxPower);
            List<Tuple<string, object, string>> powerAvg = iterationResults[iteration].FindAll(item => item.Item1 == Processing80211RegressionResults.PowerLevelAvg);
            List<Tuple<string, object, string>> powerMax = iterationResults[iteration].FindAll(item => item.Item1 == Processing80211RegressionResults.PowerLevelMax);
            List<Tuple<string, object, string>> powerMin = iterationResults[iteration].FindAll(item => item.Item1 == Processing80211RegressionResults.PowerLevelMin);
            List<Tuple<string, object, string>> powerStdDev = iterationResults[iteration].FindAll(item => item.Item1 == Processing80211RegressionResults.PowerLevelStdDev);
            List<Tuple<string, object, string>> txPowerAccuracy = iterationResults[iteration].FindAll(item => item.Item1 == Processing80211RegressionResults.TxPowerAnalysis);
            List<Tuple<string, object, string>> testStepMonotonicity = iterationResults[iteration].FindAll(item => item.Item1 == Processing80211RegressionResults.IsTestStepMonotonic);

            for (int i = 0; i < pPower.Count; i++)
            {
               // First test step can never be false.
               string isTestStepMonotonic = i > 0 ? (!System.Convert.ToBoolean(testStepMonotonicity[i].Item2)).ToString() : string.Empty;
               string istxPowerAccurate = (!System.Convert.ToBoolean(txPowerAccuracy[i].Item2)).ToString();
               double upperMean = System.Convert.ToDouble(powerAvg[i].Item2) + (1.96 * System.Convert.ToDouble(powerStdDev[i].Item2));
               double lowerMean = System.Convert.ToDouble(powerAvg[i].Item2) - (1.96 * System.Convert.ToDouble(powerStdDev[i].Item2));
               text = $"{pPower[i].Item2.ToString()},{powerAvg[i].Item2.ToString()},{powerMax[i].Item2.ToString()},{powerMin[i].Item2.ToString()},{powerStdDev[i].Item2.ToString()},{upperMean},{lowerMean},{istxPowerAccurate},{isTestStepMonotonic}\r\n";
               buffer = System.Text.ASCIIEncoding.ASCII.GetBytes(text);
               logFileStream.Write(buffer, 0, buffer.Length);
               logFileStream.Flush();
            }

            logFileStream.Close();
            logFileStream.Dispose();
         }
      }
      private int GetSkippedIterationCountFromEvtFile(XmlReader reader, string scenarioName)
      {
         int count = 0;
         while (reader.ReadToFollowing("note"))
         {
            //string details = 
            if (reader.GetAttribute("details").Contains(scenarioName) && reader.GetAttribute("details").Contains("was terminated early"))
            {
               count++;
            }
         }
         return count;
      }
      public void GetIterationInformation(string scenarioName, out List<List<Tuple<string, object, string>>> iterationInputs,
                                                               out List<List<Tuple<string, object, string>>> iterationResults,
                                                               out List<string> iterationVerdict)
      {
         string scenarioLog = this.myScenarioLogs.FirstOrDefault(item => item.Contains(scenarioName));
         iterationVerdict = this.ReadMxVLogFile(scenarioName, scenarioLog, out iterationInputs, out iterationResults);
         if (scenarioName == TestPurposes.TP80211TxtPhy07Name)
         {
            this.CreateLogFileForTxtPhyBV07(iterationInputs, iterationResults);
         }
         else if (scenarioName == TestPurposes.TP80211RxtPhy05Name)
         {
            this.CreateLogFileForRxtPhyBV05(iterationInputs, iterationResults);
         }
      }
      public List<string> GetLogFilesFromMxOutFolder(string scenarioName, List<Tuple<string, object, string>> inputs, int iteration)
      {
         List<string> files = new List<string>();
         string folder = this.myMxOutFolders.Find(item => item.Contains(scenarioName));
         string[] traceFilePaths = Directory.GetFiles(folder, "_Trace.xml");
         string[] csvFilePaths = Directory.GetFiles(folder, Processing80211RegressionResults.CsvFileSearchString);

         if (traceFilePaths.Count() != 0 || csvFilePaths.Count() != 0)
         {
            string channel = inputs.Find(item => item.Item1.Contains(Processing80211RegressionResults.TestChannelNumber)).Item2.ToString();
            int dataRate = System.Convert.ToInt32(inputs.Find(item => item.Item1.Contains(Processing80211RegressionResults.DataRate)).Item2);
            string datarateName = Enum.GetName(typeof(DataRate), dataRate);
            if (traceFilePaths.Count() != 0)
            {
               foreach (string file in traceFilePaths)
               {
                  string traceFileName = Path.GetFileName(file);
                  if (traceFileName.Contains(channel) && traceFileName.Contains(datarateName))
                  {
                     files.Add(file);
                  }
               }
            }
            if (csvFilePaths.Count() != 0)
            {
               foreach (string file in csvFilePaths)
               {
                  string csvFileName = Path.GetFileName(file);
                  if ((csvFileName.Contains(channel) && csvFileName.Contains(datarateName)))
                  {
                     files.Add(file);
                  }
               }
            }
         }
         return files;
      }
      private bool TryGetSignalIds(string mxlogFile, List<string> signalNames, out List<SignalInfo> filteredList)
      {
         filteredList = new List<SignalInfo>();
         XmlTextReader mxlogReader = new XmlTextReader(mxlogFile);
         mxlogReader.WhitespaceHandling = WhitespaceHandling.None;
         mxlogReader.Read();

         mxlogReader.ReadToFollowing("Signals");
         XmlReader signalsSubtree = mxlogReader.ReadSubtree();
         signalsSubtree.Read();
         while (signalsSubtree.ReadToFollowing("SigInfo"))
         {
            int id;
            string units = string.Empty;
            string name = string.Empty;
            XmlReader subtree = signalsSubtree.ReadSubtree();
            subtree.Read();
            subtree.ReadToFollowing("Id");
            id = subtree.ReadElementContentAsInt();
            subtree.Read();
            name = subtree.ReadContentAsString();
            subtree.Read();
            if(subtree.ReadToNextSibling("Analog"))
            {
               subtree.ReadToFollowing("Units");
               units = subtree.ReadElementContentAsString();
            }
            foreach (string signalName in signalNames)
            {
               if (signalName.Replace(" ", string.Empty).ToLowerInvariant() == name.Replace(" ", string.Empty).ToLowerInvariant())
               {
                  filteredList.Add(new SignalInfo(name, id, units));
               }
            }
         }

         return filteredList.Count == 0 ? false : true;
      }
      private Tuple<string, object, string> GetSignalValuePair(int id, List<SignalInfo> signalInfoList, object value)
      {
         foreach (SignalInfo info in signalInfoList)
         {
            if (id == info.Id)
            {
               if (info.Name == Processing80211RegressionResults.Payload)
               {
                  string[] data = System.Convert.ToString(value).Split(' ');
                  return new Tuple<string, object, string>(info.Name, $"{data.Count()} bytes of {data[0]}", string.Empty);
               }
               else if (info.Name == Processing80211RegressionResults.PduData)
               {
                  string[] contents = System.Convert.ToString(value).Split(' ').Take(22).ToArray();
                  string data = string.Empty;
                  foreach (string content in contents)
                  {
                     data += $"{content} ";
                  }

                  data += "\nExpected first 22 bytes of Pdu: 88 00 00 00 FF FF FF FF FF FF 23 DF 34 23 43 FD FF FF FF FF FF FF";
                  return new Tuple<string, object, string>(info.Name, data, string.Empty);
               }
               else
               {
                  string units = string.IsNullOrEmpty(info.Units) ? string.Empty : $"{info.Units}";
                  return new Tuple<string, object, string>(info.Name, value, units);
               }               
            }
         }
         return new Tuple<string, object, string>(string.Empty, new object(), string.Empty);
      }
      public List<Tuple<string, object>> GetTestCaseInputs(string scenarioName)
      {
         List<Tuple<string, object>> testCaseInputSignals = new List<Tuple<string, object>>();
         string scenarioLog = this.myScenarioLogs.FirstOrDefault(item => item.Contains(scenarioName));
         List<SignalInfo> signalInfoList = new List<SignalInfo>();
         Dictionary<string, double> signals = new Dictionary<string, double>();

         XmlTextReader mxlogReader = new XmlTextReader(scenarioLog);
         mxlogReader.WhitespaceHandling = WhitespaceHandling.None;
         mxlogReader.Read();
         mxlogReader.ReadToFollowing("Scenario");
         mxlogReader.ReadToFollowing("Path");

         if (!this.TryGetSignalIds(scenarioLog, this.TestCaseInputs, out signalInfoList))
         {
            // TODO: Throw Error if unable to get signal list
         }

         // Section: Test Case Input
         while (!mxlogReader.EOF)
         {
            mxlogReader.Read();
            if (mxlogReader.Name == "D")
            {
               mxlogReader.MoveToAttribute("Id");
               int id = mxlogReader.ReadContentAsInt();
               while (mxlogReader.Read())
               {
                  if (mxlogReader.Name == "V")
                  {
                     string content = mxlogReader.ReadElementContentAsString();
                     double val;
                     Tuple<string, object, string> input;

                     if (double.TryParse(content, out val))
                     {
                        input = this.GetSignalValuePair(id, signalInfoList, val);
                        if (!string.IsNullOrEmpty(input.Item1) && !testCaseInputSignals.Exists(item => item.Item1 == input.Item1))
                        {
                           testCaseInputSignals.Add(new Tuple<string, object>(input.Item1, (object)input.Item2));
                        }
                     }
                     else
                     {
                        if (!string.IsNullOrEmpty(content))
                        {
                           input = this.GetSignalValuePair(id, signalInfoList, (object)content);
                           if (!string.IsNullOrEmpty(input.Item1) && !testCaseInputSignals.Exists(item => item.Item1 == input.Item1))
                           {
                              testCaseInputSignals.Add(new Tuple<string, object>(input.Item1, (object)input.Item2));
                           }
                        }
                     }
                     break;
                  }
               }
            }
         }

         return testCaseInputSignals;
      }
      private void ReadMxVEvtFiles(List<string> scenarioRunEventsFiles, out List<string> runTimeErrors)
      {
         runTimeErrors = new List<string>();
         XmlReaderSettings settings = new XmlReaderSettings();
         settings.IgnoreWhitespace = true;
         settings.CloseInput = true;
         int errorCount = 0;

         foreach (string scenarioRunEventsFile in scenarioRunEventsFiles)
         {
            string scenarioName = Path.GetFileNameWithoutExtension(scenarioRunEventsFile);
            XmlReader evtFileReader = XmlReader.Create(scenarioRunEventsFile, settings);
            errorCount = this.GetSkippedIterationCountFromEvtFile(evtFileReader, scenarioName);
            if (errorCount > 0)
            {
               runTimeErrors.Add($"{scenarioName} contains errors. Report for {errorCount} iterartions not created. Details:\n");
            }

            evtFileReader = XmlReader.Create(scenarioRunEventsFile, settings);
            while (evtFileReader.ReadToFollowing("note"))
            {
               int severityLevel;
               if (int.TryParse(evtFileReader.GetAttribute("severity"), out severityLevel))
               {
                  if (severityLevel < 0)
                  {
                     long time = long.Parse(evtFileReader.GetAttribute("t")) / 1000000000;
                     string details = $"TestCase Time {time} sec, {evtFileReader.GetAttribute("details")}\n";
                     runTimeErrors.Add(details);
                  }
               }
            }
         }
      }
      private List<string> ReadMxVLogFile(string scenarioName, string mxlogFile, out List<List<Tuple<string, object, string>>> iterationInputs, out List<List<Tuple<string, object, string>>> iterationResults)
      {
         List<SignalInfo> inputSignalList;
         List<SignalInfo> outputSignalList;
         List<string> iterationVerdict = new List<string>();

         iterationInputs = new List<List<Tuple<string, object, string>>>();
         iterationResults = new List<List<Tuple<string, object, string>>>();

         XmlReaderSettings settings = new XmlReaderSettings();
         settings.IgnoreWhitespace = true;
         XmlReader mxlogReader = XmlReader.Create(mxlogFile, settings);
         mxlogReader.Read();

         mxlogReader.ReadToFollowing("Scenario");
         mxlogReader.ReadToFollowing("Path");

         if (!this.TryGetSignalIds(mxlogFile, this.StimulusSignals, out inputSignalList))
         {
            // TODO: Throw Error if unable to get signal list
         }
         if (!this.TryGetSignalIds(mxlogFile, this.ResultSignals, out outputSignalList))
         {
            // TODO: Throw Error if unable to get signal list
         }
         SignalInfo runTimeErrorSignal = outputSignalList.Find(item => item.Name.Equals(Processing80211RegressionResults.MxVRunTimeEvent));

         mxlogReader.ReadToFollowing("DataBlock");

         List<Tuple<string, object, string>> inputsForMonotonicTest = new List<Tuple<string, object, string>>();
         List<Tuple<string, object, string>> resultsForMonotonicTest = new List<Tuple<string, object, string>>();
         bool iterationVerdictForMonotonicTest = true;

         while (!mxlogReader.EOF)
         {
            // Section: Iteration Signals
            if (mxlogReader.ReadToFollowing("Comment"))
            {
               XmlReader subtree = mxlogReader.ReadSubtree();
               string comment = subtree.ReadToFollowing("Str") ? subtree.ReadElementContentAsString() : string.Empty;

               if (comment.Contains(scenarioName) && comment.Contains("starts running"))
               {
                  List<Tuple<string, object, string>> inputs = new List<Tuple<string, object, string>>();
                  List<Tuple<string, object, string>> results = new List<Tuple<string, object, string>>();
                  mxlogReader.ReadToFollowing("D");

                  while (mxlogReader.Name == "D")
                  {
                     subtree = mxlogReader.ReadSubtree();
                     int id = 0;
                     if (int.TryParse(mxlogReader.GetAttribute("Id"), out id))
                     {
                        if (subtree.ReadToDescendant("V"))
                        {
                           string content = subtree.ReadElementContentAsString();
                           double val;

                           if (double.TryParse(content, out val))
                           {
                              Tuple<string, object, string> input;
                              Tuple<string, object, string> result;

                              input = this.GetSignalValuePair(id, inputSignalList, val);
                              if (!string.IsNullOrEmpty(input.Item1))
                              {
                                 inputs.Add(input);
                              }

                              result = this.GetSignalValuePair(id, outputSignalList, val);
                              if (!string.IsNullOrEmpty(result.Item1))
                              {
                                 // Where multiple transitions exist, use the latest
                                 if (results.Exists(item => item.Item1 == result.Item1))
                                 {
                                    int index = results.FindIndex(item => item.Item1 == result.Item1);
                                    results[index] = result;
                                 }
                                 else
                                 {
                                    if (scenarioName.Contains(TestPurposes.TP80211TxtMac01Name) && result.Item1 == Processing80211RegressionResults.TestAnalysisOut)
                                    {
                                       int index = results.FindIndex(item => item.Item1 == Processing80211RegressionResults.SequenceNumber);
                                       if (index != -1)
                                       {
                                          string sequenceNumberAnalysis = Convert.ToInt32(result.Item2) == 0 ? "Increments per frame" : "Does not increment";
                                          results[index] = new Tuple<string, object, string>(Processing80211RegressionResults.SequenceNumber, sequenceNumberAnalysis, string.Empty);
                                       }
                                    }
                                    if (!(scenarioName.Contains(TestPurposes.TP80211RxtMac01Name) || scenarioName.Contains(TestPurposes.TP80211TxtMac01Name) && result.Item1 == Processing80211RegressionResults.TestAnalysisOut))
                                    {
                                       results.Add(result);
                                    }
                                 }
                              }
                           }
                           else if (content.Contains(' ') && !content.Contains("null data")) //Check if content is a byte array
                           {
                              Tuple<string, object, string> result = this.GetSignalValuePair(id, outputSignalList, content);
                              if (!string.IsNullOrEmpty(result.Item1))
                              {
                                 if (results.Exists(item => item.Item1 == result.Item1))
                                 {
                                    int index = results.FindIndex(item => item.Item1 == result.Item1);
                                    results[index] = result;
                                 }
                                 else
                                 {
                                    results.Add(result);
                                 }
                              }
                           }
                        }
                     }
                     mxlogReader.Read();
                  }

                  if (mxlogReader.Name == "Comment")
                  {
                     subtree = mxlogReader.ReadSubtree();
                     subtree.ReadToFollowing("Str");
                     comment = subtree.ReadElementContentAsString();
                     if (comment.Contains(scenarioName) && comment.Contains("is Completed"))
                     {
                        if (!(scenarioName.Equals(TestPurposes.TP80211RxtPhy05Name) || scenarioName.Equals(TestPurposes.TP80211TxtPhy07Name)))
                        {
                           // Iteration finished
                           iterationInputs.Add(inputs);
                           iterationResults.Add(results);
                        }
                        else
                        {
                           inputsForMonotonicTest.AddRange(inputs);
                           resultsForMonotonicTest.AddRange(results);
                        }
                     }
                  }
               }

               if ((scenarioName.Equals(TestPurposes.TP80211RxtPhy05Name) || scenarioName.Equals(TestPurposes.TP80211TxtPhy07Name)) &&
                  comment.Contains("Check For Monotonicity") && comment.Contains("starts running"))
               {
                  // Iteration finished
                  iterationInputs.Add(inputsForMonotonicTest);
                  iterationResults.Add(resultsForMonotonicTest);
                  inputsForMonotonicTest = new List<Tuple<string, object, string>>();
                  resultsForMonotonicTest = new List<Tuple<string, object, string>>();
               }

               if (comment.Contains("Post Processing Result") && comment.Contains("Job"))
               {
                  if (!(scenarioName.Equals(TestPurposes.TP80211RxtPhy05Name) || scenarioName.Equals(TestPurposes.TP80211TxtPhy07Name)))
                  {
                     if (comment.Contains(scenarioName))
                     {
                        iterationVerdict.Add(comment.Contains("passed") ? "PASS" : "FAIL");
                     }
                  }
                  else if (scenarioName.Equals(TestPurposes.TP80211RxtPhy05Name) || scenarioName.Equals(TestPurposes.TP80211TxtPhy07Name))
                  {
                     if (comment.Contains("Check For Monotonicity"))
                     {
                        string verdict = iterationVerdictForMonotonicTest ? "Power Level Accuracy PASS, " : "Power Level Accuracy FAIL, ";
                        verdict += comment.Contains("passed") ? "Monotonicity PASS" : "Monotonicity FAIL";
                        iterationVerdict.Add(verdict);
                     }
                     else if (comment.Contains(scenarioName))
                     {
                        iterationVerdictForMonotonicTest = comment.Contains("passed") ? iterationVerdictForMonotonicTest : iterationVerdictForMonotonicTest;
                     }
                  }
               }
            }
         }

         return iterationVerdict;
      }
      private void ReadMxVRegressionLogFile()
      {
         XmlTextReader mxrlgReader = new XmlTextReader(this.myMxVRegressionLogFile);
         mxrlgReader.WhitespaceHandling = WhitespaceHandling.None;
         mxrlgReader.Read();

         mxrlgReader.ReadToFollowing("Results");
         while (mxrlgReader.ReadToFollowing("Result"))
         {
            mxrlgReader.MoveToAttribute("Judgement");
            string result = mxrlgReader.ReadContentAsString();
            mxrlgReader.ReadToFollowing("Scenario");
            mxrlgReader.MoveToAttribute("Name");
            string scenarioName = mxrlgReader.ReadContentAsString();
            string name = TestPurposes.TP80211.Find(item => scenarioName.Contains(item));
            if (scenarioName != "Calibrate Modem" && scenarioName != "Check Modem Output" && !string.IsNullOrEmpty(name))
            {
               this.myScenarioResults.Add(new MxDSRCReportGenerator.ResultSummary(name, result));
            }
         }
      }
      
      private class SignalInfo
      {
         public SignalInfo(string name, int id, string units)
         {
            this.Id = id;
            this.Name = name;
            this.Units = units;
         }

         public int Id
         {
            get;
            private set;
         }
         public string Name
         {
            get;
            private set;
         }
         public string Units
         {
            get;
            private set;
         }
      }
   }
}
