﻿//******************************************************************************************
//*   Proprietary program material                                                         *
//*                                                                                        *
//*   This material is proprietary to Danlaw, Inc., and is not to be reproduced,  used     *
//*   or disclosed except in accordance with a written license agreement with Danlaw, Inc. *
//*                                                                                        *
//*   (C) Copyright 2017 Danlaw, Inc.  All rights reserved.                                *
//*                                                                                        *
//*   Danlaw, Inc., believes that the material furnished herewith is accurate and          *
//*   reliable.  However, no responsibility, financial or otherwise, can be accepted for   *
//*   any consequences arising out of the use of this material.                            *
//*                                                                                        *
//******************************************************************************************

namespace MxDSRCReportGenerator
{
   internal class ResultSummary
   {
      public ResultSummary(string testCaseName, string result)
      {
         this.ScenarioName = testCaseName;
         this.Result = result;
      }

      public string Result
      {
         get;
         private set;
      }
      public string ScenarioName
      {
         get;
         private set;
      }
   }
}
